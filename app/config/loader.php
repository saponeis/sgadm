<?php

$loader = new \Phalcon\Loader();


$loader->registerNamespaces(
    array(
       "Admin\\Controllers" => $config->application->controllersDir,
       "Admin\\Models"      => $config->application->modelsDir,
       "Admin\\views"      => $config->application->viewsDir,
    )
)->register();

// /**
//  * We're a registering a set of directories taken from the configuration file
//  */
// $loader->registerDirs(
// 	array(
// 		$config->application->controllersDir,
// 		$config->application->modelsDir
// 	)
// )
