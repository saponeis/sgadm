<?php

return new \Phalcon\Config(array(
	'database' => array(
		'adapter'  => 'Mysql',
		'host'     => 'localhost',
		'username' => 'root',
		'password' => 'root',
		'dbname'     => 'sgadm',
		'name'     => 'sgadm',
	),
	'application' => array(
		'controllersDir' => __DIR__ . '/../../app/controllers/',
		'modelsDir'      => __DIR__ . '/../../app/models/',
		'viewsDir'       => __DIR__ . '/../../app/views/',
		'libraryDir'     => __DIR__ . '/../../app/library/',
		'formsDir'       => __DIR__ . '/../../app/forms/',
		'cacheDir'       => __DIR__ . '/../../app/cache/volt/',
		'vendorDir'		 => __DIR__ . '/../../vendor/',
		'baseUri'        => 'http://socialgamers.dev/',
		'debug'			 => 'true',

	),
	'mail' =>  array(
		'fromName' => 'SocialGamers',
		'fromEmail' => 'noreply@socialgamers.com.br',
		'smtp' => array(
			'server' => 'smtp.socialgamers.com.br',
			'port' => 587,
			'security' => 'ssl',
			'username' => 'noreply@socialgamers.com.br',
			'password' => 'piloto4510',
		),
	),
));
