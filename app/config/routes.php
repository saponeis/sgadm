<?php

use Phalcon\Mvc\Router;

$router = new Router(false);


$router->add("/", [
    'controller' => 'index',
    'action' => 'index',
]);

$router->add("/campeonatos", [
    'controller' => 'championship',
    'action' => 'index',
]);

$router->add("/parceiros", [
    'controller' => 'index',
    'action' => 'partners',
]);

$router->add("/entrar", [
    'controller' => 'index',
    'action' => 'login',
]);
    
//ADMIN URL's
$router->add("/admin/etapas", [
    'controller' => 'admin',
    'action' => 'steps',
]);

//ADMIN URL's
$router->add("/admin/etapas/nova", [
    'controller' => 'admin',
    'action' => 'newStep',
]);

$router->add("/admin/etapas/criar", [
    'controller' => 'admin',
    'action' => 'createStep',
]);

$router->add("/admin/etapas/editar", [
    'controller' => 'admin',
    'action' => 'editStep',
]);

$router->add("/admin/etapas/salvar", [
    'controller' => 'admin',
    'action' => 'saveStep',
]);

$router->add("/admin/etapas/encerrar/{stepId}", [
    'controller' => 'admin',
    'action' => 'closeStep',
]);

$router->add("/admin/etapas/report-vitoria/{stepId}", [
    'controller' => 'admin',
    'action' => 'victoryReport',
]);

$router->add("/admin/etapas/report", [
    'controller' => 'admin',
    'action' => 'report',
]);

$router->add("/admin/etapas/report/validar/{reportId}", [
    'controller' => 'admin',
    'action' => 'validateReport',
]);

$router->add("/admin/etapas/lista-de-presenca/{stepId}", [
    'controller' => 'admin',
    'action' => 'attendanceList',
]);
$router->add("/admin/forcarcheckinlist", [
    'controller' => 'admin',
    'action' => 'checkinForceList',
]);

$router->add("/admin/etapas/lista-de-presenca", [
    'controller' => 'admin',
    'action' => 'listAttendance',
]);

$router->add("/admin/ferramentas", [
    'controller' => 'admin',
    'action' => 'tools',
]);

$router->add("/admin/forcar", [
    'controller' => 'admin',
    'action' => 'force',
]);


$router->add("/admin/codigodetorneio", [
    'controller' => 'admin',
    'action' => 'generateCode',
]);





//CAMPEONATOS

$router->add("/admin/campeonatos", [
    'controller' => 'admin',
    'action' => 'championship',
]);

$router->add("/admin/campeonatos/novo", [
    'controller' => 'admin',
    'action' => 'newChampionship',
]);

$router->add("/admin/campeonatos/criar", [
    'controller' => 'admin',
    'action' => 'createChampionship',
]);

$router->add("/admin/campeonatos/editar", [
    'controller' => 'admin',
    'action' => 'editChampionship',
]);

$router->add("/admin/campeonatos/salvar", [
    'controller' => 'admin',
    'action' => 'saveChampionship',
]);



$router->add("/admin", [
    'controller' => 'admin',
    'action' => 'index',
]);


// Championship


$router->add("/campeonatos/{slug}", [
    'controller' => 'championship',
    'action' => 'about',
]);

$router->add("/campeonatos/inscricao/{championshipId}", [
    'controller' => 'championship',
    'action' => 'subscribe',
]);

$router->add("/campeonatos/check-in/{championshipId}", [
    'controller' => 'championship',
    'action' => 'checkin',
]);

$router->add("/campeonatos/checkin", [
    'controller' => 'championship',
    'action' => 'checkinTeam',
]);


$router->add("/campeonatos/inscreverse", [
    'controller' => 'championship',
    'action' => 'subscribeTeam',
]);

$router->add("/campeonatos/regras/{championshipId}", [
    'controller' => 'championship',
    'action' => 'rules',
]);

$router->add("/campeonatos/chave/{championshipId}", [
    'controller' => 'championship',
    'action' => 'brackets',
]);

$router->add("/campeonatos/chave/etapa/{stepId}", [
    'controller' => 'championship',
    'action' => 'stepBrackets',
]);

$router->add("/campeonatos/ranking/{championshipId}", [
    'controller' => 'championship',
    'action' => 'ranking',
]);

$router->add("/campeonatos/times/{championshipId}", [
    'controller' => 'championship',
    'action' => 'teams',
]);

$router->add("/campeonatos/times/etapa/{stepId}", [
    'controller' => 'championship',
    'action' => 'stepTeams',
]);

$router->add("/campeonatos/reportar-vitoria/{championshipId}", [
    'controller' => 'championship',
    'action' => 'victoryReport',
]);

$router->add("/campeonatos/reportarvitoria", [
    'controller' => 'championship',
    'action' => 'reportVictory',
]);


// TEAM URL'S


$router->add("/times/novo", [
    'controller' => 'team',
    'action' => 'new',
]);


$router->add("/times/criar-time", [
    'controller' => 'team',
    'action' => 'create',
]);

$router->add("/times/meu-time", [
    'controller' => 'team',
    'action' => 'profile',
]);

$router->add("/times/perfil/{teamId}", [
    'controller' => 'team',
    'action' => 'profile',
]);

$router->add("/times/editar-time", [
    'controller' => 'team',
    'action' => 'editProfile',
]);

$router->add("/times/editar", [
    'controller' => 'team',
    'action' => 'editTeam',
]);


$router->add("/times/adicionar-jogador", [
    'controller' => 'team',
    'action' => 'addPlayer',
]);

$router->add("/times/adicionar", [
    'controller' => 'team',
    'action' => 'playerAdd',
]);

$router->add("/times/mudar-capitao", [
    'controller' => 'team',
    'action' => 'changeCapitan',
]);

$router->add("/times/remover-jogador", [
    'controller' => 'team',
    'action' => 'removePlayer',
]);

$router->add("/times/deletar-time", [
    'controller' => 'team',
    'action' => 'deleteTeam',
]);


$router->add("/times/deletar", [
    'controller' => 'team',
    'action' => 'teamDelete',
]);


$router->add("/times/remover", [
    'controller' => 'team',
    'action' => 'playerRemove',
]);

$router->add("/times/sair", [
    'controller' => 'team',
    'action' => 'exit',
]);

$router->add("/times", [
    'controller' => 'team',
    'action' => 'index',
]);

// URL DE USER

$router->add("/usuario", [
    'controller' => 'user',
    'action' => 'index',
]);

$router->add("/usuario/login", [
    'controller' => 'user',
    'action' => 'login',
]);

$router->addPost("/usuario/novo", [
    'controller' => 'user',
    'action' => 'create',
]);

$router->add("/usuario/confirmar/{hash}", [
    'controller' => 'user',
    'action' => 'confirm',
    // 'hash'  => 1,
]);

$router->add("/usuario/perfil/{userId}", [
    'controller' => 'user',
    'action' => 'profile',
    // 'hash'  => 1,
]);

$router->add("/usuario/sucesso", [
    'controller' => 'user',
    'action' => 'success',
]);

$router->add("/usuario/nao-confirmado", [
    'controller' => 'user',
    'action' => 'notConfirm',
]);

$router->add("/usuario/reconfirmar", [
    'controller' => 'user',
    'action' => 'reConfirm',
]);

$router->add("/erro", [
    'controller' => 'index',
    'action' => 'error',
]);

$router->add("/usuario/esqueci-senha", [
    'controller' => 'user',
    'action' => 'forgotPassword',
]);

$router->add("/usuario/nova-senha", [
    'controller' => 'user',
    'action' => 'newPassword',
]);

$router->add("/usuario/senha-sucesso", [
    'controller' => 'user',
    'action' => 'passwordSuccess',
]);

$router->add("/usuario/alterar-senha", [
    'controller' => 'user',
    'action' => 'changePassword',
]);

$router->add("/usuario/editar", [
    'controller' => 'user',
    'action' => 'editProfile',
]);


$router->add("/sair", [
    'controller' => 'user',
    'action' => 'logout',
]);

$router->notFound([
    "controller" => "index",
    "action"     => "route404"
]);


//MERCADO

$router->add("/mercado", [
    'controller' => 'index',
    'action' => 'underConstruction',
]);

return $router;