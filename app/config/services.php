<?php

/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = new \Phalcon\DI\FactoryDefault();

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->set('url', function() use ($config) {
	$url = new \Phalcon\Mvc\Url();
	$url->setBaseUri($config->application->baseUri);
	return $url;
});

/**
 * Setting up the view component
 */
$di->set('view', function () use ($config) {
    $view = new \Phalcon\Mvc\View();
    $view->setViewsDir($config->application->viewsDir);
    $view->registerEngines(array(
        '.volt' => function ($view, $di) use ($config) {
            $volt = new \Phalcon\Mvc\View\Engine\Volt($view, $di);
            $voltOptions = array(
                'compiledPath' => $config->application->cacheDir,
                'compiledSeparator' => '_'
            );
            if ($config->application->debug) {
                    $voltOptions['compileAlways'] = true;
            }
            $volt->setOptions($voltOptions);
            //load function php
            $compiler = $volt->getCompiler();
            //define variable translate
            $compiler->addFunction('t', '_');
            return $volt;
        }
    ));
    return $view;
}, true);

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set('db', function() use ($config) {
	return new \Phalcon\Db\Adapter\Pdo\Mysql(array(
		"host" => $config->database->host,
		"username" => $config->database->username,
		"password" => $config->database->password,
		"dbname" => $config->database->name
	));
});

/**
 * Add routing capabilities
 */
$di->set('router', function () {
    require __DIR__.'/routes.php';
    return $router;
});

/**
 * Start the session the first time some component request the session service
 */
$di->set('session', function() {
	$session = new \Phalcon\Session\Adapter\Files();
	$session->start();
	return $session;
});

/**
 * Register a user component
 */
$di->set('elements', function(){
	return new Elements();
});

$di->set("config", function() use ($config) {
    return $config;
});

$di->set('flashSession', function () {
    $flash = new \Phalcon\Flash\Session(
        array(
            'error'   => 'alert alert-danger',
            'success' => 'alert alert-success',
            'notice'  => 'alert alert-info',
            'warning' => 'alert alert-warning'
        )
    );

    return $flash;
});

// Registering a dispatcher
$di->set('dispatcher', function () {
    $dispatcher = new \Phalcon\Mvc\Dispatcher();
    $dispatcher->setDefaultNamespace("Admin\\Controllers");
    return $dispatcher;
});