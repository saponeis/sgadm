<?php

use Phalcon\Mvc\Controller,
    Phalcon\Mvc\View;

namespace Admin\Controllers;

class ControllerBase extends \Phalcon\Mvc\Controller
{

    public function index()
    {
        $this->auth();
        $userId = $this->session->get('userId');
        return $this->response->redirect("usuario/perfil/$userId");
    }

	public function initialize()
    {
        $isLogged = $this->session->get('auth');
        $this->view->setLayout('full');
        if ($isLogged){
            $this->view->setLayout('logged');  
            $this->view->summoner = $this->session->get('summoner');
            $this->view->name = $this->session->get('name');
            $this->view->teamId = $this->session->get('teamId');
            $this->view->userId = $this->session->get('userId');
            $this->view->role = $this->session->get('role');
            $this->view->email = $this->session->get('email');
            $this->view->userPicture = $this->session->get('picture');
        }
        $this->view->championships = \Admin\Models\Championship::find();
	}

    public function auth()
    {
        if (!$this->session->has("auth")) {
            return $this->response->redirect("/");
        }
    }

    public function registerSession($user)
    {
        $this->session->set('auth', 1);
        $this->session->set('userId', $user->id);
        $this->session->set('email', $user->email);
        $this->session->set('summoner', $user->summoner);
        $this->session->set('name', $user->getFullName());
        $this->session->set('teamId', $user->teamId);
        $this->session->set('role', $user->role);
        $this->session->set('picture', $user->picture);

    }

    public function isPost()
    {
        if (!$this->request->isPost()){
            $this->index();
        }
    }

    public function redirectError($message, $backUrl)
    {
        $this->flashSession->error($message);
        return $this->dispatcher->forward(  
            [
                'controller' => 'index',
                'action' => 'error',
                'params' => ['back' =>$backUrl],
            ]
        );          
    }
}