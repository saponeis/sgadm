<?php

use Phalcon\Mvc\Controller,
    Phalcon\Mvc\View;

namespace Admin\Controllers;

class ChampionshipController extends ControllerBase
{
    public function subscribeAction()
    {

        $backUrl = "/usuario/perfil/";
        $message = "Sua Requisição não pode ser processada no momento.";
        $message .= '<br/>Por gentileza tente novamente.';

        
        $championshipId = $this->dispatcher->getParam('championshipId');
        if (!$championshipId) {
            return $this->redirectError($message, $backUrl);
        }

        $championship = \Admin\Models\Championship::findFirst($championshipId);

        if (!$championship) {
            return $this->redirectError($message, $backUrl);       
        }

        $this->view->championship = $championship;
        

        $steps = $championship->steps->filter(
            function ($step) {
                if ($step->status == 1) {
                    return $step;
                }
            }
        );

        $this->view->steps = $steps;

        foreach ($steps as $step) {
            $stepId[] = $step->id;
        }

        $stepId = implode(',', $stepId);
        $teamId = $this->session->get('teamId');

        $this->view->teamId = $teamId;

        $subs = \Admin\Models\Subscribe::find([
                "teamId = :teamId: and stepId  in (:stepId:)",
                "bind" => ['teamId' => $teamId, 'stepId'=> $stepId,],
            ]);

        if (!$this->session->has("auth")) {
            $message = "Faça o Login primeiro para inscrever seu time.";
            $this->view->message = $message;
            $this->view->error = true;
            return 0;
        }

        if (!$teamId) {
            $message = "É necessário criar um time para se inscrever na etapa.";
            $this->view->message = $message;
            $this->view->error = true;

            return 0;
        }

        
        $role = $this->session->get('role');
        if ($role != 3) {
            $message = "Apenas o capitão pode inscrever seu time.";
            $this->view->message = $message;
            $this->view->error = true;
            return 0;
        }
        
        if ($subs->count()) {
            $message = "Seu time já está inscrito em uma etapa do campeonato!";
            $this->view->message = $message;
            $this->view->error = true;

            return 0;
        }


        $this->view->error = false;
    }        

    public function subscribeTeamAction()
    {

        $backUrl = "/usuario/perfil/";
        $message = "Sua Requisição não pode ser processada no momento.";
        $message .= '<br/>Por gentileza tente novamente.';

        if (!$this->request->isPost()) {
            return $this->redirectError($message, $backUrl);
        }

        $post = $this->request->getPost();
        $subs = new \Admin\Models\Subscribe;
        $subs->createData($post);
        if (!$subs->save()) {
            $messages = '';
            foreach ($subs->getMessages() as $message) {
                $messages .= $message . '<br/>';
            }
            return $this->redirectError($messages, $backUrl);
        }
        $message = "Inscrição realizada com sucesso!";
        $this->flashSession->success($message);
        return $this->response->redirect("times/meu-time");
    }

    public function bracketsAction()
    {
        $championshipId = $this->dispatcher->getParam('championshipId');
        if (!$championshipId) {
            return $this->response->redirect("/campeonatos");
        }

        $championship = \Admin\Models\Championship::findFirst($championshipId);
        $this->view->championship = $championship;
    }

    public function stepBracketsAction()
    {
        $backUrl = "/usuario/perfil/";
        $message = "Sua Requisição não pode ser processada no momento.";
        $message .= '<br/>Por gentileza tente novamente.';

        $this->view->error = false;
        $stepId = $this->dispatcher->getParam('stepId');

        if (!$stepId) {
            return $this->redirectError($message, $backUrl);
        }

        $step = \Admin\Models\Step::findFirst($stepId);
        if (!$step) {
            return $this->redirectError($message, $backUrl);
        }

        $this->view->step = $step;
    }

    public function rulesAction() {
        $championshipId = $this->dispatcher->getParam('championshipId');
        if (!$championshipId) {
            return $this->response->redirect("/campeonatos");
        }

        $championship = \Admin\Models\Championship::findFirst($championshipId);
        $this->view->championship = $championship;
    }

    public function stepTeamsAction() 
    {

        $backUrl = "/usuario/perfil/";
        $message = "Sua Requisição não pode ser processada no momento.";
        $message .= '<br/>Por gentileza tente novamente.';

        $this->view->error = false;
        $stepId = $this->dispatcher->getParam('stepId');

        if (!$stepId) {
            return $this->redirectError($message, $backUrl);
        }

        $step = \Admin\Models\Step::findFirst($stepId);
        $this->view->step = $step;

        if (!$step) {
            $message = "Não existem times inscritos neste campeonato.";
            $this->view->message = $message;
            $this->view->error = true;
            return 0;
        }
        $teams = [];
        
        foreach ($step->subscribes as $subs) {
            if ($subs->team){
               $teams[] =  $subs->team;    
            }
        }
        // var_dump($teams); die;
        if (empty($teams)) {
            $message = "Não existem times inscritos nesta etapa.";
            $this->view->message = $message;
            $this->view->error = true;
            return 0;
        }
        
        $this->view->teams = $teams;
    }

    public function teamsAction()
    {

        $backUrl = "/usuario/perfil/";
        $message = "Sua Requisição não pode ser processada no momento.";
        $message .= '<br/>Por gentileza tente novamente.';

        $this->view->error = false;
        $championshipId = $this->dispatcher->getParam('championshipId');

        if (!$championshipId) {
            return $this->redirectError($message, $backUrl);
        }

        $this->view->steps = \Admin\Models\Step::find(
            [
                'championshipId = :championshipId:',
                'bind' => ['championshipId' => $championshipId],
            ]
        );
    }

    public function rankingAction() 
    {
        $championshipId = $this->dispatcher->getParam('championshipId');
        if (!$championshipId) {
            return $this->response->redirect("/campeonatos");
        }

        $championship = \Admin\Models\Championship::findFirst($championshipId);
        $this->view->championship = $championship;
    }
    
    public function aboutAction()
    {
        
        $slug = $this->dispatcher->getParam('slug');
        if (!$slug) {
            return $this->response->redirect("campeonatos");
        }    

        $championship = \Admin\Models\Championship::findFirst([
            'slug = :slugname:',
            'bind' => ['slugname' => $slug],
        ]);


        if (!$championship) {
            return $this->response->redirect("/campeonatos");
        }  

        $this->view->championship = $championship;
        $view = 'championship/about/' . $championship->slug;
        $this->view->pick([$view]);


    }


    public function checkinAction()
    {
     
        $backUrl = "/usuario/perfil/";
        $message = "Sua Requisição não pode ser processada no momento.";
        $message .= '<br/>Por gentileza tente novamente.';

        
        $championshipId = $this->dispatcher->getParam('championshipId');
        if (!$championshipId) {
            return $this->redirectError($message, $backUrl);
        }

        $championship = \Admin\Models\Championship::findFirst($championshipId);

        if (!$championship) {
            return $this->redirectError($message, $backUrl);       
        }

        $teamId = $this->session->get('teamId');
        
        if (!$teamId) {
            $message = "É preciso ter um time para acessar essa área.";
            return $this->redirectError($message, $backUrl);

        }


        $team = \Admin\Models\Team::findFirst($teamId);

        $subs = $team->subscribe->filter(
            function ($sub) {
                if ($sub->step->canCheckin()) {
                    return $sub;
                }
            }
        );

        $this->view->subs = $subs;

        $this->view->championship = $championship;

    }

    public function checkinTeamAction()
    {

        $backUrl = "/usuario/perfil/";
        $message = "Sua Requisição não pode ser processada no momento.";
        $message .= '<br/>Por gentileza tente novamente.';

        if (!$this->request->isPost()) {
            return $this->redirectError($message, $backUrl);
        }

        $post = $this->request->getPost();

        $sub = \Admin\Models\Subscribe::findFirst($post['subId']);

        if (!$sub) {
            return $this->redirectError($messages, $backUrl);
        }
        $sub->status = 2;
        $sub->updatedAt = Date("Y-m-d H:m:s");

        if (!$sub->save()) {
            $messages = '';
            foreach ($subs->getMessages() as $message) {
                $messages .= $message . '<br/>';
            }
            return $this->redirectError($messages, $backUrl);
        }
        $message = "Checkin realizado com sucesso!";
        $this->flashSession->success($message);
        return $this->response->redirect("times/meu-time");
    }


    public function indexAction()
    {

    }

    public function reportVictoryAction()
    {
        
        $backUrl = "/usuario/perfil/";
        $message = "Sua Requisição não pode ser processada no momento.";
        $message .= '<br/>Por gentileza tente novamente.';

        if (!$this->request->isPost()) {
            return $this->redirectError($message, $backUrl);
        }

        $post = $this->request->getPost();
        

        $report = new \Admin\Models\VictoryReport();
        

        $report->createData($post);
        if (!$report->save()) {
            $messages = '';
            foreach ($report->getMessages() as $message) {
                $messages .= $message . '<br/>';
            }
            return $this->redirectError($messages, $backUrl);
        }
        $message = "Vitória Reportada com Sucesso!";
        $this->flashSession->success($message);
        return $this->response->redirect("times/meu-time");
    }



    public function victoryReportAction()
    {

        $backUrl = "/usuario/perfil/";
        $message = "Sua Requisição não pode ser processada no momento.";
        $message .= '<br/>Por gentileza tente novamente.';

        
        $championshipId = $this->dispatcher->getParam('championshipId');
        
        if (!$championshipId) {
            return $this->redirectError($message, $backUrl);
        }

        $championship = \Admin\Models\Championship::findFirst($championshipId);

        if (!$championship) {
            return $this->redirectError($message, $backUrl);       
        }

        $this->view->championship = $championship;
        
        $teamId = $this->session->get('teamId');

        if (!$teamId) {
            $message = "É necessário criar um time para se inscrever na etapa.";
            $this->view->message = $message;
            $this->view->error = true;

            return 0;
        }


        $team = \Admin\Models\Team::findFirst($teamId);


        $subs = [];

        foreach ($team->subscribe as $sub) {
            if ($sub->status == 2 && $sub->step->championship->id == $championshipId) {
                $subs[] = $sub;
            }
        }




        $this->view->subs = $subs;

        if (!$this->session->has("auth")) {
            $message = "Faça o Login primeiro para inscrever seu time.";
            $this->view->message = $message;
            $this->view->error = true;
            return 0;
        }

        
        $role = $this->session->get('role');
        if ($role != 3) {
            $message = "Apenas o capitão pode inscrever seu time.";
            $this->view->message = $message;
            $this->view->error = true;
            return 0;
        }

/*
        $report = \Admin\Models\VictoryReport::findFirst([
            '(victoryTeam = :teamId: or defeatedTeam = :teamID:) and status = 1' ,
            'bind' => ['teamId' => $teamID],
        ]);

        if ($report) {
            $message = "Já existe um report para o seu time, nossa equipe está trabalhando para validar e liberar seu próximo jogo.";
            $this->view->message = $message;
            $this->view->error = true;
            return 0;
        }
        //var_dump($subs);die;
*/
        $this->view->error = false;
    }        

}