<?php

use Phalcon\Mvc\Controller,
    Phalcon\Mvc\View;

namespace Admin\Controllers;

class UserController extends ControllerBase
{
    public function indexAction()
    {
        parent::index();
    }

    public function loginAction()
    {

        $backUrl = "/usuario/perfil/";
        $message = "Sua Requisição não pode ser processada no momento.";
        $message .= '<br/>Por gentileza tente novamente.';


        if (!$this->request->isPost()) {
            return $this->redirectError($message, $backUrl);
        }

        $post = $this->request->getPost();
        $user = \Admin\Models\User::findFirst(
                [
                    "email = :email: and password = :senha:",
                    "bind" => [
                            "email" => $post['email'],
                            "senha" => md5($post['password']),
                        ],
                ]
            );

        if (!$user) {
            $error = 'O email ou a senha informada não são válidas.';
            $error .= '<br/>Por gentileza tente novamente ';
            $error .= "<a href='/entrar>Clicando Aqui</a>";
            $this->flashSession->error($error);
            $backUrl = "/entrar";
            $this->dispatcher->forward(
                [
                    'controller' => 'index',
                    'action' => 'error',
                    'params' => ['back' =>$backUrl],
                ]
            );
            return 0;
        }
     
        if ($user->status == 0) {
            return $this->response->redirect("usuario/nao-confirmado");
        }

        $this->registerSession($user);


        if($user->role == 1) {
            return $this->response->redirect("admin");
        }

        return $this->response->redirect("usuario/perfil/");
    }

    public function createAction()
    {
     
        $backUrl = "/entrar";
        $messages = "Sua Requisição não pode ser processada no momento.";
        $messages .= '<br/>Por gentileza tente novamente.';

        if (!$this->request->isPost()) {
            return $this->redirectError($messages, $backUrl);
        }

        $post = $this->request->getPost();
        $user = \Admin\Models\User::findFirst(
            [
                "email = :email:",
                "bind" => ['email' => $post['email']]
            ]
        );
        if ($user) {
            $messages = sprintf('O email %s já foi cadastrado.',
                $post['email']
            );

            return $this->redirectError($messages, $backUrl);
        }

        $user = new \Admin\Models\User;
        $user->createData($post);
        
        if (!$user->save()) {
            $messages = '';
            foreach ($user->getMessages() as $message) {
                $messages .= $message . '<br/>';
            }
            return $this->redirectError($messages, $backUrl);
        }

        $this->sendConfirmationEmail($user);
    }

    public function sendConfirmationEmail(\Admin\Models\User $user)
    {
        $backUrl = "/";
        $message = "Sua Requisição não pode ser processada no momento.";
        $message .= '<br/>Por gentileza tente novamente.';

        $mail = new \Admin\Models\Mail;
        if ( !$mail->sendConfirmEmail($user) ) {
            $message = sprintf("Houve um erro ao enviar o email de confirmação para %s.", $user->email);
            return $this->redirectError($message, $backUrl);
        }
 
        return $this->response->redirect("usuario/sucesso");
    }
    
    public function reConfirmAction()
    {
    
        $backUrl = "/entrar";
        $messages = "Sua Requisição não pode ser processada no momento.";
        $messages .= '<br/>Por gentileza tente novamente.';

        if (!$this->request->isPost()) {
            return $this->redirectError($messages, $backUrl);
        }

        $post = $this->request->getPost();
        $user = \Admin\Models\User::findFirst(
            [
                "email = :email:",
                "bind" => ['email' => $post['email']]
            ]
        );
        if (!$user) {
            $messages = sprintf('O email %s não foi encontrado em nossa base de dados.',
                $post['email']
            );
      
            $messages .= 'Por gentileza faça o seu cadastro.';
            return $this->redirectError($messages, $backUrl);
        }

        $this->sendConfirmationEmail($user);
   }

    public function confirmAction()
    {
        
        $backUrl = "/usuario/reconfirmar";
        $messages = "Sua Requisição não pode ser processada no momento.";
        $messages .= '<br/>Por gentileza tente novamente.';


        $hash = $this->dispatcher->getParam("hash");
        $user = \Admin\Models\User::findFirst(array(
                "hash = :hash:",
                "bind" => array('hash' => $hash)
        ));
        if (!$user) {
            $messages = 'Não foi possível localizar e confirmar sua conta de usuário.';
            return $this->redirectError($messages, $backUrl);
        }
        $newHash = md5(uniqid(rand(), TRUE));
        $user->hash = $newHash;

        $user->status = 1;

        if (!$user->save()) {
            $messages = '';
            foreach ($user->getMessages() as $message) {
                $messages .= $message . '<br/>';
            }
            return $this->redirectError($messages, $backUrl);
        }
   }

   public function newPasswordAction()
   {
        $backUrl = "/usuario/esqueci-senha";
        $messages = "Sua Requisição não pode ser processada no momento.";
        $messages .= '<br/>Por gentileza tente novamente.';

        if (!$this->request->isPost()) {
            return $this->redirectError($messages, $backUrl);
        }

        $post = $this->request->getPost();
        $user = \Admin\Models\User::findFirst(array(
                "email = :email:",
                "bind" => array('email' => $post['email'])
        ));
        // var_dump($user);die;
        if (!$user) {
            $messages = 'Não foi possível localizar sua conta através do email.';
            return $this->redirectError($messages, $backUrl);
        }

        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $password = substr(str_shuffle($chars),0,10);
        $newPassword = md5($password);

        $user->password = $newPassword;

        if (!$user->save()) {
            $messages = '';
            foreach ($user->getMessages() as $message) {
                $messages .= $message . '<br/>';
            }
            return $this->redirectError($messages, $backUrl);
        }

        $data = array( 'password' => $password);
        $mail = new \Admin\Models\Mail;
        if (!$mail->sendResetPasswordEmail($user, $password)) {
            $messages = 'Houve um problema ao enviar o email de senha.';
            return $this->redirectError($messages, $backUrl);
        }

        return $this->response->redirect("usuario/senha-sucesso");
    }

    public function profileAction()
    {
        $userId = $this->dispatcher->getParam('userId');
        $menu = false;
        if (!$userId) {
            $this->auth();
            $userId = $this->session->get('userId');
            $menu = true;
        }
        
        $userObject = \Admin\Models\User::findFirst($userId);

        $this->view->userName = $userObject->getFullName();
        $this->view->user = $userObject;
        $this->view->menu = $menu;

    }

    public function changePasswordAction()
    {

        $backUrl = "/usuario/alterar-senha";
        $message = "Sua Requisição não pode ser processada no momento.";
        $message .= '<br/>Por gentileza tente novamente.';
        
        if (!$this->request->isPost()) {
            return 0;
        }
        $post = $this->request->getPost();

        $userId = $this->session->get("userId");
        $user = \Admin\Models\User::findFirst($userId);

        if (!$user) {
            return $this->redirectError($message, $backUrl);
        }

        $password = md5($post['password']);

        if ($password != $user->password) {
            $error = 'Senha atual está incorreta.';
            $this->flashSession->error($error);
            return 0;
        }

        $newPassword = md5($post['newPassword']);

        if ($newPassword == $user->password) {
            $error = 'A nova senha deve ser diferente da atual..';
            $this->flashSession->error($error);
            return 0;
        }

        $user->password = $newPassword;
        if (!$user->save()) {
            $messages = '';
            foreach ($user->getMessages() as $message) {
                $messages .= $message . '<br/>';
            }
            return $this->redirectError($messages, $backUrl);
        }

        $message = 'Senha alterada com sucesso!';
        $this->flashSession->success($message);
        return $this->response->redirect("usuario/perfil/");
    }

    public function editProfileAction()
    {

        $backUrl = "/usuario/perfil/";
        $message = "Sua Requisição não pode ser processada no momento.";
        $message .= '<br/>Por gentileza tente novamente.';
        
        $user = \Admin\Models\User::findFirst(
                $this->session->get('userId')
            );


        if (!$user) {
            return $this->redirectError($message, $backUrl);
        }

        $this->view->user = $user;

        if (!$this->request->isPost()) {
            return 0;
        }
        
        $post = $this->request->getPost();

        $user->editProfile($post);
        if (!$user->save()) {
            $messages = '';
            foreach ($user->getMessages() as $message) {
                $messages .= $message . '<br/>';
            }
            return $this->redirectError($messages, $backUrl);
        }

        $message = "Perfil editado com sucesso!";
        $this->flashSession->success($message);

        if (!$this->request->hasFiles()) {
            return 0;
        }
                
        $files = $this->request->getUploadedFiles();
        $file = $files[0];

        if (!$file->isUploadedFile()) {
            return 0;
        }

        $filename = $user->id . '.' . $file->getExtension();
        $image =  new \Admin\Models\Images; 
        $result = $image->saveImageUserProfile($file, $user);
  
      if (!isset($result) || !$result) {
            return $this->redirectError($messages, $backUrl);
        }

        $user->picture = $filename;
        if (!$user->save()) {
            $messages = '';
            foreach ($user->getMessages() as $message) {
                $messages .= $message . '<br/>';
            }
            return $this->redirectError($messages, $backUrl);
        }

        $this->registerSession($user);
        $this->view->user = $user;
    }

    public function logOutAction()
    {
        $this->session->destroy();
        return $this->response->redirect('');
    }

    public function passwordSuccessAction()
    {

    }
   
    public function forgotPasswordAction()
    {

    }

    public function successAction()
    {

    }    

    public function notConfirmAction()
    {

    }
    
    public function newAction()
    {

    }
}