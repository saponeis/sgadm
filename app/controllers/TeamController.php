<?php

use Phalcon\Mvc\Controller,
    Phalcon\Mvc\View;

namespace Admin\Controllers;

class TeamController extends ControllerBase
{

    public function indexAction()
    {
        $this->view->teams = \Admin\Models\Team::find();    
    }

    public function profileAction()
    {
        $menu = false;
        $teamId = $this->dispatcher->getParam('teamId');
     
 		$backUrl = "/usuario/perfil/";
		$message = "Não encontramos o time desejado.";
        $message .= '<br/>Por gentileza tente novamente.';
        if (!$teamId) {
        	$teamId = $this->session->get('teamId');
        }

        if (!$teamId) {
            return $this->redirectError($message, $backUrl);
        }

        // var_dump($teamId);die;
        $team = \Admin\Models\Team::findFirst($teamId);

        if (!$team) {
 			return $this->redirectError($message, $backUrl);
     	}

        $userId = $this->session->get('userId');	

    	if ($team->capitanId == $userId) {
			$menu = true;
    	}
        $this->view->isLog = $this->session->has("auth");
    	$this->view->team = $team;
    	$this->view->menu = $menu;
        $this->view->players = $team->getAllPlayers();
    }

    public function createAction()
    {
        $backUrl = "/times/novo";
		$error = "Sua Requisição não pode ser processada no momento.";
        $error .= '<br/>Por gentileza tente novamente.';
        
        if (!$this->request->isPost()) {
 			return $this->redirectError($message, $backUrl);
   		}

        $post = $this->request->getPost();
        $team = \Admin\Models\Team::findFirst(
            [
                "name like :name:",
                "bind" => ['name' => '%' . $post['name'] . '%'],
            ]
        );
        
        if ($team) {
            $message = sprintf('O Time %s já foi cadastrado.',
                $post['name']
            );
        
            $message .= '<br/>Por gentileza tente novamente com outro nome.';
		
			return $this->redirectError($message, $backUrl);
	
        }

        $post['capitanId'] = $this->session->get('userId');
        $team = new \Admin\Models\Team;
        $team->createData($post);
        if (!$team->save()) {
        	$messages = '';
            foreach ($team->getMessages() as $message) {
                $messages .= $message . '<br/>';
            }
			return $this->redirectError($messages, $backUrl);

        }

        $user = \Admin\Models\User::findFirst($team->capitanId);

        if (!$user){
			return $this->redirectError($messages, $backUrl);
		}

        $user->teamId = $team->id;
        
        if (!$user->isAdm()){
            $user->role = 3;    
        }

        if (!$user->save()) {
			$messages = '';
            foreach ($team->getMessages() as $message) {
                $messages .= $message . '<br/>';
            }
			return $this->redirectError($messages, $backUrl);
        }

        $this->registerSession($user);

        return $this->response->redirect("times/meu-time");
    }

	public function editProfileAction()
	{
    	$teamId = $this->session->get('teamId');
    	$team = \Admin\Models\Team::findFirst($teamId);
    	$this->view->team = $team;
	}


    public function editTeamAction()
    {
        $userId = $this->session->get('userId');
        $teamId = $this->session->get('teamId');

        $backUrl = "/times/editar-time";
        $message = "Sua Requisição não pode ser processada no momento.";
        $message .= '<br/>Por gentileza tente novamente.';
        
        if (!$userId || !$teamId) {
            return $this->redirectError($message, $backUrl);
        }

        if (!$this->request->isPost()) {
			return $this->redirectError($message, $backUrl);
		}
        $post = $this->request->getPost();


        $team = \Admin\Models\Team::findFirst($post['teamId']);

        if (!$team) {
			return $this->redirectError($message, $backUrl);
		}

		if ($team->capitanId != $userId) {
            $message = 'Apenas o capitão pode editar o time';
			return $this->redirectError($message, $backUrl);

		}

		$team->editProfile($post);

		if (!$team->save()) {
			$messages = '';
            foreach ($team->getMessages() as $message) {
                $messages .= $message . '<br/>';
            }
			return $this->redirectError($messages, $backUrl);
		}

        if ($this->request->hasFiles()) {

            $files = $this->request->getUploadedFiles();
            $file = $files[0];
            if ($file->isUploadedFile()) {
                $filename = $post['teamId'] . '.' . $file->getExtension();
                $image =  new \Admin\Models\Images; 
                $result = $image->saveImageTeamProfile($file, $team);
                
                  if (!isset($result) || !$result) {
                        return $this->redirectError($messages, $backUrl);
                    }
                    
                $team->picture = $filename;
                $team->save();
            }
        } 

        $message = "Perfil editado com sucesso!";
        $this->flashSession->success($message);

        return $this->dispatcher->forward(  
            [
                'action' => 'editProfile',
                'params' => ['team' =>$team],
            ]
        );  

    }

    public function playerAddAction()
    {
		$userId = $this->session->get('userId');

        $backUrl = "/times/adicionar-jogador";
		$message = "Sua Requisição não pode ser processada no momento.";
        $message .= '<br/>Por gentileza tente novamente.';

        if (!$this->request->isPost()) {
			return $this->redirectError($message, $backUrl);
		}
        
        $post = $this->request->getPost();
        $team = \Admin\Models\Team::findFirst($post['teamId']);
        $player = $post['player'];

        if (!$team) {
            return $this->redirectError($message, $backUrl);
        }

        if ($team->isTeamFull()) {
            $message =  'Já existem 9 jogadores no seu time. Remova um nogador para adicionar outros.';
            $message .= '<br/>Por gentileza tente novamente.';
            return $this->redirectError($message, $backUrl);
        }

        if ($team->isInTournament()) {
            $message =  'Não é permitido alterar o time após checkin em um campeonato';
	        $message .= '<br/>Por gentileza tente novamente assim que finalizar o campeonato.';
			return $this->redirectError($message, $backUrl);
        }

        if (strpos($player, '#')===0) {
            $user = \Admin\Models\User::findFirst(substr($player, 1));


        } else {
            $user = \Admin\Models\User::findFirst(
                [
                    "email = :email:",
                    "bind" => [
                        "email" => $player,
                    ],
                ]
            );
        
        }

        if(!$user) {
            $message =  'Não encontramos o jogador em nossa base de dados.';
            $message .= '<br/>Por gentileza certifique-se de que ele está cadastrado no sistema da Social Gamers e tente novamente.';
            return $this->redirectError($message, $backUrl);
        }

        if($user->role != 5) {
            $message =  'O jogador já se encontra em outro time.';
            $message .= '<br/>Por gentileza certifique-se de que ele está cadastrado no sistema da Social Gamers e tente novamente.';
            return $this->redirectError($message, $backUrl);
        }

        $user->teamId = $team->id;
        
        if (!$user->isAdm()) {
            $user->role = 4;
        }

        if (!$user->save()) {
			$messages = '';
            foreach ($team->getMessages() as $message) {
                $messages .= $message . '<br/>';
            }
			return $this->redirectError($messages, $backUrl);
        }

        $message = "Jogador Adicionado com Sucesso!";
        $this->flashSession->success($message);
        return $this->response->redirect("times/meu-time");
    }

    public function changeCapitanAction()
    {

        $teamId = $this->session->get('teamId');
        $team = \Admin\Models\Team::findFirst($teamId);
        $this->view->players = $team->getAllPlayers();

        $backUrl = "/times/mudar-capitão";
        $message = "Sua Requisição não pode ser processada no momento.";
        $message .= '<br/>Por gentileza tente novamente.';

        if (!$this->request->isPost()) {
            $team = \Admin\Models\Team::findFirst($teamId);
            return 0;
        }
            
        $post = $this->request->getPost();

        if(!$team) {
            return $this->redirectError($messages, $backUrl);
        }

        $oldCapitan = \Admin\Models\User::findFirst($team->capitanId);
        $newCapitan = \Admin\Models\User::findFirst($post['player']);
        $team->capitanId = $newCapitan->id;
            
        if (!$team->save()) {
            $messages = '';
            foreach ($team->getMessages() as $message) {
                $messages .= $message . '<br/>';
            }
            return $this->redirectError($messages, $backUrl);
        }

        if (!$oldCapitan->isAdm()) {
            $oldCapitan->role = 4;
        }
            
        if (!$oldCapitan->save()) {
            $messages = '';
            foreach ($team->getMessages() as $message) {
                $messages .= $message . '<br/>';
            }
            return $this->redirectError($messages, $backUrl);
        }


        if (!$newCapitan->isAdm()) {
            $newCapitan->role = 3;
        }

        if (!$newCapitan->save()) {
            $messages = '';
            foreach ($team->getMessages() as $message) {
                $messages .= $message . '<br/>';
            }
            return $this->redirectError($messages, $backUrl);
        }

        $message = "Capitão Alterado com Sucesso!";
        $this->flashSession->success($message);
        $this->view->players = $team->getAllPlayers();
        return $this->response->redirect("times/meu-time");

    }


    public function playerRemoveAction()
    {

        $teamId = $this->session->get('teamId');
        $team = \Admin\Models\Team::findFirst($teamId);

        $backUrl = "/times/remover-jogador";
        $message = "Sua Requisição não pode ser processada no momento.";
        $message .= '<br/>Por gentileza tente novamente.';

        if(!$team) {
            return $this->redirectError($messages, $backUrl);
        }
        
        $this->view->players = $team->getAllPlayers();
        
        if (!$this->request->isPost()) {
            return 0;
        }
        
        $post = $this->request->getPost();

        $user = \Admin\Models\User::findFirst($post['player']);

        if(!$user) {
            return $this->redirectError($messages, $backUrl);
        }
        $user->teamId = 0;
        $user->role = 5;
        if (!$user->save()) {
            $messages = '';
            foreach ($team->getMessages() as $message) {
                $messages .= $message . '<br/>';
            }
            return $this->redirectError($messages, $backUrl);
        }

        $message = "Jogador removido com sucesso!";
        $this->flashSession->success($message);
        return $this->response->redirect("times/meu-time");
    }

    public function exitAction()
    {
        $backUrl = "/times/remover-jogador";
        $message = "Sua Requisição não pode ser processada no momento.";
        $message .= '<br/>Por gentileza tente novamente.';

        $teamId = $this->session->get('teamId');
        $userId = $this->session->get('userId');
        $userRole = $this->session->get('role');

        $team = \Admin\Models\Team::findFirst($teamId);

        if($userId == $team->capitanId) {
            $messages = "Para sair do time você deve trocar de capitão antes.";
            return $this->redirectError($messages, $backUrl);
        }

        $user = \Admin\Models\User::findFirst($userId);
        $user->teamId = 0;
        $user->role = 5;
        if (!$user->save()) {
            $messages = '';
            foreach ($user->getMessages() as $message) {
                $messages .= $message . '<br/>';
            }
            return $this->redirectError($messages, $backUrl);
        }

        $message = "Você saiu do time com sucesso!";
        $this->flashSession->success($message);
        return $this->response->redirect("usuario/perfil");
    }

    public function addPlayerAction()
    {
        $this->view->teamId = $this->session->get('teamId');
    }

    public function removePlayerAction()
    {
        $teamId = $this->session->get('teamId');
        $team = \Admin\Models\Team::findFirst($teamId);
        $this->view->players = $team->getAllPlayers();
    }

    public function teamDeleteAction()
    {

        $teamId = $this->session->get('teamId');
        $userId = $this->session->get('userId');
        

        $team = \Admin\Models\Team::findFirst($teamId);
        $this->view->players = $team->getAllPlayers();

        $backUrl = "/times/deletar-time";
        $message = "Sua Requisição não pode ser processada no momento.";
        $message .= '<br/>Por gentileza tente novamente.';

        if($userId != $team->capitanId) {
            $messages = "Apenas o capitão pode deletar o time.";
            return $this->redirectError($messages, $backUrl);
        }

        if (!$this->request->isPost()) {
            return $this->redirectError($messages, $backUrl);
        }
            
        $post = $this->request->getPost();

        if(!$team) {
            return $this->redirectError($messages, $backUrl);
        }

        $players = $team->getAllPlayers();
            
        foreach ($players as $player) {
            $player->role = 5;
            $player->teamId = 0;
            if (!$player->save()) {
                $messages = '';
                foreach ($user->getMessages() as $message) {
                    $messages .= $message . '<br/>';
                }
                return $this->redirectError($messages, $backUrl);
            } 
        }

        if (!$team->delete()) {
            $messages = '';
            foreach ($user->getMessages() as $message) {
                $messages .= $message . '<br/>';
            }
            return $this->redirectError($messages, $backUrl);
        }


        $message = "Time deletado com sucesso!";
        $this->flashSession->success($message);
        return $this->response->redirect("/usuario/perfil/");

    }

    public function newAction()
    {

    }

    public function deleteTeamAction()
    {

    }

}