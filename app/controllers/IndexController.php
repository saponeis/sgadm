<?php

use Phalcon\Mvc\Controller,
    Phalcon\Mvc\View;

namespace Admin\Controllers;

class IndexController extends ControllerBase
{
    public function indexAction()
    {

    }

    public function route404Action()
    {

    }

    public function partnersAction()
    {
    
    }

    public function errorAction()
    {
        $url = $this->dispatcher->getParam('back');
        if ($url) {
            $this->view->backUrl = $url;
        }
    }

    public function loginAction()
    {
    	if ($this->session->has("auth")) {
    		return $this->response->redirect("usuarios/index");
    	}
    }

    public function underConstructionAction()
    {

    }
}

