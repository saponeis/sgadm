<?php

use Phalcon\Mvc\Controller,
    Phalcon\Mvc\View;

namespace Admin\Controllers;

class AdminController extends ControllerBase
{

    public function initialize()
    {
        $role = $this->session->get('role');
        if ($role != 1) {
            return $this->response->redirect("/");
        }
        parent::initialize();
    }

    public function createChampionshipAction()
    {
        $backUrl = "/admin/campeonatos/novo";
		$message = "Sua Requisição não pode ser processada no momento.";
        $message .= '<br/>Por gentileza tente novamente.';
        
        if (!$this->request->isPost()) {
			return $this->redirectError($message, $backUrl);
		}
        $post = $this->request->getPost();
        $championship = new \Admin\Models\Championship();
        $championship->createData($post);

		if (!$championship->save()) {
			$messages = '';
            foreach ($championship->getMessages() as $message) {
                $messages .= $message . '<br/>';
            }
			return $this->redirectError($messages, $backUrl);
		}
            $files = $this->request->getUploadedFiles();

        if ($this->request->hasFiles()) {

            $files = $this->request->getUploadedFiles();
            $file = $files[0];
            if ($file->isUploadedFile()) {
                $filename = 'header_' . $championship->id . '.' . $file->getExtension();
                $image =  new \Admin\Models\Images; 
                $image->saveImageChampionshipProfile($file, $filename, $championship->id);
                $championship->image = $filename;
                $championship->save();
            }

	        $file = $files[1];
	        if ($file->isUploadedFile()) {
	            $filename = 'menu_' . $championship->id . '.' . $file->getExtension();
	            $image =  new \Admin\Models\Images; 
                $image->saveImageChampionshipProfile($file, $filename, $championship->id);
	            $championship->imageMenu = $filename;
	            $championship->save();
	        }

        } 
        $message = "Campeonato Criado com Sucesso!";
        $this->flashSession->success($message);
        
        return $this->response->redirect("/admin/campeonatos");
    }

    public function saveChampionshipAction()
    {
        $backUrl = "/admin/campeonatos/novo";
		$message = "Sua Requisição não pode ser processada no momento.";
        $message .= '<br/>Por gentileza tente novamente.';
        
        if (!$this->request->isPost()) {
			return $this->redirectError($message, $backUrl);
		}
        $post = $this->request->getPost();
        $championship = \Admin\Models\Championship::findFirst($post['championshipId']);
        $championship->editData($post);

		if (!$championship->save()) {
			$messages = '';
            foreach ($championship->getMessages() as $message) {
                $messages .= $message . '<br/>';
            }
			return $this->redirectError($messages, $backUrl);
		}
        
        $files = $this->request->getUploadedFiles();
        if ($this->request->hasFiles()) {

            $files = $this->request->getUploadedFiles();
            $file = $files[0];
            if ($file->isUploadedFile()) {
                $filename = 'header_' . $championship->id . '.' . $file->getExtension();
                $image =  new \Admin\Models\Images; 
                $image->saveImageChampionshipProfile($file, $filename, $championship->id);
                $championship->image = $filename;
                $championship->save();
            }

	        $file = $files[1];
	        if ($file->isUploadedFile()) {
	            $filename = 'menu_' . $championship->id . '.' . $file->getExtension();
	            $image =  new \Admin\Models\Images; 
	                $image->saveImageChampionshipProfile($file, $filename, $championship->id);
	            $championship->imageMenu = $filename;
	            $championship->save();
	        }

        } 
        $message = "Campeonato Editado com Sucesso!";
        $this->flashSession->success($message);
        
        return $this->response->redirect("/admin/campeonatos/editar");
    }

    public function createStepAction()
    {
        $backUrl = "/admin/etapas/nova";
        $message = "Sua Requisição não pode ser processada no momento.";
        $message .= '<br/>Por gentileza tente novamente.';
        
        if (!$this->request->isPost()) {
            return $this->redirectError($message, $backUrl);
        }
        
        $post = $this->request->getPost();


        $step = new \Admin\Models\Step();
        $step->createData($post);

        if (!$step->save()) {
            $messages = '';
            foreach ($step->getMessages() as $message) {
                $messages .= $message . '<br/>';
            }
            return $this->redirectError($messages, $backUrl);
        }

        $message = "Etapa Criada com Sucesso!";
        $this->flashSession->success($message);
        
        return $this->response->redirect("/admin/etapas");
    }

    public function saveStepAction()
    {
        if (!$this->request->isPost()) {
            return $this->redirectError($message, $backUrl);
        }
        
        $post = $this->request->getPost();
        //var_dump($post); die;


        $step = \Admin\Models\Step::findFirst($post['stepId']);
        $step->editData($post);
        if (!$step->save()) {
            $messages = '';
            foreach ($step->getMessages() as $message) {
                $messages .= $message . '<br/>';
            }
            return $this->redirectError($messages, $backUrl);
        }

        $message = "Etapa Alterada com Sucesso!";
        $this->flashSession->success($message);
        
        return $this->response->redirect("/admin/etapas");
        

    }

    public function closeStepAction()
    {
        $backUrl = "/admin/etapas/encerrar";
        $message = "Sua Requisição não pode ser processada no momento.";
        $message .= '<br/>Por gentileza tente novamente.';
        
        $stepId = $this->dispatcher->getParam('stepId');

        if ($stepId){

            $step = \Admin\Models\Step::findFirst($stepId);

            if (!$step) {
                return $this->redirectError($message, $backUrl);
            }

            $step->status = 2;

            if (!$step->save()) {
                $messages = '';
                foreach ($step->getMessages() as $message) {
                    $messages .= $message . '<br/>';
                }
                return $this->redirectError($messages, $backUrl);
            }

            $message = "Etapa Encerrada com Sucesso!";
            $this->flashSession->success($message);
            return $this->response->redirect("/admin/etapas");

        }

        $this->view->steps = \Admin\Models\Step::find('status = 1');
    }

    public function attendanceListAction()
    {
        $backUrl = "/admin/etapas/encerrar";
        $message = "Sua Requisição não pode ser processada no momento.";
        $message .= '<br/>Por gentileza tente novamente.';
        
        $stepId = $this->dispatcher->getParam('stepId');

        if (!$stepId){
            return $this->redirectError($message, $backUrl);
        }

        $subs = \Admin\Models\Subscribe::find([
            'stepId = :stepId:',
            'bind' =>['stepId' =>$stepId],
            'order' => 'status DESC, updatedAt DESC',
            ]);

        if (!$subs) {
            return $this->redirectError($message, $backUrl);
        }
        $this->view->subs = $subs;
    }

    public function checkinForceListAction()
    {
        $backUrl = "/admin/etapas/lista-de-presenca";
        $message = "Sua Requisição não pode ser processada no momento.";
        $message .= '<br/>Por gentileza tente novamente.';
        
        if (!$this->request->isPost())
        {
            return $this->redirectError($message, $backUrl);
        }

        $post = $this->request->getPost();

        if (!$post){
            return $this->redirectError($message, $backUrl);
        }

        $sub = \Admin\Models\Subscribe::findFirst($post['subId']);

        if (!$sub){
            return $this->redirectError($message, $backUrl);
        }

        $sub->status = 2;
        $sub->updatedAt = Date('Y-m-d H:m:s');

        if (!$sub->save()) {
            $messages = '';
            foreach ($sub->getMessages() as $message) {
                $messages .= $message . '<br/>';
            }
            return $this->redirectError($messages, $backUrl);
        }

        $message = "Check-In realizado com sucesso!";
        $this->flashSession->success($message);
        return $this->response->redirect($post['url']);
    }


    public function forceAction()
    {
        $backUrl = "/admin/ferramentas";
        $message = "Sua Requisição não pode ser processada no momento.";
        $message .= '<br/>Por gentileza tente novamente.';
        
        if (!$this->request->isPost())
        {
            return $this->redirectError($message, $backUrl);
        }

        $post = $this->request->getPost();

        if (!$post){
            return $this->redirectError($message, $backUrl);
        }

        $sub = \Admin\Models\Subscribe::findFirst([
            'stepId = :stepId: and teamId = :teamId: and status = 1',
            'bind' => [
                'stepId' => $post['stepId'],
                'teamId' => $post['teamId'],
            ],
        ]);
        if (!$sub){
            $sub = new \Admin\Models\Subscribe();
            $sub->createData($post);
        }

        $sub->status = 2;
        $sub->updatedAt = Date('Y-m-d H:m:s');

        if (!$sub->save()) {
            $messages = '';
            foreach ($sub->getMessages() as $message) {
                $messages .= $message . '<br/>';
            }
            return $this->redirectError($messages, $backUrl);
        }

        $message = "Check-In realizado com sucesso!";
        $this->flashSession->success($message);
        return $this->response->redirect('/admin/ferramentas');
    }

    public function generateCodeAction()
    {

        $this->view->disable();
        $rand = rand(0, 1000);

        $extra = [
            "matchID" => $rand,
            "orgID" => $rand,
            "tournamentID" => $rand,
        ];

        $name = 'sg_msl_game_'.$rand;
        $pass = $name;
        $conf = [
                    'name' => $name,
                    'password' => $pass,
                    'report' => 'http://sg.com.br/report.php',
                    'extra' => json_encode($extra),
                ];

        $json_base64 = base64_encode(json_encode($conf, JSON_UNESCAPED_SLASHES));
        $url_format = "pvpnet://lol/customgame/joinorcreate/%s/%s/%s/%s/%s";
        $return = [
            'name' => $name,
            'password' => $pass,
            'code' => sprintf($url_format, 'map11', 'pick6', 'team5', 'specALL', $json_base64),
        ];

        echo (sprintf($url_format, 'map11', 'pick6', 'team5', 'specALL', $json_base64));
    }

    public function victoryReportAction()
    {
        $backUrl = "/admin/etapas/";
        $message = "Sua Requisição não pode ser processada no momento.";
        $message .= '<br/>Por gentileza tente novamente.';
        
        $stepId = $this->dispatcher->getParam('stepId');

        if (!$stepId){
            return $this->redirectError($message, $backUrl);
        }

        $reports = \Admin\Models\VictoryReport::find([
            'stepId = :stepId:',
            'bind' =>['stepId' =>$stepId],
            'order' => 'status ASC, createdAt DESC',
            ]);

        if (!$reports) {
            return $this->redirectError($message, $backUrl);
        }
        $this->view->reports = $reports;
    }

    public function validateReportAction()
    {
        $backUrl = "/admin/etapas/";
        $message = "Sua Requisição não pode ser processada no momento.";
        $message .= '<br/>Por gentileza tente novamente.';
        
        $reportId = $this->dispatcher->getParam('reportId');

        if (!$reportId){
            return $this->redirectError($message, $backUrl);
        }

        $report = \Admin\Models\VictoryReport::findFirst($reportId);

        if (!$report) {
            return $this->redirectError($message, $backUrl);
        }
        $report->status = 2;

        if (!$report->save()) {
            $messages = '';
            foreach ($report->getMessages() as $message) {
                $messages .= $message . '<br/>';
            }
            return $this->redirectError($messages, $backUrl);
        }

        $message = "Report validado com sucesso!";
        $this->flashSession->success($message);
        return $this->response->redirect("admin/etapas/report-vitoria/{$report->stepId}");
    }

    public function listAttendanceAction()
    {
        $this->view->steps = \Admin\Models\Step::find('status = 1');
    } 

    public function editStepAction()
    {

        $this->view->steps = \Admin\Models\Step::find('status = 1');
    }

    public function editChampionshipAction()
    {
        $this->view->championships = \Admin\Models\Championship::find();
    }

    public function newStepAction()
    {
        $this->view->championships = \Admin\Models\Championship::find();        
    }

    public function toolsAction()
    {
        $this->view->teams = \Admin\Models\Team::find();
        $this->view->steps = \Admin\Models\Step::find('status = 1');
    }
    public function reportAction()
    {
        $this->view->steps = \Admin\Models\Step::find('status = 1');
        $this->view->closedSteps = \Admin\Models\Step::find('status = 2');
    }

    public function newChampionshipAction()
    {

    }

    public function stepsAction()
    {

    }

    public function indexAction()
    {

    }

    public function championshipAction()
    {
        
    }

}