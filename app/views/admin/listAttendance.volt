<section class="page-top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/" class="home">Social Gamers</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/admin" class="home">Admin</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/admin/etapas" class="home">Etapas</a>
						</span>
					</li>
					<li class="current_item">
						<span typeof="v:Breadcrumb">
							<span property="v:title">Lista de  Presença</span>
						</span>
					</li>
                </ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2 class="yellow">Lista de  Presença</h2>
			</div>
		</div>
	</div>
</section>

<div class="container">
	<div class="row">
       <div class="col-md-12">
    		<div class="center">
				<div class="col-md-12">
                	<p class="featured lead">
                		Selecione a Etapa::
					</p>	
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?php $this->flashSession->output() ?>
		</div>		

		{% for step in steps %}
			<div class="col-md-3">
				<a href="/admin/etapas/lista-de-presenca/{{step.id}}" class="push-top push-bottom">
					<div class="featured-box featured-box-yellow" style="height: 343px;">
						<div class="box-content">
							<i class="icon-featured icon">
								{% if step.championship.image is defined %}
									<img class="img-circle img-responsive img-championship" src="/files/championship/{{step.championship.id}}/{{step.championship.image}}">
								{% else %}
									<img class="img-circle img-responsive img-championship"  src="/img/team/team-1.png">
								{% endif %}
							</i>
							<h4>{{step.name}}</h4>
						</div>
					</div>
				</a>
			</div>
		{% endfor %}
	</div>
</div>