<section class="page-top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/" class="home">Social Gamers</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/admin" class="home">Admin</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/admin/campeonatos" class="home">Campeonatos</a>
						</span>
					</li>
					<li class="current_item">
						<span typeof="v:Breadcrumb">
							<span property="v:title">Novo Campeonato</span>
						</span>
					</li>
                </ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2 class="yellow">Novo Campeonato</h2>
			</div>
		</div>
	</div>
</section>

<div class="container">
	<div class="row">
       <div class="col-md-12">
    		<div class="center">
				<div class="col-md-12">
                	<p class="featured lead">
                		Novo Campeonato
						<br/>
					</p>	
				</div>
			</div>
		</div>
	</div>
	<div class = "row">	
		<div class="featured-box featured-box-yellow default">
			<div class="box-content">
				<form id="editProfile" action="/admin/campeonatos/criar" method="post" enctype="multipart/form-data">
					<div class="col-md-12">
						<div class="row">
							<div class="form-group col-md-12">
								<label for="name">Nome do Campeonato:</label>
								<input type="text" value="" name="name"  class="form-control input-lg" id="name"/>
							</div>
							<div class="form-group">
								<label for="about">O que é:</label>
								<textarea name="about" id="about" class="editor form-control" rows="20" style="height: 150px;"></textarea>
							</div>

							<div class="form-group col-md-6">
								<label for="rules">Regras:</label>
								<textarea name="rules" id="rules" class="editor form-control" rows="20" style="height: 150px;"></textarea>
							</div>
							<div class="form-group col-md-6">
								<label for="image">Foto de Capa - 572 x 572</label> 
								<input id="image" type="file" name="image" class="form-control input-lg">
							</div>
							<div class="form-group col-md-6">
									<label for="menuImage">Foto do Menu - 260 x 75</label> 
									<input id="menuImage" type="file" name="menuImage" class="form-control input-lg">
							</div>
						</div>
						<hr class="tall"/>
					</div>
					<div class="row">
						<div class="col-md-4 col-md-offset-8">
							<input type="submit" value="Criar Campeonato" id="salvar" class="btn-lg btn yellow-btn" />
							<a href="/times/meu-time">
							<input type="button" value="Voltar" id="voltar" class="btn-lg btn yellow-btn" />
							</a>
						</div>
					</div>
				</form>
			</div>
    	</div>
	</div>
</div>