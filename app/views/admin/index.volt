<section class="page-top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a rel="v:url" property="v:title" title="Go to Social Gamers." href="/" class="home">Social Gamers</a>
						</span>
					</li>
					<li class="current_item">
						<span typeof="v:Breadcrumb">
							<span property="v:title">Admin</span>
						</span>
					</li>
                </ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2 class="yellow">Painel Administrativo</h2>
			</div>
		</div>
	</div>
</section>

<div class="container">
	<div class="row">
		<?php $this->flashSession->output() ?>

		<div class="row featured-boxes">
			<div class="col-md-3">
				<a href="/admin/campeonatos">
					<div class="featured-box featured-box-primary" style="height: 343px;">
						<div class="box-content">
							<i class="icon-featured icon fa fa-trophy"></i>
							<h4>Campeonatos</h4>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-3">
				<a href="/admin/etapas">
					<div class="featured-box featured-box-secundary" style="height: 343px;">
						<div class="box-content">
							<i class="icon-featured icon fa fa-book"></i>
							<h4>Etapas</h4>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-3">
				<a href="/admin/ranking">
					<div class="featured-box featured-box-tertiary" style="height: 343px;">
						<div class="box-content">
							<i class="icon-featured icon fa fa-sitemap"></i>
							<h4>Ranking</h4>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-3">
				<a href="admin/medalhas">	
					<div class="featured-box featured-box-quartenary" style="height: 343px;">
						<div class="box-content">
							<i class="icon-featured icon fa fa-certificate"></i>
							<h4>Medalhas</h4>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-3">
				<a href="admin/ferramentas">	
					<div class="featured-box featured-box-yellow" style="height: 343px;">
						<div class="box-content">
							<i class="icon-featured icon fa fa-cogs"></i>
							<h4>Ferramentas</h4>
						</div>
					</div>
				</a>
			</div>
		</div>
	</div>
</div>
