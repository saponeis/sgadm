<section class="page-top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/" class="home">Social Gamers</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/admin" class="home">Admin</a>
						</span>
					</li>

					<li class="current_item">
						<span typeof="v:Breadcrumb">
							<span property="v:title">Campeonatos</span>
						</span>
					</li>
                </ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2 class="yellow">Campeonatos</h2>
			</div>
		</div>
	</div>
</section>

<div class="container">
	<div class="row">
		<?php $this->flashSession->output() ?>
		<div class="row featured-boxes">
			<div class="col-md-3">
				<a href="/admin/campeonatos/novo">
					<div class="featured-box featured-box-primary" style="height: 343px;">
						<div class="box-content">
							<i class="icon-featured icon fa fa-exclamation-triangle"></i>
							<h4>Novo Campeonato</h4>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-3">
				<a href="/admin/campeonatos/editar">
					<div class="featured-box featured-box-tertiary" style="height: 343px;">
						<div class="box-content">
							<i class="icon-featured icon fa fa fa-pencil-square-o"></i>
							<h4>Editar Campeonatos</h4>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-3">
				<a href="/admin/etapas">
					<div class="featured-box featured-box-quartenary" style="height: 343px;">
						<div class="box-content">
							<i class="icon-featured icon fa fa fa-book"></i>
							<h4>Etapas</h4>
						</div>
					</div>
				</a>
			</div>

		</div>
	</div>
</div>