<section class="page-top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/" class="home">Social Gamers</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/admin" class="home">Admin</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/admin/etapas" class="home">Etapas</a>
						</span>
					</li>
					<li class="current_item">
						<span typeof="v:Breadcrumb">
							<span property="v:title">Report de Vitória</span>
						</span>
					</li>
                </ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2 class="yellow">Report de Vitória</h2>
			</div>
		</div>
	</div>
</section>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?php $this->flashSession->output() ?>
		</div>
		<div class="col-md-12 table-responsive">
		<table class="table table-hover table-bordered">
			<thead>
				<tr>
				    <td class ="text-center">Time Vencedor</td>
				    <td class ="text-center">Time Derrotado</td>
				    <td class ="text-center">Print URL</td>
				    <td class ="text-center">Atualizado em </td>
				    <td class ="text-center">Ação</td>
				</tr>
			</thead>
			<tbody>
				{% for report in reports %}
					{% if report.status == 2 %}
						<tr class="success">
					{% else %}
						<tr class="danger">
					{% endif %}
					<td class="text-center">
						<a href="/times/perfil/{{report.teamvictory.id}}" target="_blank" style='color: #000;'>
							{{report.teamvictory.name}}
						</a>
					</td>
					<td class="text-center">
						<a href="/times/perfil/{{report.teamdefeated.id}}" target="_blank" style='color: #000;'>
							{{report.teamdefeated.name}}
						</td>
					<td class="text-center">
						<a href="{{report.printUrl}}" target="_blank" style='color: #000;'>
							{{report.printUrl}}
						</a>
					</td>
					<td class="text-center">{{report.updatedAt}}</td>
 				    <td class="text-center">
						{% if report.status == 2 %}
	 				    	<span class="btn btn-sm btn-primary yellow-btn">Validado</span>
						{% else %}
	 				    	<a href="/admin/etapas/report/validar/{{report.id}}" class="btn btn-sm btn-primary yellow-btn">Validar</a>
						{% endif %}
 				    </td>
				</tr>
				{% endfor %}
			</tbody>
		</table>
	</div>
</div>