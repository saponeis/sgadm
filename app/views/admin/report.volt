<section class="page-top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/" class="home">Social Gamers</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/admin" class="home">Admin</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/admin/etapas" class="home">Etapas</a>
						</span>
					</li>
					<li class="current_item">
						<span typeof="v:Breadcrumb">
							<span property="v:title">Report de Vitória</span>
						</span>
					</li>
                </ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2 class="yellow">Report de Vitória</h2>
			</div>
		</div>
	</div>
</section>

<div class="container">
	<div class="row">
       <div class="col-md-12">
    		<div class="center">
				<div class="col-md-12">
                	<p class="featured lead">
                		Selecione a Etapa para ver os Reports
						<br/>
					</p>	
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?php $this->flashSession->output() ?>
		</div>		
		<div class="row">
			<div class="pricing-table">			
				{% for step in steps %}
					<div class="col-md-3">
						<a href="/admin/etapas/report-vitoria/{{step.id}}" class="push-top push-bottom" data-toggle="modal" data-target="#{{step.id}}">
							<div class="plan">
								<h3>
									{{step.championship.name}}
									<span>
										{% if step.championship.image is defined %}
										<img class="img-circle img-responsive img-championship" src="/files/championship/{{step.championship.id}}/{{step.championship.image}}">
										{% else %}
										<img class="img-circle img-responsive img-championship"  src="/img/team/team-1.png">
										{% endif %}
									</span>
								</h3>
								<a  href="/admin/etapas/report-vitoria/{{step.id}}" class="btn btn-lg btn-primary yellow-btn">{{step.name}}</a>
							</div>
						</a>
					</div>
				{% endfor %}
			</div>
		</div>
	</div>
</div><div class="container">
	<div class="row">
       <div class="col-md-12">
    		<div class="center">
				<div class="col-md-12">
                	<p class="featured lead alert alert-danger">
                		Etapas Encerradas
						<br/>
					</p>	
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="row">
			<div class="pricing-table">			
				{% for step in closedSteps %}
					<div class="col-md-3">
						<a href="/admin/etapas/report-vitoria/{{step.id}}" class="push-top push-bottom" data-toggle="modal" data-target="#{{step.id}}">
							<div class="plan">
								<h3>
									{{step.championship.name}}
									<span>
										{% if step.championship.image is defined %}
										<img class="img-circle img-responsive img-championship" src="/files/championship/{{step.championship.id}}/{{step.championship.image}}">
										{% else %}
										<img class="img-circle img-responsive img-championship"  src="/img/team/team-1.png">
										{% endif %}
									</span>
								</h3>
								<a  href="/admin/etapas/report-vitoria/{{step.id}}" class="btn btn-lg btn-primary yellow-btn">{{step.name}}</a>
							</div>
						</a>
					</div>
				{% endfor %}
			</div>
		</div>
	</div>
</div>