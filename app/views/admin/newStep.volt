    {{stylesheet_link('vendor/bootstrap-datepicker/css/datepicker.css')}}
    {{stylesheet_link('vendor/bootstrap-timepicker/css/bootstrap-timepicker.css')}}


<section class="page-top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/" class="home">Social Gamers</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/admin" class="home">Admin</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/admin/campeonatos" class="home">Campeonatos</a>
						</span>
					</li>
					<li class="current_item">
						<span typeof="v:Breadcrumb">
							<span property="v:title">Nova Etapa</span>
						</span>
					</li>
                </ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2 class="yellow">Nova Etapa</h2>
			</div>
		</div>
	</div>
</section>

<div class="container">
	<div class="row">
       <div class="col-md-12">
    		<div class="center">
				<div class="col-md-12">
                	<p class="featured lead">
                		Selecione o Campeonato que deseja criar uma nova etapa
						<br/>
					</p>	
				</div>
			</div>
		</div>
	</div>
	<div class="row">
	<div class="col-md-12">
		<?php $this->flashSession->output() ?>
	</div>		

	{% for championship in championships %}
		<div class="col-md-3">
			<a href="#" class="push-top push-bottom" data-toggle="modal" data-target="#{{championship.id}}">
				<div class="featured-box featured-box-yellow" style="height: 343px;">
					<div class="box-content">
						<i class="icon-featured icon">
							{% if championship.image is defined %}
								<img class="img-circle img-responsive img-championship" src="/files/championship/{{championship.id}}/{{championship.image}}">
							{% else %}
								<img class="img-circle img-responsive img-championship"  src="/img/team/team-1.png">
							{% endif %}
						</i>
						<h4>{{championship.name}}</h4>
					</div>
				</div>
			</a>
		</div>
		<div class="modal fade" id="{{championship.id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-body">
						<div class="featured-box featured-box-yellow default">
							<div class="box-content">
								<form id="editProfile" action="/admin/etapas/criar" method="post" enctype="multipart/form-data">
									<div class="col-md-12">
										<div class="row">
											<div class="form-group">
												<label for="name">Nome:</label>
												<input type="text" value="" name="name"  class="form-control input-lg" id="name"/>
											</div>
											<div class="form-group">
												<input type="hidden" name='championshipId' value="{{championship.id}}"/>
												<label for="startDate">Data de Inicio:</label>
												<input type="text" value="" name="startDate" id="startDate"  class="startDate form-control input-lg"/>
											</div>
											<div class="form-group">
												<label for="startTime">Hora de Inicio:</label>
												<input type="text" value="" name="startTime"  class="form-control input-lg startTime" id="startTime"/>
											</div>
											<div class="form-group">
												<label for="checkinDate">Data de Check-in:</label>
												<input type="text" value="" name="checkinDate"  class="form-control input-lg checkinDate" id="checkinDate"/>
											</div>
											<div class="form-group">
												<label for="checkinTime">Hora do Inicio do Check-in:</label>
												<input type="text" value="" name="checkinTime"  class="form-control input-lg checkinTime" id="checkinTime"/>
											</div>
											<div class="form-group col-md-6">
												<label for="brackets">Chaves:</label>
												<textarea name="brackets" id="brackets" class="editor form-control" rows="20" style="height: 150px;"></textarea>
											</div>
										</div>
										<hr class="tall"/>
									</div>
									<div class="row">
										<div class="col-md-12">
											<input type="submit" value="Criar" id="salvar" class="btn-lg btn yellow-btn" />
											<button type="button" class="btn-lg btn yellow-btn" data-dismiss="modal">Voltar</button>
										</div>
									</div>
								</form>
							</div>
				    	</div>
					</div>
				</div>
			</div>
		</div>
	{% endfor %}
</div>

{{ javascript_include('vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}
{{ javascript_include('vendor/bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR.js') }}

{{ javascript_include('vendor/bootstrap-timepicker/js/bootstrap-timepicker.js') }}
{{ javascript_include('js/views/view.newStep.js') }}