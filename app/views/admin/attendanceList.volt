<section class="page-top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/" class="home">Social Gamers</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/admin" class="home">Admin</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/admin/etapas" class="home">Etapas</a>
						</span>
					</li>
					<li class="current_item">
						<span typeof="v:Breadcrumb">
							<span property="v:title">Lista de Presença</span>
						</span>
					</li>
                </ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2 class="yellow">Lista de  Presença</h2>
			</div>
		</div>
	</div>
</section>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?php $this->flashSession->output() ?>
		</div>
		<div class="col-md-12 table-responsive">
		<table class="table table-hover table-bordered">
			<thead>
				<tr>
				    <td class ="text-center">Time</td>
				    <td class ="text-center">Capitão</td>
				    <td class ="text-center">Email</td>
				    <td class ="text-center">Status</td>
				    <td class ="text-center">Atualizado em </td>
				    <td class ="text-center">Forçar Check-In</td>
				</tr>
			</thead>
			<tbody>
				{% for sub in subs %}
					{% if sub.status == 2 %}
						<tr class="success">
					{% else %}
						<tr class="danger">
					{% endif %}
					 <td class ="text-center">{{sub.team.name}}</td>
					 <td class ="text-center">{{sub.team.capitan.summoner}}</td>
					 <td class ="text-center">{{sub.team.capitan.email}}</td>
					 {% if sub.status == 2 %}
					 	<td class ="text-center">Check-In Realizado</td>
					 {% else %}
					 	<td class ="text-center"></td>
					 {% endif %}
					 <td class ="text-center">{{sub.updatedAt}}</td>
					 {% if sub.status == 2 %}
					 	<td class ="text-center"></td>
					 {% else %}
					 	<td class ="text-center">
				 			<form action="/admin/forcarcheckinlist" method="POST" style="margin-bottom:0px">
					 		<input type="hidden" name="subId" value="{{sub.id}}"/>
					 		<input type="hidden" name="teamId" value="{{sub.team.id}}"/>
					 		<input type="hidden" name="url" value="admin/etapas/lista-de-presenca/{{sub.step.id}}"/>
					 		<input class="btn btn-sm btn-warning" type ="submit" value="Forçar Check-In"/>
					 		</form>
					 	</td>
					 {% endif %}
				</tr>
				{% endfor %}
			</tbody>
		</table>
	</div>
</div>