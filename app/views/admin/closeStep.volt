    {{stylesheet_link('vendor/bootstrap-datepicker/css/datepicker.css')}}
    {{stylesheet_link('vendor/bootstrap-timepicker/css/bootstrap-timepicker.css')}}


<section class="page-top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/" class="home">Social Gamers</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/admin" class="home">Admin</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/admin/etapas" class="home">Etapas</a>
						</span>
					</li>
					<li class="current_item">
						<span typeof="v:Breadcrumb">
							<span property="v:title">Encerrar etapa</span>
						</span>
					</li>
                </ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2 class="yellow">Encerrar Etapa</h2>
			</div>
		</div>
	</div>
</section>

<div class="container">
	<div class="row">
       <div class="col-md-12">
    		<div class="center">
				<div class="col-md-12">
                	<p class="featured lead">
                		Selecione a Etapa que deseja encerrar:
                		<div class="alert alert-danger"> ATENÇÃO - Após encerrar uma etapa, nãó é possível alterar seu status. Proceda com cautela.</div>
					</p>	
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?php $this->flashSession->output() ?>
		</div>		

		{% for step in steps %}
			<div class="col-md-3">
				<a href="/admin/etapas/encerrar/{{step.id}}" class="push-top push-bottom">
					<div class="featured-box featured-box-yellow" style="height: 343px;">
						<div class="box-content">
							<i class="icon-featured icon">
								{% if step.championship.image is defined %}
									<img class="img-circle img-responsive img-championship" src="/files/championship/{{step.championship.id}}/{{step.championship.image}}">
								{% else %}
									<img class="img-circle img-responsive img-championship"  src="/img/team/team-1.png">
								{% endif %}
							</i>
							<h4>{{step.name}}</h4>
						</div>
					</div>
				</a>
			</div>
		{% endfor %}
	</div>
</div>

{{ javascript_include('vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}
{{ javascript_include('vendor/bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR.js') }}

{{ javascript_include('vendor/bootstrap-timepicker/js/bootstrap-timepicker.js') }}
{{ javascript_include('js/views/view.newStep.js') }}