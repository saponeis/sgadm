<section class="page-top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a rel="v:url" property="v:title" title="Go to Social Gamers." href="/" class="home">Social Gamers</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a rel="v:url" property="v:title" title="Go to Social Gamers." href="/admin" class="home">Admin</a>
						</span>
					</li>
					<li class="current_item">
						<span typeof="v:Breadcrumb">
							<span property="v:title">Ferramentas</span>
						</span>
					</li>                </ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2 class="yellow">Ferramentas Administrativo</h2>
			</div>
		</div>
	</div>
</section>

<div class="container">
	<div class="row">
		<?php $this->flashSession->output() ?>
		<div class="row featured-boxes">
			<div class="col-md-3">
				<a href="#" class="push-top push-bottom" data-toggle="modal" data-target="#force">
					<div class="featured-box featured-box-primary" style="height: 343px;">
						<div class="box-content">
							<i class="icon-featured icon fa fa-trophy"></i>
							<h4>Forçar Inscrição + Chekin</h4>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-3">
				<a href="#" class="push-top push-bottom" data-toggle="modal" data-target="#tournamentCode">
					<div class="featured-box featured-box-tertiary" style="height: 343px;">
						<div class="box-content">
							<i class="icon-featured icon fa fa-book"></i>
							<h4>Código de Torneio</h4>
						</div>
					</div>
				</a>
			</div>
		</div>
	</div>
</div>


<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="modal fade" id="force" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-body">
							<div class="featured-box featured-box-yellow default">
								<div class="box-content">
									<form id="editProfile" action="/admin/forcar" method="post">
										<div class="col-md-12">
											<div class="row">
												<div class="form-group">
													<label for="teamId">Time:</label>
													<select name="teamId">
													{% for team in teams %}
	  													<option value="{{team.id}}">
	  														{{team.name}}
	  													</option>
													{% endfor %}
													</select>
												</div>
												<div class="form-group">
													<label for="stepId">Etapa:</label>
													<select name="stepId">
														{% for step in steps %}
		  													<option value="{{step.id}}">
		  														{{step.championship.name}} - {{step.name}}
		  													</option>
														{% endfor %}
													</select>
												</div>
											</div>
											<hr class="tall"/>
										</div>
										<div class="row">
											<div class="col-md-12">
												<input type="submit" value="Forçar!" id="salvar" class="btn-lg btn yellow-btn" />
												<button type="button" class="btn-lg btn yellow-btn" data-dismiss="modal">Voltar</button>
											</div>
										</div>
									</form>
								</div>
					    	</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="tournamentCode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-body">
							<div class="featured-box featured-box-yellow default">
								<div class="box-content">
									<div class="col-md-12">
										<div class="row">
											<div class="form-group">
											<textarea id='tournamentCodeText' class="form-control" rows="3"  data-plugin-textarea-autosize="true" style="overflow: hidden; word-wrap: break-word; height: 150px;">
											
											</textarea>
											</div>
										</div>
										<hr class="tall"/>
									</div>
									<div class="row">
										<div class="col-md-12">
											<button type="button" id="tournamentCodeGen" class="btn-lg btn yellow-btn">Gerar!</button>
											<button type="button" class="btn-lg btn yellow-btn" data-dismiss="modal">Voltar</button>
											</div>
										</div>
									</form>
								</div>
					    	</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
{{ javascript_include('js/views/view.tools.js') }}