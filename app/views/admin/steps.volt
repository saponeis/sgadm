<section class="page-top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/" class="home">Social Gamers</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/admin" class="home">Admin</a>
						</span>
					</li>

					<li class="current_item">
						<span typeof="v:Breadcrumb">
							<span property="v:title">Etapas</span>
						</span>
					</li>
                </ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2 class="yellow">Etapas</h2>
			</div>
		</div>
	</div>
</section>

<div class="container">
	<div class="row">
		<?php $this->flashSession->output() ?>
		<div class="row featured-boxes">
			<div class="col-md-3">
				<a href="/admin/etapas/nova">
					<div class="featured-box featured-box-primary" style="height: 343px;">
						<div class="box-content">
							<i class="icon-featured icon fa fa-exclamation-triangle"></i>
							<h4>Nova Etapa</h4>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-3">
				<a href="/admin/etapas/editar">
					<div class="featured-box featured-box-tertiary" style="height: 343px;">
						<div class="box-content">
							<i class="icon-featured icon fa fa fa-pencil-square-o"></i>
							<h4>Editar Etapa</h4>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-3">
				<a href="/admin/etapas/encerrar/">
					<div class="featured-box featured-box-quartenary" style="height: 343px;">
						<div class="box-content">
							<i class="icon-featured icon fa fa fa-book"></i>
							<h4>Encerrar Etapas</h4>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-3">
				<a href="/admin/etapas/report">
					<div class="featured-box featured-box-primary" style="height: 343px;">
						<div class="box-content">
							<i class="icon-featured icon fa fa fa-book"></i>
							<h4>Report de Vitória</h4>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-3">
				<a href="/admin/etapas/lista-de-presenca">
					<div class="featured-box featured-box-secundary" style="height: 343px;">
						<div class="box-content">
							<i class="icon-featured icon fa fa fa-book"></i>
							<h4>Lista de Presença</h4>
						</div>
					</div>
				</a>
			</div>
		</div>
	</div>
</div>