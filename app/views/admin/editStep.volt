<section class="page-top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/" class="home">Social Gamers</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/admin" class="home">Admin</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/admin/etapas" class="home">Etapas</a>
						</span>
					</li>
					<li class="current_item">
						<span typeof="v:Breadcrumb">
							<span property="v:title">Editar Etapa</span>
						</span>
					</li>
                </ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2 class="yellow">Editar Etapa</h2>
			</div>
		</div>
	</div>
</section>

<div class="container">
	<div class="row">
       <div class="col-md-12">
    		<div class="center">
				<div class="col-md-12">
                	<p class="featured lead">
                		Selecione a Etapa que deseja editar
						<br/>
					</p>	
				</div>
			</div>
		</div>
	</div>
	<div class="row">
	<div class="col-md-12">
		<?php $this->flashSession->output() ?>
	</div>		

	<div class="row">
		<div class="pricing-table">			
			{% for step in steps %}
				<div class="col-md-3">
					<a href="#" class="push-top push-bottom" data-toggle="modal" data-target="#{{step.id}}">

						<div class="plan">
							<h3>
								{{step.championship.name}}
								<span>
									{% if step.championship.image is defined %}
									<img class="img-circle img-responsive img-championship" src="/files/championship/{{step.championship.id}}/{{step.championship.image}}">
									{% else %}
									<img class="img-circle img-responsive img-championship"  src="/img/team/team-1.png">
									{% endif %}
								</span>
							</h3>
							<a data-toggle="modal" data-target="#{{step.id}}" class="btn btn-lg btn-primary yellow-btn" href="#">{{step.name}}</a>
						</div>
					</a>
				</div>
				<div class="modal fade" id="{{step.id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								<h2 class="modal-title" id="myModalLabel">Editar Campeonato</h2>
							</div>
							<div class="modal-body">
								<div class="featured-box featured-box-yellow default">
									<div class="box-content">
										<form id="editProfile" action="/admin/etapas/salvar" method="post" enctype="multipart/form-data">
											<div class="col-md-12">
												<div class="row">
													<div class="form-group">
														<label for="name">Nome:</label>
														<input type="text" value="{{step.name}}" name="name" class="form-control input-lg" id="name"/>
													</div>
													<div class="form-group">
														<input type="hidden" name='championshipId' value="{{step.championship.id}}"/>
														<input type="hidden" name='stepId' value="{{step.id}}"/>
														<label for="startDate">Data de Inicio:</label>
														<input type="text" value="{{ step.getStartDate() }}" name="startDate" id="startDate"  class="startDate form-control input-lg"/>
													</div>
													<div class="form-group">
														<label for="startTime">Hora de Inicio:</label>
														<input type="text" value="{{ step.getStartTime() }}" name="startTime"  class="startTime form-control input-lg" id="startTime"/>
													</div>
													<div class="form-group">
														<label for="checkinDate">Data de Check-in:</label>
														<input type="text" value="{{ step.getCheckinDate() }} " name="checkinDate"  class="checkinDate form-control input-lg" id="checkinDate"/>
													</div>
													<div class="form-group">
														<label for="checkinTime">Hora de Inicio do Check-in:</label>
														<input type="text" value="{{ step.getCheckinTime() }}" name="checkinTime"  class="checkinTime form-control input-lg" id="checkinTime"/>
													</div>
													<div class="form-group">
														<label for="brackets">Chaves:</label>
														<textarea name="brackets" id="brackets" class="editor form-control" rows="20" style="height: 150px;">{{championship.brackets}}</textarea>
													</div>
												</div>
												<hr class="tall"/>
											</div>
											<div class="row">
												<div class="col-md-12">
													<input type="submit" value="Salvar" id="salvar" class="btn-lg btn yellow-btn" />
													<button type="button" class="btn-lg btn yellow-btn" data-dismiss="modal">Voltar</button>
												</div>
											</div>
										</form>
									</div>
						    	</div>
							</div>
						</div>
					</div>
				</div>
			{% endfor %}
		</div>
	</div>

</div>

{{ javascript_include('vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}
{{ javascript_include('vendor/bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR.js') }}
{{ javascript_include('vendor/bootstrap-timepicker/js/bootstrap-timepicker.js') }}
{{ javascript_include('js/views/view.newStep.js') }}