<section class="page-top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/" class="home">Social Gamers</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/admin" class="home">Admin</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/admin/campeonatos" class="home">Campeonatos</a>
						</span>
					</li>
					<li class="current_item">
						<span typeof="v:Breadcrumb">
							<span property="v:title">Editar Campeonatos</span>
						</span>
					</li>
                </ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2 class="yellow">Editar Campeonatos</h2>
			</div>
		</div>
	</div>
</section>

<div class="container">
	<div class="row">
       <div class="col-md-12">
    		<div class="center">
				<div class="col-md-12">
                	<p class="featured lead">
                		Selecione o Campeonato que deseja editar
						<br/>
					</p>	
				</div>
			</div>
		</div>
	</div>
	<div class="row">
	<div class="col-md-12">
		<?php $this->flashSession->output() ?>
	</div>		

	{% for championship in championships %}
		<div class="col-md-3">
			<a href="#" class="push-top push-bottom" data-toggle="modal" data-target="#{{championship.id}}">
				<div class="featured-box featured-box-yellow" style="height: 343px;">
					<div class="box-content">
						<i class="icon-featured icon">
							{% if championship.image is defined %}
								<img class="img-circle img-responsive img-championship" src="/files/championship/{{championship.id}}/{{championship.image}}">
							{% else %}
								<img class="img-circle img-responsive img-championship"  src="/img/team/team-1.png">
							{% endif %}
						</i>
						<h4>{{championship.name}}</h4>
					</div>
				</div>
			</a>
		</div>
		<div class="modal fade" id="{{championship.id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h2 class="modal-title" id="myModalLabel">Editar Campeonato</h2>
					</div>
					<div class="modal-body">
						<div class="featured-box featured-box-yellow default">
							<div class="box-content">
								<form id="editProfile" action="/admin/campeonatos/salvar" method="post" enctype="multipart/form-data">
									<div class="col-md-12">
										<div class="row">
											<div class="form-group">
												<input type="hidden" name='championshipId' value="{{championship.id}}"/>
												<label for="name">Nome do Campeonato:</label>
												<input type="text" value="{{championship.name}}" name="name"  class="form-control input-lg" id="name"/>
											</div>
											<div class="form-group">
												<label for="about">O que é:</label>
												<textarea name="about" id="about" class="editor form-control" rows="20" style="height: 150px;">{{championship.about}}</textarea>
											</div>
											<div class="form-group">
												<label for="rules">Regras:</label>
												<textarea name="rules" id="rules" class="editor form-control" rows="20" style="height: 150px;">{{championship.rules}}</textarea>
											</div>
											<div class="form-group">
												<label for="image">Foto de Capa - 585 x 585</label> 
												<input id="image" type="file" name="image" class="form-control input-lg">
											</div>
											<div class="form-group6">
													<label for="menuImage">Foto do Menu - 260 x 75</label> 
													<input id="menuImage" type="file" name="menuImage" class="form-control input-lg">
											</div>
										</div>
										<hr class="tall"/>
									</div>
									<div class="row">
										<div class="col-md-12">
											<input type="submit" value="Salvar Alterações" id="salvar" class="btn-lg btn yellow-btn" />
											<button type="button" class="btn-lg btn yellow-btn" data-dismiss="modal">Voltar</button>
										</div>
									</div>
								</form>
							</div>
				    	</div>
					</div>
				</div>
			</div>
		</div>
	{% endfor %}
</div>