
{{ javascript_include('vendor/pnotify/pnotify.custom.js') }}
{{ javascript_include('js/views/view.login.js') }}

<div role="main" class="main">
	<div class="container">
		<hr class="tall">
		<div class="row">
			<div class="col-md-12">
				<div class="row featured-boxes login">
					<div class="col-md-6">
						<div class="featured-box featured-box-yellow default">
							<div class="box-content">
								<h4>Ja tenho uma Conta</h4>
								<form action="/usuario/login" id="" method="post">
									<div class="row">
										<div class="form-group">
											<div class="col-md-12">
												<label>Email</label>
												<input type="text" required value="" name="email" class="form-control input-lg">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<div class="col-md-12">
												<a class="pull-right" href="/usuario/esqueci-senha">(Esqueceu a senha?)</a>
												<label>Senha</label>
												<input type="password" required value="" name="password" class="form-control input-lg">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<input type="submit" value="Login" class="btn btn-primary pull-right push-bottom yellow-btn" data-loading-text="Loading...">
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="featured-box featured-box-yellow default">
							<div class="box-content">
								<section class="panel form-wizard" id="w1">
									<header class="panel-heading">
										<h4>Cadastrar</h4>
									</header>
									<div class="panel-body panel-body-nopadding">
										<div class="wizard-tabs">
											<ul class="wizard-steps">
												<li class="active">
													<a href="#w1-account" data-toggle="tab" class="text-center">
														<span class="badge hidden-xs">1</span>
														Conta
													</a>
												</li>
												<li>
													<a href="#w1-profile" data-toggle="tab" class="text-center">
														<span class="badge hidden-xs">2</span>
														Perfil
													</a>
												</li>
												<li>
													<a href="#w1-confirm" data-toggle="tab" class="text-center">
														<span class="badge hidden-xs">3</span>
														Termos
													</a>
												</li>
											</ul>
										</div>
										<form class="form-horizontal" novalidate="novalidate" method="post" action="/usuario/novo">
											<div class="tab-content">
												<div id="w1-account" class="tab-pane active">
													<div class="form-group">
														<label class="col-sm-4 control-label" for="w1-summoner">Invocador</label>
														<div class="col-sm-8">
															<input type="text" class="form-control input-sm" name="summoner" id="w1-summoner" required>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-4 control-label" for="w1-email">Email</label>
														<div class="col-sm-8">
															<input type="email" class="form-control input-sm" name="email" id="w1-email" required>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-4 control-label" for="w1-password">Senha</label>
														<div class="col-sm-8">
															<input type="password" class="form-control input-sm" name="password" id="w1-password" minlength="6" required>
														</div>
													</div>
												</div>
												<div id="w1-profile" class="tab-pane">
													<div class="form-group">
														<label class="col-sm-4 control-label" for="w1-first-name">Nome</label>
														<div class="col-sm-8">
															<input type="text" class="form-control input-sm" name="firstName" id="w1-first-name" required>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-4 control-label" for="w1-last-name">Sobrenome</label>
														<div class="col-sm-8">
															<input type="text" class="form-control input-sm" name="lastName" id="w1-last-name" required>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-4 control-label" for="w1-last-name">Data de Nascimento</label>
														<div class="col-sm-8">
															<input type="text" class="form-control input-sm" name="birthdate" id="w1-birthdate" required>
														</div>
													</div>
												</div>
												<div id="w1-confirm" class="tab-pane">
													<div class="form-group">
														<label class="col-sm-4 control-label" for="w1-city">Cidade</label>
														<div class="col-sm-8">
															<input type="text" class="form-control input-sm" name="city" id="w1-city" required>
														</div>
													</div>

													<div class="form-group">
														<label class="col-sm-4 control-label" for="w1-state">Estado</label>
														<div class="col-sm-8">
															<input type="text" class="form-control input-sm" name="state" id="w1-state" required>
														</div>
													</div>
													<div class="form-group">
														<div class="col-sm-2"></div>
														<div class="col-sm-10">
															<div class="checkbox-custom">
																<input type="checkbox" name="terms" id="w1-terms" required>
																<label for="w1-terms">Eu li e aceito os
																	<a href="/termos"> termos de serviço </a>
																</label>
															</div>
														</div>
													</div>
												</div>
											</div>
										</form>
									</div>
									<div class="panel-footer">
										<ul class="pager">
											<li class="previous disabled">
												<a class='yellow-btn'><i class="fa fa-angle-left"></i> Anterior</a>
											</li>
											<li class="finish hidden pull-right">
												<a class="yellow-btn">Cadastrar</a>
											</li>
											<li class="next" >
												<a class="yellow-btn">Próximo <i class="fa fa-angle-right"></i></a>
											</li>
										</ul>
									</div>
								</section>
							</div>
				</div>
			</div>	
		</div>
	</div>
</div>
