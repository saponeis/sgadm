<div role="main" class="main">
    <section class="page-top">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumb">
						<li class="home">
							<span typeof="v:Breadcrumb">
								<a rel="v:url" property="v:title" title="Go to Social Gamers." href="/" class="home">Social Gamers</a>
							</span>
						</li>
						<li class="current_item">
							<span typeof="v:Breadcrumb">
								<span property="v:title">Parceiros</span>
							</span>
						</li>
	                </ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h2 class="yellow">Parceiros</h2>
				</div>
			</div>
		</div>
	</section>

    <div class="container">
        <div class="row">
            <div class="span12 col-md-12 column_first column_last">
        		<div class="center">
            		<h2 class="short word-rotator-title">
                		Venha fazer parte da iniciativa
                		<strong class="inverted">
                    		<span class="word-rotate" data-plugin-options='{"delay": 2000}'>
                        		<span class="word-rotate-items">
                            		<span>Social Gamers.</span>
                            		<span>Social Live.</span>
                        		</span>
                    		</span>
                		</strong>
            		</h2>
                	<div>
                    	<p class="featured lead">
                    		A Social Gamers é uma comunidade de games online especializada em criar, planejar, produzir e transmitir ações em jogos voltados ao cenário do eSports. Com nosso portal criado no inicio de 2015 a Social Gamers focou seus esforços em conteúdos voltados para os games mais consagrados do mercado.
                    	</p>
            		</div>
            	</div>
        	</div>
    	</div>
	</div>
    <div class="container">
        <div class="row">
			<hr class="tall">
			<h2 class="appear-animation pulse appear-animation-visible" data-appear-animation="pulse">Nossas Especializações</h2>
	        <div class="col-sm-6 col-md-4 column_first">
	            <div class="feature-box secundary">
	    			<div class="feature-box-icon">
	    				<i class="fa fa-sitemap"></i>
	    			</div>
	    			<div class="feature-box-info">
	    				<h4 class="shorter">Campeonatos</h4>
	    				<p class="tall">Organização de campeonatos semanais com premiações exclusivas voltadas ao amador e o semiprofissional.</p>
	    			</div>
	    		</div>
	        </div>
	        <div class="col-sm-6 col-md-4">
	            <div class="feature-box secundary">
	    			<div class="feature-box-icon">
	    				<i class="fa fa-users"></i>
	    			</div>
	    			<div class="feature-box-info">
	    				<h4 class="shorter">Staff</h4>
	    				<p class="tall">Gestão de profissionais ativos no ramo do esports para trazer um evento bem estruturado e organizado para os jogadores.</p>
	    			</div>
	    		</div>
	        </div>
	        <div class="col-sm-6 col-md-4 column_last">
	            <div class="feature-box secundary">
	    			<div class="feature-box-icon">
	    				<i class="fa fa-desktop"></i>
	    			</div>
	    			<div class="feature-box-info">
	    				<h4 class="shorter">SocialgamersTV</h4>
	    				<p class="tall">Stream de gameplay e dos eventos da Social Gamers com narração, sorteios e brincadeiras.</p>
	    			</div>
	    		</div>
	        </div>
	        <div class="col-sm-6 col-md-4 column_first">
	            <div class="feature-box secundary">
	    			<div class="feature-box-icon">
	    				<i class="fa fa-trophy"></i>
	    			</div>
	    			<div class="feature-box-info">
	    				<h4 class="shorter">Premiações</h4>
	    				<p class="tall">Campeonatos com premiações exclusivas* (premiação liberada mediante a desenvolvedora ou parceiros).</p>
	    			</div>
	    		</div>
	        </div>
	        <div class="col-sm-6 col-md-4">
	            <div class="feature-box secundary">
	    			<div class="feature-box-icon">
	    				<i class="fa fa-comments"></i>
	    			</div>
	    			<div class="feature-box-info">
	    				<h4 class="shorter">Estrutura de comunicação</h4>
	    				<p class="tall">Disponibilizamos para equipes e jogadores salas de teamspeak 3 para facilitar a comunicação entre as equipes.</p>
	    			</div>
	    		</div>
	        </div>
	        <div class="col-sm-6 col-md-4 column_last">
	            <div class="feature-box secundary">
	    			<div class="feature-box-icon">
	    				<i class="fa fa-support"></i>
	    			</div>
	    			<div class="feature-box-info">
	    				<h4 class="shorter">Suporte ao campeonato</h4>
	    				<p class="tall">Estamos 24hs disponíveis para tirar qualquer duvida ou ajudar o jogador.</p>
	    			</div>
	    		</div>
			</div>
		</div>
	</div>
	<hr class="tall"/>
	<div class="container">
		<div class="row">
			<div class="module rich-text">
				<h2>Jogos que Trabalhamos</h2>
			</div>
			<div class="module">
				<div class="col-sm-3 col-md-3 column_last">
					<img class="img-responsive" src="img/site/lol.png" alt="">
				</div>
				<div class="col-sm-3 col-md-3 column_last">
					<img class="img-responsive" src="img/site/hots1.png" alt="">
				</div>
				<div class="col-sm-3 col-md-3 column_last">
					<img class="img-responsive" src="img/site/hs.png" alt="">
				</div>
				<div class="col-sm-3 col-md-3 column_last">
					<img class="img-responsive" src="img/site/csgo1.png" alt="">
				</div>								
			</div>
		</div>
	</div>
	<hr class="tall"/>

	<div class="container">
        <div class="row">                    
        	<div class="span12 col-md-12 column_first column_last">
            	<div class="row featured-boxes">
        			<div class="col-sm-6 col-md-6 column_first">
						<div class="featured-box featured-box-primary">
							<div class="box-content">
								<i class="icon-featured fa fa-thumbs-up"></i>
								<h4>Anuncie</h4>
								<p>Gostou do projeto Social Gamers? <a href="/">Clique aqui</a> e veja nosso Mídia Kit com todas as informações de audiência, público e formatos que podemos trabalhar juntos.</p>
							</div>
						</div>
					</div>
        			<div class="col-sm-6 col-md-6 column_last">
						<div class="featured-box featured-box-quartenary">
							<div class="box-content">
								<i class="icon-featured fa fa-rocket"></i>
								<h4>Trabalhe conosco</h4>
								<p>Se você é uma pessoa dinâmica e sempre disposta a enfrentar novos desafios, esta é sua chance de fazer parte da Social Gamers. Envie seu currículo para nós <a href="http://socialgamers.com.br/contato/">clicando aqui.</a></p>
							</div>
						</div>
					</div>
				</div>
				<hr class="tall">
        	</div>
        	<div class="span6 col-md-6 column_first">
            	<div class="module module-html"><h2>Vídeo Institucional</h2>
					<iframe src="https://www.youtube.com/embed/EssLx8n6VxE" allowfullscreen="" frameborder="18" height="315" width="560"></iframe>
					<div class="clear"></div>
				</div>
        	</div>
        	<div class="span6 col-md-6 column_last">
            	<div class="module module-html"><h2>Campanha #leagueevida</h2>
					<iframe src="https://www.youtube.com/embed/bsIBb2xlQrg" allowfullscreen="" frameborder="25" height="315" width="560"></iframe>
					<div class="clear"></div>
				</div>
        	</div>
        	<div class="span12 col-md-12 column_first column_last">
	            <hr class="tall">
	            <div class="module rich-text">
	            	<h2>Conheça nossos Parceiros</h2>
				</div>
				<div class="module">
					<div class="row">
						<div class="col-sm-4 col-md-4 column_last">
							<img class="img-responsive" src="img/site/LW_01.png" alt="">
						</div>
						<div class="col-sm-4 col-md-4 column_last">
							<img class="img-responsive" src="img/site/LP_01.png" alt="">
						</div>
					</div>
				</div>
			</div>
        </div>
    </div>
</div>
<hr class="tall">
<div class="span12 col-md-12 column_first column_last yellow-bg">
    <div class ="container">
        <div class="row center counters">
            <div class="col-md-3 col-sm-6">
                <strong data-to="5682">0</strong>
                <label>Jogadores cadastrados</label>
            </div>
            <div class="col-md-3 col-sm-6">
                <strong data-to="1031">0</strong>
                <label>Times cadastros</label>
            </div>
            <div class="col-md-3 col-sm-6">
                <strong data-to="60">0</strong>
                <label>Campeonatos realizados</label>
            </div>
            <div class="col-md-3 col-sm-6">
                <strong data-to="12">0</strong>
                <label>Vitoriosos</label>
            </div>
        </div>
    </div>
</div>
