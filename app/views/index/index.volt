
<!-- Current Page CSS -->
<link rel="stylesheet" href="vendor/rs-plugin/css/settings.css" media="screen">
<link rel="stylesheet" href="vendor/circle-flip-slideshow/css/component.css" media="screen">
<!-- Specific Page Vendor and Views -->
{{ javascript_include('/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') }}
{{ javascript_include('/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') }}
{{ javascript_include('/vendor/circle-flip-slideshow/js/jquery.flipshow.js') }}
{{ javascript_include('/js/views/view.home.js') }}


<div class="slider-container">
	<div class="slider" id="revolutionSlider" data-plugin-revolution-slider data-plugin-options='{"startheight": 500}'>
		<ul>
			<li data-slotamount="7" data-masterspeed="1000" data-fstransition="fade" data-fsmasterspeed="1000" data-fsslotamount="7">
				

				<div class="tp-caption tp-fade fadeout fullscreenvideo"
					data-x="0"
					data-y="0"
					data-speed="1000"
					data-start="100"
					data-easing="Power4.easeOut"
					data-elementdelay="0.01"
					data-endelementdelay="0.1"
					data-endspeed="1500"
					data-endeasing="Power4.easeIn"
					data-autoplay="true"
					data-autoplayonlyfirsttime="false"
					data-nextslideatend="true"
					data-volume="mute"
					data-forceCover="1"
					data-aspectratio="16:9"
					data-forcerewind="on">

					<video preload="none" width="100%" height="100%" poster="/video/dark.jpg"> 
						<source src="/video/dark.mp4" type="video/mp4" />
						<source src="/video/dark.ogv" type="video/ogg">
					</video>

				<div class="tp-dottedoverlay threexthree"></div>
				</div>

				<div class="tp-caption main-label stl"
					 data-x="360"
					 data-y="180"
					 data-speed="300"
					 data-start="1000"
					 data-easing="easeOutExpo">Social
				</div>

				<div class="tp-caption main-label stl yellow"
					 data-x="550"
					 data-y="180"
					 data-speed="300"
					 data-start="1000"
					 data-easing="easeOutExpo">Gamers
				</div>

				<div class="tp-caption top-label ctr stb"
					 data-x="325"
					 data-y="250"
					 data-speed="300"
					 data-start="3000"
					 data-easing="easeOutExpo">Diversão, informação e competição para você
				</div>
				

			</li>
		</ul>
	</div>
</div>

<div class="home-intro">
</div>

<div class="container">
	<div class="row center">
		<div class="col-md-12">
			<h2 class="short word-rotator-title">
				Deixe seus jogos favoritos
				<strong class="inverted">
					<span class="word-rotate" data-plugin-options='{"delay": 2000}'>
						<span class="word-rotate-items">
							<span>mais emocionante</span>
							<span>mais técnico</span>
							<span>mais gratificante</span>
						</span>
					</span>
				</strong>
				e muito mais competitvo!
			</h2>
		</div>
	</div>
</div>

<div class="home-concept">
	<div class="container">
		<div class="row center">
			<div class="col-md-2 col-md-offset-1">
	            <div class="process-image appear-animation appear-animation-visible bounceIn" data-appear-animation="bounceIn">
	            	<a href="#"> <img src="img/noticias.png" alt="" /> </a>
					<strong>Notícias sobre games</strong>
				</div>
			</div>
			<div class="col-md-2">
				<div class="process-image" data-appear-animation="bounceIn" data-appear-animation-delay="200">
					<img src="img/campeonatos.png" alt="" />
					<strong>Campeonatos gratuitos</strong>
				</div>
			</div>
			<div class="col-md-2">
				<div class="process-image" data-appear-animation="bounceIn" data-appear-animation-delay="400">
					<img src="img/premios.png" alt="" />
					<strong>Premiações imperdíveis</strong>
				</div>
			</div>	
			<div class="col-md-4 col-md-offset-1">
				<div class="project-image">
					<div class="concept-slideshow fc-slideshow">
						<ul class="fc-slides">
							<li style="display: list-item;">
								<a href="#"><img class="img-responsive" src="img/social-gamers1.png" /></a>
							</li>
						</ul>
						<nav>
							<div class="fc-left">
								<span></span>
								<span></span>
								<span></span>
								<i class="fa fa-arrow-left"></i>
							</div>
							<div class="fc-right">
								<span></span>
								<span></span>
								<span></span>
								<i class="fa fa-arrow-right"></i>
							</div>
						</nav>
						<div style="transition: none 0s ease 0s ; transform: none; display: none;" class="fc-flip">
							<div class="fc-front">
								<div> <a href="#"><img class="img-responsive" src="img/social-gamers1.png" /> </a> </div>
                             	<div style="transition: opacity 538.462ms ease 0s; opacity: 1;" class="fc-overlay-light"> </div>
                            </div>
                            <div style="transform: rotate3d(1, 1, 0, 180deg);" class="fc-back">
                            	<div> <a href="#"> <img class="img-responsive" src="img/social-gamers1.png" /></a> </div>
                            	<div style="opacity: 0; transition: opacity 538.462ms ease 0s;" class="fc-overlay-dark"> </div>
                            </div>
                        </div>
					</div>
					<strong class="our-work">Isso é Social Gamers</strong>
				</div>
			</div>
		</div>
	</div>

	<div id="separator" class="span12 col-md-12 column_first column_last">
        <hr class="tall">
    </div>

</div>

<div class="home-intro">
    <div class="container">
        <div class="row">            
        	<div class="span12 col-md-12 column_first column_last">
            	<div class="center">
					<h2 class="short">
						<font color="white">Acompanhe a Social GamersTV e fique por dentro das novidades de nossos eventos</font>
					</h2>
					<p class="featured lead">
						<iframe src="http://www.twitch.tv/socialgamerstv/embed" scrolling="no" frameborder="0" height="378" width="620"></iframe>
					</p>
				</div>
        	</div>
    	</div>
    </div>
</div>


<div class="module">
    <div class="container">
	    <div class="row">                
        	<div class="span12 col-md-12 column_first column_last">
	            <h2>Em Breve na <strong>Social Gamers</strong> </h2>
		            <div class="row">
		       			<div class="col-sm-6 col-md-6 column_first">
		            		<div class="feature-box">
		    					<div class="feature-box-icon">
		    						<i class="fa fa-users"></i>
		    					</div>
			    				<div class="feature-box-info">
			    					<h4 class="shorter">Rede Social</h4>
			    					<p class="tall">Uma rede social projetada para gamers e fans do esports com interação, gameficação e competição.</p>
			    				</div>
		    				</div>
		        		</div>
		        		<div class="col-sm-6 col-md-6 column_last">
				            <div class="feature-box">
				    			<div class="feature-box-icon">
				    				<i class="fa fa-gears"></i>
				    			</div>
				    			<div class="feature-box-info">
				    				<h4 class="shorter">Social Manager</h4>
				    				<p class="tall">Contrate Jogadores para sua equipe ou então recrute com facilidade e agilidade.</p>
				    			</div>
				    		</div>
		        		</div>
		        	</div>
	        		<div class="row">
				        <div class="col-sm-6 col-md-6 column_first">
				            <div class="feature-box">
				    			<div class="feature-box-icon">
				    				<i class="fa fa-newspaper-o"></i>
				    			</div>
				    			<div class="feature-box-info">
				    				<h4 class="shorter">Agregador de notícias</h4>
				    				<p class="tall">Encontre as melhores matérias e informações sobre seus jogos. </p>
				    			</div>
				    		</div>
				        </div>
				        <div class="col-sm-6 col-md-6 column_last">
				            <div class="feature-box">
				    			<div class="feature-box-icon">
				    				<i class="fa fa-line-chart"></i>
				    			</div>
				    			<div class="feature-box-info">
				    				<h4 class="shorter">Ranking de ligas</h4>
				    				<p class="tall">Participe dos eventos diários e concorra a premiações mensais.</p>
				    			</div>
				    		</div>
				        </div>
			        </div>
			        <div class="row">
				        <div class="col-sm-6 col-md-6 column_first">
				            <div class="feature-box">
				    			<div class="feature-box-icon">
				    				<i class="fa fa-bar-chart"></i>
				    			</div>
				    			<div class="feature-box-info">
				    				<h4 class="shorter">Pro Stats</h4>
				    				<p class="tall">Procure builds para seus games, compare suas skills com as de profissionais do eSport.</p>
				    			</div>
				    		</div>
				        </div>
				        <div class="col-sm-6 col-md-6 column_last">
				            <div class="feature-box">
				    			<div class="feature-box-icon">
				    				<i class="fa fa-home"></i>
				    			</div>
				    			<div class="feature-box-info">
				    				<h4 class="shorter">Eventos presenciais</h4>
				    				<p class="tall">Uma feira voltada ao competitivo trazendo uma informação, diversão e competições para vocês.</p>
				    			</div>
				    		</div>
				        </div>
        			</div>
        			<hr class="tall" />
		        	<div class="center">
		   				<h2 class="short word-rotator-title">
							Não somos os únicos
							<strong class="inverted">
								<span class="word-rotate" data-plugin-options='{"delay": 2000}'>
									<span class="word-rotate-items">
										<span>animados</span>
										<span>felizes</span>
										<span>contentes</span>
										<span>empolgados</span>
										<span>mais gratificante</span>

									</span>
								</span>
							</strong>
							com o projeto da Social Gamers!
						</h2>
						<h4 class="lead tall">Veja abaixo alguns de nossos patrocinadores.</h4>
						





<div class="container">
    <div class="row">
        <div class="col-md-12 column_first column_last">
			<div id="home-carousel">
				<div>
					<img class="img-responsive" src="/img/site/parceiros/multilaser.png" alt="">
				</div>
				<div>
					<img class="img-responsive" src="/img/site/parceiros/nuvem.png" alt="">
				</div>
				<div>
					<img class="img-responsive" src="/img/site/parceiros/locaweb-home-png.png" alt="">
				</div>
				<div>
					<img class="img-responsive" src="/img/site/parceiros/gank.png" alt="">
				</div>
				<div>
					<img class="img-responsive" src="/img/site/parceiros/losd-project.png" alt="">
				</div>
				<div>
					<img class="img-responsive" src="/img/site/parceiros/tvcolosimus.png" alt="">
				</div>
			</div>
		</div>
	</div>
</div>






				</div>
			</div>
		</div>
	</div>

	<hr class="tall" />

	<div class="map-section">
        <section class="featured_section footer map">
            <div class="container"> </div>
		</section>
	</div>
</div>
