<section class="page-top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/" class="home">Social Gamers</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/campeonatos" class="home">Campeonatos</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/campeonatos/{{championship.slug}}" class="home">{{championship.name}}</a>
						</span>
					</li>
					<li class="current_item">
						<span typeof="v:Breadcrumb">
							<span property="v:title">Chave de Torneio</span>
						</span>
					</li>
                </ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2 class="yellow">Chave de Torneio</h2>
			</div>
		</div>
	</div>
</section>


<div class="container">
	<div class="row">
       <div class="col-md-12">
    		<div class="center">
				<div class="col-md-12">
                	<p class="featured lead">
                		Selecione a etapa para ver a chave de torneio:
						<br/>
					</p>	
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="pricing-table">			
			{% for step in championship.steps %}
				<div class="col-md-3">
					<a href="/campeonatos/chave/etapa/{{step.id}}" class="push-top push-bottom">
						<div class="plan">
							<h3>
								{{step.name}}
								<span>
									{% if championship.image is defined %}
									<img class="img-circle img-responsive img-championship" src="/files/championship/{{championship.id}}/{{championship.image}}">
									{% else %}
									<img class="img-circle img-responsive img-championship"  src="/img/team/team-1.png">
									{% endif %}
								</span>
							</h3>
							<button class="btn btn-lg btn-primary yellow-btn">Chave de Torneio</button>
						</div>
					</a>
				</div>
			{% endfor %}
		</div>
	</div>
</div>