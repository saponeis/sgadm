<section class="page-top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/" class="home">Social Gamers</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/campeonatos" class="home">Campeonatos</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/campeonatos/{{step.championship.slug}}" class="home">{{step.championship.name}}</a>
						</span>
					</li>
					<li class="current_item">
						<span typeof="v:Breadcrumb">
							<span property="v:title">Times</span>
						</span>
					</li>
                </ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2 class="yellow">Times</h2>
			</div>
		</div>
	</div>
</section>

<div class="container">
	<div class="row">
		{% if error %}

		<h1> {{message}} </h1>
		{% else %}
		<div class="pricing-table">			
			{% for team in teams %}
				<div class="col-md-3">
					<div class="plan most-popular">
						<h3>
							<span>
								{% if team.picture is defined %}
									<img class="img-circle img-responsive img-profile-team"  src="/files/teamprofile/{{team.id}}/profile_id_{{team.picture}}" alt="">
								{% else %}
									<img class="img-circle img-responsive" width="100"  src="/img/avatar.png" alt="">
								{% endif %}
							</span>
						</h3>
						<a class="btn btn-lg btn-primary yellow-btn" href="/times/perfil/{{team.id}}">{{team.name}}</a>
						<br/>
						<br/>
						<ul class="social-icons">
							{% if team.facebook is defined %}
								<li class="facebook"><a href="{{team.facebook}}" target="_blank" title="Facebook">Facebook</a></li>
							{% endif %}

							{% if team.youtube is defined %}
								<li class="youtube"><a href="{{team.youtube}}" target="_blank" title="youtube">Twitter</a></li>
							{% endif %}

							{% if team.twitter is defined %}
								<li class="twitter"><a href="{{team.twitter}}" target="_blank" title="twitter">Youtube</a></li>
							{% endif %}

							{% if team.twitch is defined %}
								<li class="zerply"><a href="{{team.twitch}}" target="_blank" title="Twitch">Twitch</a></li>
							{% endif %}
						</ul>		
					</div>
				</div>
			{% endfor %}
		</div>
		{% endif %}
	</div>
</div>