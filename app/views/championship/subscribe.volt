<section class="page-top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/" class="home">Social Gamers</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/campeonatos/" class="home">Campeonatos</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/campeonatos/{{championship.slug}}" class="home">{{championship.name}}</a>
						</span>
					</li>
					<li class="current_item">
						<span typeof="v:Breadcrumb">
							<span property="v:title">Inscrições!</span>
						</span>
					</li>
                </ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2 class="yellow">Inscreva Seu Time!</h2>
			</div>
		</div>
	</div>
</section>

<div class="container">
	<div class="row">
       <div class="col-md-12">
    		<div class="center">
				<div class="col-md-12">
                	<p class="featured lead">
                		Selecione a Etapa para qual deseja inscrever seu time:
						<br/>
					</p>	
				</div>
			</div>
		</div>
	</div>
	<div class="row">
	<div class="col-md-12">
		<?php $this->flashSession->output() ?>
	</div>		

	<div class="row">
		<div class="pricing-table">			
			{% for step in steps %}
				<div class="col-md-3">
					<a href="#" class="push-top push-bottom" data-toggle="modal" data-target="#{{step.id}}">

						<div class="plan">
							<h3>
								{{step.name}}
								<span>
									{% if championship.image is defined %}
									<img class="img-circle img-responsive img-championship" src="/files/championship/{{championship.id}}/{{championship.image}}">
									{% else %}
									<img class="img-circle img-responsive img-championship"  src="/img/team/team-1.png">
									{% endif %}
								</span>
							</h3>
							<button class="btn btn-lg btn-primary yellow-btn">Inscreva-se</button>
						</div>
					</a>
				</div>
				<div class="modal fade" id="{{step.id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-body">
								<div class="featured-box featured-box-yellow default">
									<div class="box-content border-yellow">
										<form id="editProfile" action="/campeonatos/inscreverse" method="post" enctype="multipart/form-data">
											<div class="row">
												<div class="col-md-12">
													{% if error %}
            										<div class="alert alert-danger">
            										ATENÇÃO - {{message}}
            										</div>
													<button type="button" class="btn-lg btn yellow-btn" data-dismiss="modal">Voltar</button>
													{% else %}
													<input type="hidden" name="stepId" value="{{step.id}}" />
													<input type="hidden" name="teamId" value="{{teamId}}" />
													<input type="submit" value="Inscrever Time" id="salvar" class="btn-lg btn yellow-btn" />
													<button type="button" class="btn-lg btn yellow-btn" data-dismiss="modal">Voltar</button>
													{%endif%}
												</div>
											</div>
										</form>
									</div>
						    	</div>
							</div>
						</div>
					</div>
				</div>
			{% endfor %}
		</div>
	</div>

</div>