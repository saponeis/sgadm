{{ javascript_include('/js/views/view.ul.js') }}

		<link rel="stylesheet" href="/vendor/owl-carousel/owl.carousel.css" media="screen">
		<link rel="stylesheet" href="/vendor/owl-carousel/owl.theme.css" media="screen">
<section class="page-top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/" class="home">Social Gamers</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/campeonatos" class="home">Campeonatos</a>
						</span>
					</li>

					<li class="current_item">
						<span typeof="v:Breadcrumb">
							<span property="v:title">University League</span>
						</span>
					</li>
                </ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2 class="yellow">University League</h2>
			</div>
		</div>
	</div>
</section>

<div class="container">
    <div class="row">
        <div class="col-md-4 column_first">
            {% if championship.image is defined %}
                <img class="img-circle img-responsive img-championship" src="/files/championship/{{championship.id}}/{{championship.image}}">
            {% endif %}
        </div>
        <div class="col-md-8 column_last">
            <h2 class="shorter">
            	<strong>University League</strong></h2><h4>Campeonato Universitário</h4>
            	<p>A <strong>University&nbsp;League</strong>&nbsp;chegou em 2016 para movimentar&nbsp;o cenário do eSports dentro das universidades. Nosso evento terá duas fases sendo a primeira fase uma seletiva de&nbsp;cada universidade participante aonde irá definir seu representante oficial, a seletiva será realizada no dia 31 de janeiro de 2016. &nbsp;Para participar basta estar cursando alguma faculdade ou curso técnico e se inscrever.</p>
				<p>
					<a href="/campeonatos/inscricao/{{championship.id}}">
						<button type="button" class="btn btn-default btn-lg">Inscreva-se para seletiva</button>
					</a>
				</p>
            <hr class="light">
            <hr class="light">
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
       <div class="col-md-12 column_first column_last">
        	<div class="center">
				<h2 class="short word-rotator-title">
				Veja algumas das
					<strong class="inverted">
						<span class="word-rotate active" data-plugin-options="{delay: 2000}" style="height: 54px;">
							<span class="word-rotate-items" style="top: -108px;">
								<span>Faculdades</span>
								<span>Universidades</span>
							</span>
						</span>
					</strong>
					que estão apoiando nosso projeto
				</h2>
			</div>
			<hr class="light">
			<hr class="light">
		</div>
	</div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12 column_first column_last">
			<div id="ul-carousel">
				<div>
					<img class="img-responsive" src="/img/site/faculdades/fei.png" alt="">
				</div>
				<div>
					<img class="img-responsive" src="/img/site/faculdades/unifieo.png" alt="">
				</div>
				<div>
					<img class="img-responsive" src="/img/site/faculdades/ufacbc.png" alt="">
				</div>
				<div>
					<img class="img-responsive" src="/img/site/faculdades/unifesp.png" alt="">
				</div>
				<div>
					<img class="img-responsive" src="/img/site/faculdades/unicamp.png" alt="">
				</div>

				<div>
					<img class="img-responsive" src="/img/site/faculdades/fatec.jpg" alt="">
				</div>
			</div>
		</div>
	</div>
</div>


<div class="featured_section call-to-action footer">
    <div class="container">
        <div class="row">
	        <div class="col-md-12 column_first column_last">
	            <h2 class="">Quer ver sua Universidade participando da University League?</h2>
				<div class="col-md-12">
					<p class="lead">
						Para sua universidade participar é muito simples! Basta você fazer o cadastro de sua universidade ou diretório acadêmico em nosso site que nossa equipe irá entrar em contato com você .
					</p>
				</div>
				<div class="col-md-2">
					<button type="button" class="btn btn-primary btn-lg yellow-btn">Inscreva sua Universidade</button>
				</div>
			</div>
		</div>
	</div>
</div>
        </div>
        <div id="builder-row-568eb1263236d" class="">
            <div class="container">
                <div class="row">
                    
        <div id="builder-column-568eb1263241a" class="span12 col-md-12 column_first column_last">
            <hr class="light"><hr class="light"><hr class="light"><h2 class="">Sobre a <strong>Seletiva</strong></h2>
        </div>
        <div id="builder-column-568eb27c76b19" class="span4 col-md-4 column_first">
            <div id="builder-module-568eb01a1460d" class="module module-html"><img class="pull-left appear-animation bounceIn appear-animation-visible" src="http://i.imgur.com/AIa615r.png" width="300" height="140" data-appear-animation="bounceIn"><div class="clear"></div></div>
        </div>
        <div id="builder-column-568eb27f36e8d" class="span8 col-md-8 column_last">
            <div id="builder-module-568eb04721748" class="module module-html"><h4>Modelo Mata Mata</h4>
<p>As rodadas da Seletiva University League e é no modelo mata mata sendo jogos melhor de um (md1) e as finais do campeonato melhor de três (md3).</p><div class="clear"></div></div>
        </div>
        <div id="builder-column-568f9de5e2110" class="span12 col-md-12 column_first column_last">
            <hr class="light">
        </div>
                </div>
            </div>
            
        </div>
        <div id="builder-row-568fb448b2dcd" class="">
            <div class="container">
                <div class="row">
                    
        <div id="builder-column-568fb448b2e84" class="span12 col-md-12 column_first column_last">
            <h2 class="">Sobre a <strong>University League</strong></h2>
        </div>
        <div id="builder-column-568fb4bdf1a08" class="span4 col-md-4 column_first">
            <div id="builder-module-568fb47162660" class="module module-html"><img class="pull-left appear-animation bounceIn appear-animation-visible" src="http://i.imgur.com/AIa615r.png" width="300" height="140" data-appear-animation="bounceIn"><div class="clear"></div></div>
        </div>
        <div id="builder-column-568fb4c389a67" class="span8 col-md-8 column_last">
            <div id="builder-module-568fb47ac030d" class="module module-html"><h4>Modelo Mata Mata</h4>
<p>As rodadas do Campeonato University League é no modelo mata mata sendo jogos melhor de três (md3) e as finais do campeonato melhor de cinco (md5).</p><div class="clear"></div></div>
        </div>
                </div>
            </div>
            
        </div>
        <div id="builder-row-568eb0f694afe" class="">
            <div class="container">
                <div class="row">
                    
        <div id="builder-column-568eb0f694bb4" class="span12 col-md-12 column_first column_last">
            <hr class="light"><hr class="light">
        </div>
        <div id="builder-column-568eb505a9e15" class="span12 col-md-12 column_first column_last">
            <div class="center">
<h2 class="short word-rotator-title">Premiações incriveis semanais como <strong class="inverted"><span class="word-rotate active" data-plugin-options="{&quot;delay&quot;: 2000}" style="height: 54px;"><span class="word-rotate-items" style="top: -4.5786px;"><span>Produtos Gamer Multilaser</span><span>Riot Points</span><span>Ryze Triunfante</span><span>Produtos Gamer Multilaser</span></span></span></strong></h2></div>
<hr class="light"><hr class="light"><div id="builder-module-568ecdd795a4c" class="module module-html"><img src="http://i.imgur.com/TlF3Goj.png"><div class="clear"></div></div><hr class="light"><hr class="light"><hr class="light">
        </div>
                </div>
            </div>
            
        </div>

<div class="container">
    <div class="row">
        <div class="col-md-12 column_first column_last">
            <h2>Conheça nossa equipe de <strong>Casters</strong></h2>
        </div>
        










<div class="col-md-3 column_first">
                <div>
                    <a href = "https://www.facebook.com/Madzzolol" target="blank">
                        <img src="/img/site/narrador/madzo.png"/>
                    </a>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="col-md-3">
                <div>
                    
                <a href="https://www.facebook.com/tvcolosimus" target="blank">
                    <img src="/img/site/narrador/colosimus.png">
                </a>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="col-md-3">
                <div>
                <a href="https://www.facebook.com/mundodocelu" target="blank">
                    <img src="/img/site/narrador/celu.png">
                    
                </a>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="col-md-3 column_last">
                <div>
                <a href="https://www.facebook.com/tibianolol" target="blank">
                    
                    <img src="/img/site/narrador/tibiano.png">
                </a>

                    <div class="clear"></div>
                </div>
            </div>


        
    </div>
</div>




<div class="container">
    <div class="row">
        <div class="col-md-12 column_first column_last">
            <hr class="light">
        </div>
<div id="builder-column-56aa22f91707b" class="span6 col-md-6 column_first">
            <h2>Regras</h2>

            <div id="builder-module-56aa22fd7c0dd" class="module toogle" data-plugin-toggle=""><section class="toggle"><label><i class="fa fa-plus"></i><i class="fa fa-minus"></i>Para Participar</label><div class="toggle-content" style="display: none;"><p>Apenas Universidades do estado de São Paulo podem se inscrever para a seletiva até dia 30 de Janeiro de 2016. A seletiva tem um limite de apenas de 8 Universidades.</p>
<p>Se faz necessário apenas:<br>
1 – Cadastro da equipe e de CADA jogador na plataforma da Social Gamers pelo nosso site (socialgamers.com.br)<br>
2 – Após o cadastro no site, o capitão do time deverá enviar um e-mail para campeonatos@socialgamers.com.br com a copia do RA e RG de cada aluno/invocador da sua equipe.<br>
3 – O capitão inscrever a equipe através do site www.socialgamers.com.br<br>
4 – Realizar o check-in no dia da etapa (Check-in abre as 9h00 da manhã e se encerra ao meio-dia do Domingo).<br>
5 – Cadastro no site da RIOT para receber a premiação (link será disponibilizado pela STAFF durante a reunião dos capitães).</p>
</div></section><section class="toggle"><label><i class="fa fa-plus"></i><i class="fa fa-minus"></i>Premiação (Seletiva)</label><div class="toggle-content"><p>As 4 (quatro) equipes melhores colocadas ao término do torneio irão receber a criação descrita abaixo:</p>
<p>1º Colocado: 3200 RP + Ryze Triunfante + Vaga para University League</p>
<p>2º Colocado: 2400 RP (por jogador) </p>
<p>3º Colocado: 1600 RP (por jogador) </p>
<p>4º Colocado: 800 RP (por jogador) </p>
<p>Lembrando:  que para receber a premiação, terão que ter um minimo de 4 equipes, as equipes deverão se registrar no site da Riot, para a etapa vigente.</p>
<p>Ps. Prazo máximo de 40 dias para entrega da premiação.</p>
</div></section><section class="toggle"><label><i class="fa fa-plus"></i><i class="fa fa-minus"></i>Premiação University League</label><div class="toggle-content"><p>As 4 (quatro) equipes melhores colocadas ao término do torneio irão receber a criação descrita abaixo:</p>
<p>1º Colocado: 3200 RP + Ryze Triunfante + 5 produtos Multilaser (por jogador) + vaga para CSL</p>
<p>2º Colocado: 2400 RP (por jogador) + vaga para CSL</p>
<p>3º Colocado: 1600 RP (por jogador) + vaga para CSL</p>
<p>4º Colocado: 800 RP (por jogador) + vaga para CSL</p>
<p>Lembrando:  que para receber a premiação, terão que ter um minimo de 4 equipes, as equipes deverão se registrar no site da Riot, para a etapa vigente.</p>
<p>Ps. Prazo máximo de 40 dias para entrega da premiação.</p>
</div></section><section class="toggle"><label><i class="fa fa-plus"></i><i class="fa fa-minus"></i>Critério de desempate University League</label><div class="toggle-content"><p>1º Critério: A equipe que tiver mais vitórias ao longo do campeonato.</p>
<p>2º Critério: Confronto direto em modo MD1 (Melhor de 1), o vencedor deste confronto ganhará o desempate.</p>
<p>Seletiva<br>
A seletiva será realizada no dia 31/01 com o inicio as 13h00 que definirá a equipe que representará cada universidade. Após o cadastro de sua equipe no site da Social Gamers, sua equipe deverá se registrar na seletiva de sua universidade em questão.</p>
<p>Cronograma do dia 31/01;<br>
Check in: o capitão de cada equipe deve realizar o check in no site das 09h00 as 12h00<br>
Reunião de Capitães: é obrigatório o capitão de cada equipe estar presente em nosso TeamSpeak (ts.socialgamers.com.br) para a reunião que acontecerá as 12h30 onde será repassado todas as regras, link de registro para premiação, chave do torneio e os códigos de partida<br>
Inicio das partidas: as partidas se iniciam as 13h00 (horário de Brasília). </p>
</div></section><section class="toggle"><label><i class="fa fa-plus"></i><i class="fa fa-minus"></i> Criação de partida e jogos</label><div class="toggle-content"><p>O campeonato será no modo Melhor de 1 (Md1) com exceção da final e disputa de terceiro lugar que serão no modo Melhor de 3 (Md3).<br>
O campeonato será no modo Melhor de 1 (Md1) com exceção da final e disputa de terceiro lugar que serão no modo Melhor de 3 (Md3).</p>
<p>Sobre a Modalidade</p>
<p>Mapa: Summoner’s Rift</p>
<p>Modo de Criação</p>
<p>É de responsabilidade da organização do evento a criação das salas e geração dos códigos de torneios.<br>
Os códigos de torneios serão disponibilizados por um link enviado pela organização durante a reunião dos capitães.</p>
<p>Caso exista algum problema com a geração de códigos a criação das salas deverá seguir o padrão descrito abaixo:</p>
<p>Tamanho da Equipe: 5</p>
<p>Permitir espectadores: Todos.</p>
<p>Tipo de Partida: Torneio Competitivo.</p>
<p>Nome da partida: Seletiva UL – Nome do time A Vs. nome do time B.</p>
<p>Senha: Qualquer uma que não deixe a sala livre.</p>
<p>Picks e Bans: O modo de Pick’s e Ban’s será o clássico já disponibilizado no jogo.</p>
<p>Criação de partida</p>
<p>O código da partida será entregue para ambos os capitães atráves do link durante a reunião de capitães.<br>
Em caso de dúvidas, a STAFF deve ser comunicada pelo TeamSpeak (ts.socialgamers.com.br). Assim que o capitão pegar o código deve distribuir o para todos os membros de sua respectiva equipe.<br>
Ao entrar na sala, quem estiver como dono deve, caso se faça necessário, passar o poder de convite para a partida para o time adversário.</p>
</div></section><section class="toggle"><label><i class="fa fa-plus"></i><i class="fa fa-minus"></i>Pick Coringa</label><div class="toggle-content"><p>Pick’s coringa obrigatoriamente devem ser avisados para a equipe adversária e para a STAFF (Caso o jogo esteja sendo transmitido) após selecionar o campeão, em qualquer meio de comunicação rápido (Recomendamos via TeamSpeak ou Mensagem no LoL), pois os streamers precisam desta informação.</p>
</div></section><section class="toggle"><label><i class="fa fa-plus"></i><i class="fa fa-minus"></i>Regras Gerais</label><div class="toggle-content"><p>01 – O invocador que abandonar a partida (Deslogar sem motivo justificável e comprovável) será banido permanentemente da etapa e temporada em questão. Portanto este só poderá voltar a participar em outras etapas nas próximas temporadas.</p>
<p>02 – A equipe que for banida de 2 (dois) torneios realizados pela Social Gamers será banido de todos os torneios realizados futuramente. O controle será feito pelo nome da equipe e jogadores inscritos na mesma.</p>
<p>03 – O invocador que desrespeitar outros invocadores durante os torneios será banido do torneio em questão.</p>
<p>Caso exista uma reincidência o mesmo será banido permanentemente dos eventos realizados pela Social Gamers.</p>
<p>04 – A equipe com nome ofensivo, seja ela direcionado a alguma pessoa ou organização em geral, e qualquer religião, opção política, opção sexual, de gênero, cor, raça ou credo será eliminada automaticamente e sem aviso prévio.</p>
<p>05 – O invocador que desrespeitar qualquer um dos STAFFs, Server Admins; Moderadores; Casters e qualquer outro representante da Social Gamers, será punido da maneira que a Social Gamers julgar correto.</p>
<p>06 – A equipe que realizar o check-in para uma etapa e não for participar, deve contatar algum STAFF, Organizador ou Server Admin da SocialGamers, pelo menos 15 (quinze) minutos antes do início da partida.</p>
<p>Caso não seja avisado e a equipe perca por W.O a equipe poderá ser suspensa da próxima etapa.</p>
<p>07 – As equipes têm até dia 30/01/2015 para registrar na seletiva de sua universidade.</p>
<p>08 – É obrigatório o envio dos documentos RA e RG do aluno participante para o e-mail: campeonatos@socialgamers.com.br</p>
<p>09 – Inscreva sua equipe apenas se realmente houver disponibilidade para jogarem todos os jogos da etapa.</p>
<p>10 – O Torneio é formado por jogos no estilo Melhor de 1 (Md1) sem chave dos perdedores (Loser brackets) sendo somente final e disputa de 3º e 4º colocados Melhor de três (Md3).</p>
<p>11 – Cada equipe tem direito a 10 (dez) minutos de pause no jogo e a organização tem mais 10 (dez) minutos.</p>
<p>A organização poderá, se julgar necessário ou justo doar até 5 (cinco) minutos de pause para cada equipe.</p>
<p>Caso algum jogador não conseguir voltar para o jogo e o período de pause acabar, a equipe deve despausar imediatamente e continuar o jogo, mesmo com jogador(es) em desfalque.</p>
<p>Ps: Caso a equipe use de má fé e fique pausando e despausando a partida, a mesma será punida com desclassificação da etapa.</p>
<p>12 – Se uma equipe sofrer desfalque no que se aplicaria nas regras de numero 11 e a partida estiver no tempo de até 8 minutos e nenhum abate tenha ocorrido.</p>
<p>A partida poderá ser reiniciada se a equipe desfalcada tiver um invocador reserva inscrito no campeonato.</p>
<p>Obs. A partida só poderá ser reiniciada uma vez por equipe.</p>
<p>13 – O invocador titular ou reserva cadastrado, não pode em hipótese alguma informar os dados de sua conta para outra pessoa jogar o campeonato, caso isso aconteça, a equipe será cúmplice deste ato e perderá o direito de jogar qualquer campeonato realizado pela Social Gamers.</p>
<p>14 – A partida só pode ser iniciada com 5 (cinco) jogadores e estes jogadores devem estar previamente cadastrados e com os nome de invocadores atualizados no site da Social Gamers (socialgamers.com.br).</p>
<p>15 – A equipe com um atraso de 15 (quinze) minutos, contados a partir do horário marcado, será desclassificada por W.O.</p>
<p>16 – É extremamente proibido o uso de qualquer tipo de trapaça ou qualquer programa que beneficie ele mesmo ou a sua equipe dentro do jogo em questão ou em um sistema da Social Gamers.</p>
<p>Caso o jogador seja identificado com trapaças, a equipe será considerada como cúmplice e serão banidos dos campeonatos promovidos pela Social Gamers.</p>
<p>17 – A premiação será aplicada apenas para cinco jogadores do time, escolhidos pelo capitão.</p>
<p>18 – Invocadores com nível inferior ao 30 não podem participar do campeonato.</p>
<p>19 – É obrigatório a presença do capitão no TeamSpeak (TS) da Social Gamers (ts.socialgamers.com.br) desde a reunião dos capitães até o fim da participação de sua equipe na etapa do campeonato. A ausência do capitão no TS mediante qualquer situação desconfortante ou anormal ao evento com sua equipe será desconsiderada e/ou será julgada sem prévias considerações da equipe com Capitão ausente.</p>
<p>O checkin é liberado no dia da etapa (Domingo dia 31/01).<br>
A abertura do check-in inicia as 09h00 (Nove horas da manhã no horário de Brasília) se encerrando ao 12h00 (Meio dia no horário de Brasília).</p>
<p>20 – Não é permitido iniciar a partida com time incompleto (menos de 5 jogadores), as equipes que não se enquadrarem nesta regra serão desclassificadas.</p>
<p>21 – Ao entrar na partida, quem estiver com a liderança deverá passar o poder de convidar para o time adversário caso se faça necessário.</p>
<p>22 – Não é permitido nenhum espectador durante a seleção de campeões, seja técnico ou organizador.</p>
<p>As vagas de espectador estão disponíveis apenas para a STAFF previamente apresentados e casters da Social Gamers.</p>
<p>Ps. caso algum invocador queira assistir uma partida do campeonato que não seja transmitido, o mesmo poderá assistir a partida após a seleção de campeões na opção de espectador na lista de amigos..</p>
<p>23 – As partidas podem ser stremadas pelos jogadores somente até as 16h.</p>
<p>Após este horário devem ser encerradas ou redirecionadas para a transmissão oficial da Social Gamers. (http://www.twitch.tv/socialgamerstv)</p>
<p>Caso está regra não seja cumprida, a Social Gamers tem total liberdade para tomar qualquer decisão que achar viável.</p>
<p>24 – Com o objetivo de abranger as mais diversas situações/atos que afetam o bom andamento do campeonato, quaisquer outras condutas anti-esportivas assim julgadas pela STAFF e que não se enquadrem nas citadas acima, a ORGANIZAÇÃO tem total direito de aplicar punições, podendo ser aplicadas no ato ou em qualquer prazo que a STAFF julgar necessário. </p>
<p>25 – Estas regras podem ser alteradas, modificadas ou complementadas pela organização da Social Gamers a fim de assegurar partidas justas e a integridade do evento.</p>
<p>Estas regras foram criadas pela equipe Social Gamers e todas as informações estão expostas em nosso site: www.socialgamers.com.br</p>
</div></section></div>
        </div>
        <div class="col-md-6 column_last">
            <h2>Duvidas Frequentes</h2>
            <div  class="module toogle" data-plugin-toggle="">
                <section class="toggle">
                    <label>
                        <i class="fa fa-plus"></i>
                        <i class="fa fa-minus"></i>
                        Como faz para participar?
                    </label>
                <div class="toggle-content" style="display: none;">
                    <p>Para participar é bem simples, basta você se cadastrar em nosso site, depois de confirmar seu cadastro no  e-mail chegou o momento de criar sua equipe e convidar seus amigos. Agora é só se inscrever na MSL e se divertir.</p>
                </div>
                </section>
                <section class="toggle">
                    <label>
                        <i class="fa fa-plus"></i>
                        <i class="fa fa-minus"></i>
                        Como cadastro minha equipe no site?
                    </label>
                    <div class="toggle-content">
                        <p>Para cadastrar sua equipe basta você acessar nosso menu na aba “Times” e criar um novo time. Lembre que depois de criar seu time você pode adicionar a logo do time e convidar seus amigos <img src="/img/smilies/simple-smile.png" alt=":)" class="wp-smiley" style="height: 1em; max-height: 1em;"></p>
                    </div>
                </section>
                <section class="toggle">
                    <label>
                        <i class="fa fa-plus"></i>
                        <i class="fa fa-minus"></i>
                        Como inscrevo minha equipe no campeonato?
                    </label>
                    <div class="toggle-content">
                        <p>Para inscrever sua equipe é so ir na aba “Campeonatos” e clicar no botão inscrever! quando entrar na página você deve selecionar a etapa que quer participar e depois clicar em “inscrever” e pronto <img class="emoji " style="height: 1em; max-height: 1em;"  draggable="false" alt=":D" src="/img/smilies/smile.png"></p>
                    </div>
                </section>



                <section class="toggle">
                    <label>
                        <i class="fa fa-plus"></i>
                        <i class="fa fa-minus"></i>
                        Como faço o checkin?
                    </label>
                    <div class="toggle-content">
                        <p>Após efetuar a inscrição de sua equipe no campeonato, é facil. No dia que ocorre o campeonato, na aba Campeonatos e em sua respectiva data/etapa no qual esta inscrito. Clique no botão “Check-in” que ficará ao lado do botão de inscrição. E pronto! Aguarde novas instruções do organizador em nosso TeamSpeak!</p>
                    </div>
                </section>
                <section class="toggle">
                    <label>
                        <i class="fa fa-plus"></i>
                        <i class="fa fa-minus"></i>
                        Que horário começa o campeonato e as partidas?
                    </label>
                    <div class="toggle-content">
                        <p>O campeonato considerando desde seu Checkin ate o término, que ocorre no mesmo dia ocorre da seguinte forma:<br>
                        – O checkin inicia-se as 09h00 e termina as 12h00<br>
                        – Reunião de capitães inicia-se 12h30<br>
                        – A 1ª Rodada inicia-se as 13h00 e CADA rodada tem uma previsão de uma em uma hora para iniciar, ou seja, a previsão para 2º rodada são as 14h00, para a 3º rodada as 15h00 e assim por diante.</p>
                    </div>
                </section>
                <section class="toggle">
                    <label>
                        <i class="fa fa-plus"></i>
                        <i class="fa fa-minus"></i>
                        Meu time ganhou, como reporto a vitoria?
                    </label>
                    <div class="toggle-content">
                        <p>É muito simples! Será necessário tirar uma Printscreen do Saguão de vitória (Tela que contém todos os dados da partida, tais como kill’s, death’s, assist’s, etc.) e nos mandar clicando no menu na aba “Campeonatos” “Reportar vitória”.</p>
                    </div>
                </section>
                <section class="toggle">
                    <label>
                        <i class="fa fa-plus"></i>
                        <i class="fa fa-minus"></i>
                        Meu jogador caiu da partida, oque eu faço?
                    </label>
                    <div class="toggle-content">
                        <p>Não se preocupe, imediatamente de o comando /pause no jogo, sempre liberamos um limite de 10 min. de pause para CADA equipe. Ué, sobram 10 minutos nessa brincadeira!?!?! Sim, estes são usados para a Organização do evento (social Gamers) que no ato, verifica se é coerente a doação de 5 minutos para CADA equipe. Este pause também se aplica para casos em que o jogo esta sendo streamado por nossa STAFF.</p>
                    </div>
                </section>
                <section class="toggle">
                    <label>
                        <i class="fa fa-plus"></i>
                        <i class="fa fa-minus"></i>
                        A rodada esta pra iniciar e um jogador meu caiu, o que eu faço?
                    </label>
                    <div class="toggle-content">
                        <p>Infelizmente autorizamos uma tolerância de 15 minutos APÓS postar o código de torneio, caso ele não reconecte você precisa chamar seu jogador reserva e se não tiver será aplicado o W.O <img src="/img/smilies/frownie.png" alt=":(" class="wp-smiley" style="height: 1em; max-height: 1em;"></p>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>