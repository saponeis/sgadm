<section class="page-top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/" class="home">Social Gamers</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/campeonatos" class="home">Campeonatos</a>
						</span>
					</li>

					<li class="current_item">
						<span typeof="v:Breadcrumb">
							<span property="v:title">Circuito Social League</span>
						</span>
					</li>
                </ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2 class="yellow">Circuito Social League</h2>
			</div>
		</div>
	</div>
</section>


<div class="container">
    <div class="row">
        <div class="col-md-4 column_first">
            <div class="module">
				{% if championship.image is defined %}
					<img class="img-circle img-responsive img-championship" src="/files/championship/{{championship.id}}/{{championship.image}}">
				{% endif %}            </div>
        </div>
        <div class="col-md-8 column_last">
            <h2 class="shorter"><strong>circuito Social League</strong></h2>
            <h4>Primeira Temporada da CSL</h4>
            <p>Chega o grande momento aonde as melhores equipes que participaram da MSL e University League se confrontam para definir o grande campeão do Circuito Social League - CSL.</p>
			<p><a href="campeonatos/chave/{{championship.id}}">
				<button type="button" class="btn btn-default btn-lg">Veja a chave</button>
				</a>
			</p>
        	<hr class="light"><hr class="light">
        </div>
        <div class="col-md-12 column_first column_last">
            <div class="center">
				<h2 class="short word-rotator-title">
					Os melhores classificados da
					<strong class="inverted">
						<span class="word-rotate active" data-plugin-options="{delay:2000}" style="height: 54px;">
							<span class="word-rotate-items" style="top: 0px;">
								<span>Master Social League</span>
								<span>University League</span>
							</span>
						</span>
					</strong>
					vão se enfrentar em partidas emocionantes para definir o primeiro grande Campeão!
				</h2>
			</div>
			<hr class="light"><hr class="light">
		</div>
        <div class="col-md-6 col-md-offset-3 column_first column_last">
            <div class="center">
				<img class="img-responsive" src="/img/site/chavecsl.png" />
			</div>
		</div>

        <div id="builder-column-5695034b6e184" class="span12 col-md-12 column_first column_last">
            <hr class="light"><hr class="light">
            <div class="center">
				<h2 class="short">
					Veja quais são as equipes que estão garantindo sua classificação no Circuito Social League
				</h2>
			</div>
			<hr class="light"><hr class="light">
        </div>
				
        <div class="col-md-6 column_first">
            <div class="module module-html">
            	<img src="/img/site/ranking-msl.png">
            	<div class="clear"></div>
            </div>
            <hr class="light">
        </div>

        <div class="col-md-6 column_first">
            <div class="module module-html">
				<img src="/img/site/ranking-ul.png">
            	<div class="clear"></div>
            </div>
            <hr class="light">
        </div>
        <div class="col-md-12 column_first column_last">
            <div class="center">
				<h2 class="short word-rotator-title">
					<strong class="inverted">
						<span class="word-rotate active" data-plugin-options="{&quot;delay&quot;: 2000}" style="height: 54px;">
							<span class="word-rotate-items" style="top: -108px;">
								<span>Premiações</span>
								<span>Produtos MultiLaser</span>
								<span>Games na Nuuvem</span>
							</span>
						</span>
					</strong>
				</h2>
			</div>
			<hr class="light">
			<div id="builder-module-5692dd1a2759c" class="module module-html">
				<img src="/img/site/premios.png">
				<div class="clear"></div>
			</div>
			<hr class="light"><hr class="light"><hr class="light">
        </div>
        <div class="col-md-12 column_first column_last">



<div class="container">
    <div class="row">
        <div class="col-md-12 column_first column_last">
            <h2>Conheça nossa equipe de <strong>Casters</strong></h2>
        </div>
        










<div class="col-md-3 column_first">
	            <div>
	            	<a href = "https://www.facebook.com/Madzzolol" target="blank">
	            		<img src="/img/site/narrador/madzo.png"/>
	            	</a>
	            	<div class="clear"></div>
	            </div>
	        </div>
	        <div class="col-md-3">
	            <div>
	            	
	            <a href="https://www.facebook.com/tvcolosimus" target="blank">
	            	<img src="/img/site/narrador/colosimus.png">
            	</a>
	            	<div class="clear"></div>
	            </div>
	        </div>
	        <div class="col-md-3">
	            <div>
	            <a href="https://www.facebook.com/mundodocelu" target="blank">
	            	<img src="/img/site/narrador/celu.png">
	            	
	            </a>
					<div class="clear"></div>
				</div>
	        </div>
	        <div class="col-md-3 column_last">
	            <div>
	            <a href="https://www.facebook.com/tibianolol" target="blank">
	            	
	            	<img src="/img/site/narrador/tibiano.png">
	            </a>

	            	<div class="clear"></div>
	            </div>
	        </div>


        
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12 column_first column_last">
            <hr class="light">
        </div>
        <div class="col-md-6 column_first">
            <h2 class="">Regras</h2>
            <div class="module toogle" data-plugin-toggle="">
            	<section class="toggle">
            		<label>
            			<i class="fa fa-plus"></i>
            			<i class="fa fa-minus"></i>
            			Para participar
            		</label>
            		<div class="toggle-content" style="display: none;">
            			<p>Qualquer equipe pode participar da MSL (Master Social League).</p>
						<p>Se faz necessário apenas:</p>
						<ol>
							<li>Cadastro da equipe e de CADA jogador na plataforma da Social Gamers pelo site da MSL (msl.socialgamers.com.br).</li>
							<li>O capitão inscrever a equipe na etapa que deseja participar através do menu “Campeonatos” na plataforma. (A inscrição abre segunda-feira as 00:01 e encerra na sexta-feira as 23:59).</li>
							<li>Realizar o check-in no dia da etapa (Check-in abre as 7h da manhã e se encerra ao meio-dia do sábado).</li>
							<li>Cadastro no site da RIOT para receber a premiação (link será disponibilizado pela STAFF durante a reunião dos capitães).</li>
						</ol>						
					</div>
				</section>
				<section class="toggle yellow">
					<label>
						<i class="fa fa-plus"></i>
						<i class="fa fa-minus"></i>
						Premiação
					</label>
					<div class="toggle-content">



<p>
As 4 (quatro) melhores equipes, ao termino da etapa, irão receber a premiação abaixo:
</p>
<p>
1º Colocado: 3200 RP + Ryze Triunfante + Produtos Multilaser (1 por jogador) + 14 pontos na liga
</p>
<p>
2º Colocado: 2400 RP (por jogador) + 12 pontos na liga
</p>
<p>
3º Colocado: 1600 RP (por jogador) + 9 pontos na liga
</p>
<p>
4º Colocado: 800 RP (por jogador) + 7 pontos na liga
</p>
<p>
5ª a 8ª Colocado: 5 pontos na liga
</p>
<p>
Lembrando que para receber a premiação, as equipes deverão se registrar no site da Riot, para a etapa vigente.
</p>
<p>
O prazo maximo é de 40 dias para entrega da premiação.
</p>
<p>
Critério de desempate:
</p>
<p>
1º Critério: A equipe que tiver mais vitórias ao longo do campeonato.
</p>
<p>
2º Critério: Confronto direto em modo MD1 (Melhor de 1), o vencedor deste confronto ganhará o desempate.</p>











					</div>
				</section>
				<section class="toggle">
					<label>
						<i class="fa fa-plus"></i>
						<i class="fa fa-minus"></i>
						Pontuação
					</label>
					<div class="toggle-content">
						<p>
							A cada MSL realizada as equipes de 1º a 8º colocadas serão pontuadas, na segunda semana de Março será a feita a contabilização final, as 4 equipes mais pontuadas ao longo do trimestre conseguirão a vaga para a CSL que ocorrerá nas duas últimas semanas de Março.
							<br>
							Vale lembrar que a partir do inicio até o final da CSL, a MSL não contabilizará pontos.
						</p>
					</div>
				</section>
				<section class="toggle">
					<label>
						<i class="fa fa-plus"></i>
						<i class="fa fa-minus"></i>
						Sobre a temporada
					</label>
					<div class="toggle-content">
						<p>O campeonato ocorre nos 4 primeiros Sábados do mês e o número mínimo de equipes é 8 (oito).<br>
						Caso uma etapa não tenha 8 (oito) equipes, a mesma será cancelada.</p>
					</div>
				</section>
				<section class="toggle">
					<label>
						<i class="fa fa-plus"></i>
						<i class="fa fa-minus"></i>
						Criação de partida e jogos
					</label>
					<div class="toggle-content">
						<p>
							O campeonato será semanal no modo Melhor de 1 (Md1) com exceção da final e disputa de terceiro lugar que serão no modo Melhor de 3 (Md3).<br>
							Mapa: Summoner’s Rift<br>
							É de responsabilidade da organização do evento a criação das salas e geração dos códigos de torneios.<br>
							Os códigos de torneios serão disponibilizados por um link enviado pela organização durante a reunião dos capitães.
						</p>
						<p>Caso exista algum problema com a geração de códigos a criação das salas deverá seguir o padrão descrito abaixo:</p>
						<p>Tamanho da Equipe: 5</p>
						<p>Permitir espectadores: Todos.</p>
						<p>Tipo de Partida: Torneio Competitivo.</p>
						<p>Nome da partida: MSL – Nome do time A Vs. nome do time B.</p>
						<p>Senha: Qualquer uma que não deixe a sala livre.</p>
						<p>Picks e Bans: O modo de Pick’s e Ban’s será o clássico já disponibilizado no jogo.</p>
					</div>
				</section>
				<section class="toggle">
					<label>
						<i class="fa fa-plus"></i>
						<i class="fa fa-minus"></i>
						Pick Coringa
					</label>
					<div class="toggle-content">
						<p>Pick’s coringa obrigatoriamente devem ser avisados para a equipe adversária e para a STAFF (Caso o jogo esteja sendo streamado) após selecionar o campeão, em qualquer meio de comunicação rápido (Recomendamos via TeamSpeak ou Mensagem no LoL), pois os streamers precisam desta informação.</p>
					</div>
				</section>
				<section class="toggle">
					<label>
						<i class="fa fa-plus"></i>
						<i class="fa fa-minus"></i>
						Regras Gerais
					</label>
					<div class="toggle-content">
						<ol>
							<li>O invocador que abandonar a partida (Deslogar sem motivo justificável e comprovável) será banido permanentemente da etapa e temporada em questão. Portanto este só poderá voltar a participar em outras etapas nas próximas temporadas.</li>
							<li>A equipe que for banida de 2 (dois) torneios realizados pela Social Gamers será banido de todos os torneios realizados futuramente. O controle será feito pelo nome da equipe e jogadores inscritos na mesma.</li>
							<li>O invocador que desrespeitar outros invocadores durante os torneios será banido do torneio em questão.</li>
							Caso exista uma reincidência o mesmo será banido permanentemente dos eventos realizados pela Social Gamers.</li>
							<li>A equipe com nome ofensivo, seja ela direcionado a alguma pessoa ou organização em geral, e qualquer religião, opção política, opção sexual, de gênero, cor, raça ou credo será eliminada automaticamente e sem aviso prévio.</li>
							<li>O invocador que desrespeitar qualquer um dos STAFFs, Server Admins; Moderadores; Casters e qualquer outro representante da Social Gamers, será punido da maneira que a Social Gamers julgar correto.</li>
							<li>A equipe que realizar o check-in para uma etapa e não for participar, deve contatar algum STAFF, Organizador ou Server Admin da SocialGamers, pelo menos 15 (quinze) minutos antes do início da partida.</li>
							Caso não seja avisado e a equipe perca por W.O a equipe poderá ser suspensa da próxima etapa.</li>
							<li>As equipes têm de Segunda-feira 00:01 até as 23h59 da Sexta-feira para se registrar para a etapa.</li>
							<li>O torneio não terá inicio sem um mínimo de 8 (oito) equipes.</li>
							<li>Inscreva sua equipe apenas se realmente houver disponibilidade para jogarem todos os jogos da etapa.</li>
							<li>O Torneio é formado por jogos no estilo Melhor de 1 (Md1) sem chave dos perdedores (Loser brackets) sendo somente final e disputa de 3º e 4º colocados Melhor de três (Md3).</li>
							<li>Cada equipe tem direito a 10 (dez) minutos de pause no jogo e a organização tem mais 10 (dez) minutos.</li>
							A organização poderá, se julgar necessário ou justo doar até 5 (cinco) minutos de pause para cada equipe.</li>
							Caso algum jogador não conseguir voltar para o jogo e o período de pause acabar, a equipe deve despausar imediatamente e continuar o jogo, mesmo com jogador(es) em desfalque.</li>
							<strong>Ps: Caso a equipe use de má fé e fique pausando e despausando a partida, a mesma será punida com desclassificação da etapa.</strong>
							<li>Se uma equipe sofrer desfalque no que se aplicaria nas regras de numero 11 e a partida estiver no tempo de até 8 minutos e nenhum abate tenha ocorrido.</li>
							A partida poderá ser reiniciada se a equipe desfalcada tiver um invocador reserva inscrito no campeonato.<br>
							Obs. A partida só poderá ser reiniciada uma vez por equipe.</li>
							<li>O invocador titular ou reserva cadastrado, não pode em hipótese alguma informar os dados de sua conta para outra pessoa jogar o campeonato, caso isso aconteça, a equipe será cúmplice deste ato e perderá o direito de jogar a temporada vigente da MSL.</li>
							<li>A partida só pode ser iniciada com 5 (cinco) jogadores e estes jogadores devem estar previamente cadastrados e com os nome de invocadores atualizados no site da MSL (msl.socialgamers.com.br).</li>
							<li>A equipe com um atraso de 15 (quinze) minutos, contados a partir do horário marcado, será desclassificada por W.O.</li>
							<li>É extremamente proibido o uso de qualquer tipo de trapaça ou qualquer programa que beneficie ele mesmo ou a sua equipe dentro do jogo em questão ou em um sistema da Social Gamers.</li>
							Caso o jogador seja pego com trapaças a equipe será considerada como cúmplice e serão banidos dos campeonatos promovidos pela Social Gamers.</li>
							<li>A premiação será aplicada apenas para cinco jogadores do time, escolhidos pelo capitão.</li>
							<li>Invocadores com nível inferior ao 30 não podem participar do campeonato.</li>
							<li>É obrigatório a presença do capitão no TeamSpeak (TS) da Social Gamers (ts.socialgamers.com.br) desde a reunião dos capitães até o fim da participação de sua equipe na etapa do campeonato. A ausência do capitão no TS mediante qualquer situação desconfortante ou anormal ao evento com sua equipe será desconsiderada e/ou será julgada sem prévias considerações da equipe com Capitão ausente.</li>
							<li>O checkin é liberado no dia da etapa (sábados).</li>
							A abertura do check-in inicia as 09h00 (Nove horas da manhã no horário de Brasília) se encerrando ao 12h00 (Meio dia no horário de Brasília).</li>
							<li>Não é permitido iniciar a partida com time incompleto (menos de 5 jogadores), as equipes que não se enquadrarem nesta regra serão desclassificadas.</li>
							<li>Ao entrar na partida, quem estiver com a liderança deverá passar o poder de convidar para o time adversário caso se faça necessário.</li>
							<li>Não é permitido nenhum espectador durante a seleção de campeões, seja técnico ou organizador.</li>
							As vagas de espectador estão disponíveis apenas para a STAFF previamente apresentados e casters da Social Gamers.</li>
							Ps. caso algum invocador queira assistir uma partida do campeonato que não seja transmitido, o mesmo poderá assistir a partida após a seleção de campeões na opção de espectador na lista de amigos.. </li>
							<li>As partidas podem ser stremadas pelos jogadores somente até as 16h.</li>
							Após este horário devem ser encerradas ou redirecionadas para a transmissão oficial da Social Gamers. (http://www.twitch.tv/socialgamerstv)</li>
							Caso está regra não seja cumprida, a Social Gamers tem total liberdade para tomar qualquer decisão que achar viável.</li>
							<li>Com o objetivo de abranger as mais diversas situações/atos que afetam o bom andamento do campeonato, quaisquer outras condutas anti-esportivas assim julgadas pela STAFF e que não se enquadrem nas citadas acima, a ORGANIZAÇÃO tem total direito de aplicar punições, podendo ser aplicadas no ato ou em qualquer prazo que a STAFF julgar necessário. </li>
							<li>Estas regras podem ser alteradas, modificadas ou complementadas pela organização da Social Gamers a fim de assegurar partidas justas e a integridade do evento.</li>
						</ol>
						<strong>Estas regras foram criadas pela equipe Social Gamers e todas as informações estão expostas em nosso site: www.socialgamers.com.br</strong>
					</div>
				</section>
			</div>
		</div>
		<div class="col-md-6 column_last">
		    <h2>Duvidas Frequentes</h2>
		    <div  class="module toogle" data-plugin-toggle="">
		    	<section class="toggle">
		    		<label>
		    			<i class="fa fa-plus"></i>
		    			<i class="fa fa-minus"></i>
		    			Como faz para participar?
		    		</label>
		    	<div class="toggle-content" style="display: none;">
		    		<p>Para participar é bem simples, basta você se cadastrar em nosso site, depois de confirmar seu cadastro no  e-mail chegou o momento de criar sua equipe e convidar seus amigos. Agora é só se inscrever na MSL e se divertir.</p>
				</div>
				</section>
				<section class="toggle">
					<label>
						<i class="fa fa-plus"></i>
						<i class="fa fa-minus"></i>
						Como cadastro minha equipe no site?
					</label>
					<div class="toggle-content">
						<p>Para cadastrar sua equipe basta você acessar nosso menu na aba “Times” e criar um novo time. Lembre que depois de criar seu time você pode adicionar a logo do time e convidar seus amigos <img src="/img/smilies/simple-smile.png" alt=":)" class="wp-smiley" style="height: 1em; max-height: 1em;"></p>
					</div>
				</section>
				<section class="toggle">
					<label>
						<i class="fa fa-plus"></i>
						<i class="fa fa-minus"></i>
						Como inscrevo minha equipe no campeonato?
					</label>
					<div class="toggle-content">
						<p>Para inscrever sua equipe é so ir na aba “Campeonatos” e clicar no botão inscrever! quando entrar na página você deve selecionar a etapa que quer participar e depois clicar em “inscrever” e pronto <img class="emoji " style="height: 1em; max-height: 1em;"  draggable="false" alt=":D" src="/img/smilies/smile.png"></p>
					</div>
				</section>



				<section class="toggle">
					<label>
						<i class="fa fa-plus"></i>
						<i class="fa fa-minus"></i>
						Como faço o checkin?
					</label>
					<div class="toggle-content">
						<p>Após efetuar a inscrição de sua equipe no campeonato, é facil. No dia que ocorre o campeonato, na aba Campeonatos e em sua respectiva data/etapa no qual esta inscrito. Clique no botão “Check-in” que ficará ao lado do botão de inscrição. E pronto! Aguarde novas instruções do organizador em nosso TeamSpeak!</p>
					</div>
				</section>
				<section class="toggle">
					<label>
						<i class="fa fa-plus"></i>
						<i class="fa fa-minus"></i>
						Que horário começa o campeonato e as partidas?
					</label>
					<div class="toggle-content">
						<p>O campeonato considerando desde seu Checkin ate o término, que ocorre no mesmo dia ocorre da seguinte forma:<br>
						– O checkin inicia-se as 09h00 e termina as 12h00<br>
						– Reunião de capitães inicia-se 12h30<br>
						– A 1ª Rodada inicia-se as 13h00 e CADA rodada tem uma previsão de uma em uma hora para iniciar, ou seja, a previsão para 2º rodada são as 14h00, para a 3º rodada as 15h00 e assim por diante.</p>
					</div>
				</section>
				<section class="toggle">
					<label>
						<i class="fa fa-plus"></i>
						<i class="fa fa-minus"></i>
						Meu time ganhou, como reporto a vitoria?
					</label>
					<div class="toggle-content">
						<p>É muito simples! Será necessário tirar uma Printscreen do Saguão de vitória (Tela que contém todos os dados da partida, tais como kill’s, death’s, assist’s, etc.) e nos mandar clicando no menu na aba “Campeonatos” “Reportar vitória”.</p>
					</div>
				</section>
				<section class="toggle">
					<label>
						<i class="fa fa-plus"></i>
						<i class="fa fa-minus"></i>
						Meu jogador caiu da partida, oque eu faço?
					</label>
					<div class="toggle-content">
						<p>Não se preocupe, imediatamente de o comando /pause no jogo, sempre liberamos um limite de 10 min. de pause para CADA equipe. Ué, sobram 10 minutos nessa brincadeira!?!?! Sim, estes são usados para a Organização do evento (social Gamers) que no ato, verifica se é coerente a doação de 5 minutos para CADA equipe. Este pause também se aplica para casos em que o jogo esta sendo streamado por nossa STAFF.</p>
					</div>
				</section>
				<section class="toggle">
					<label>
						<i class="fa fa-plus"></i>
						<i class="fa fa-minus"></i>
						A rodada esta pra iniciar e um jogador meu caiu, o que eu faço?
					</label>
					<div class="toggle-content">
						<p>Infelizmente autorizamos uma tolerância de 15 minutos APÓS postar o código de torneio, caso ele não reconecte você precisa chamar seu jogador reserva e se não tiver será aplicado o W.O <img src="/img/smilies/frownie.png" alt=":(" class="wp-smiley" style="height: 1em; max-height: 1em;"></p>
					</div>
				</section>
			</div>
		</div>
		</div>
		</div>
		</div>
		</div>
		</div>


            <div class="container">
                <div class="row">
                    
        <div id="builder-column-568eb0f694bb4" class="span12 col-md-12 column_first column_last">
            <hr class="light"><hr class="light">
        </div>
                </div>
            </div>
            
        </div>
        <div id="builder-row-568fb15197e04" class="featured_section call-to-action footer">
            <div class="container">
                <div class="row">
                    
        <div id="builder-column-568fb15197eb4" class="span12 col-md-12 column_first column_last">
            <h2 class="">O circuito Social League é somente para League of Legends ou tem para outros jogos?</h2><div class="row">
	<div class="col-md-10">
		<p class="lead">
		  Não se preocupe! você deseja seu jogo favorito recebendo a CSL? Envie o formulário solicitando que vamos analisar o cenário e tentar realizar uma nova liga!		</p>
	</div>
	<div class="col-md-2">
			</div>
</div><div id="builder-module-568fb2646fa6a" class="module rich-text"><button type="button" class="btn btn-primary btn-lg">Solicite o circuito para seu jogo favorito</button>
</div>
        </div>
                </div>
            </div>
            
        </div></div></div>