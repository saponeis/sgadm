<section class="page-top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/" class="home">Social Gamers</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/campeonatos/" class="home">Campeonatos</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/campeonatos/{{championship.slug}}" class="home">{{championship.name}}</a>
						</span>
					</li>
					<li class="current_item">
						<span typeof="v:Breadcrumb">
							<span property="v:title">Chek-In!</span>
						</span>
					</li>
                </ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2 class="yellow">Chek-In!</h2>
			</div>
		</div>
	</div>
</section>

<div class="container">
	<div class="row">
       <div class="col-md-12">
    		<div class="center">
                	<p class="featured lead">
                	 	{% if subs is empty %}
 							Não existem etapas disponíveis para realizar check-in.
					 	{% else %}
							Selecione a Etapa para qual deseja realizar o Chek-In:
						{% endif %}
						<br/>
					</p>	
			</div>
		</div>
	</div>
	<div class="row">
	<div class="col-md-12">
		<?php $this->flashSession->output() ?>
	</div>		

	<div class="row">
 	{% if subs is empty %}

 	{% else %}
		<div class="pricing-table">			
			{% for sub in subs %}
				<div class="col-md-3">
					<a href="#" class="push-top push-bottom" data-toggle="modal" data-target="#{{sub.id}}">
						<div class="plan">
							<h3>
								{{sub.step.name}}
								<span>
									{% if championship.image is defined %}
									<img class="img-circle img-responsive img-championship" src="/files/championship/{{championship.id}}/{{championship.image}}">
									{% else %}
									<img class="img-circle img-responsive img-championship"  src="/img/team/team-1.png">
									{% endif %}
								</span>
							</h3>
							<button class="btn btn-lg btn-primary yellow-btn">Check-In</button>
						</div>
					</a>
				</div>
				<div class="modal fade" id="{{sub.id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-body">
								<div class="featured-box featured-box-yellow default">
									<div class="box-content border-yellow">
										<form id="editProfile" action="/campeonatos/checkin" method="post" enctype="multipart/form-data">
											<div class="row">
												<div class="col-md-12">
													{% if sub.status == 2 %}
            										<div class="alert alert-danger">
            										ATENÇÃO - Checkin Já Realizado
            										</div>
													<button type="button" class="btn-lg btn yellow-btn" data-dismiss="modal">Voltar</button>
													{% else %}
													<input type="hidden" name="subId" value="{{sub.id}}" />
													<input type="hidden" name="teamId" value="{{teamId}}" />
													<input type="submit" value="Realizar Chek-In" id="salvar" class="btn-lg btn yellow-btn" />
													<button type="button" class="btn-lg btn yellow-btn" data-dismiss="modal">Voltar</button>
													{%endif%}
												</div>
											</div>
										</form>
									</div>
						    	</div>
							</div>
						</div>
					</div>
				</div>
			{% endfor %}
		</div>
		{% endif %}
	</div>
</div>