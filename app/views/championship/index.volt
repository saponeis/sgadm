<div role="main" class="main">
    <section class="page-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li class="home">
                            <span typeof="v:Breadcrumb">
                                <a title="Go to Social Gamers." href="/" class="home"> Social Gamers </a>
                            </span>
                        </li>
                        <li class="current_item">
                            <span typeof="v:Breadcrumb">
                                <span>Campeonatos</span>
                            </span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2 class="yellow">Campeonatos</h2>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row center">
            <div class="col-md-12">
                <h2 class="short word-rotator-title">
                    Venha fazer parte da comunidade mais
                    <strong class="inverted">
                        <span class="word-rotate" data-plugin-options='{"delay": 2000}'>
                            <span class="word-rotate-items">
                                <span>competitiva.</span>
                                <span>estruturada.</span>
                                <span>organizada.</span>
                            </span>
                        </span>
                    </strong>
                </h2>
                <div>
                    <p class="featured lead">A Social Gamers traz um novo modelo de entretenimento eletrônico, com o objetivo de se consolidar como o principal evento competitivo online para gamers do Brasil, organizando <span class="alternative-font yellow">ligas e campeonatos online</span> dos mais variados jogos eletrônicos.</p>
                </div>
            </div>
        </div>
    </div>
    <hr class="tall"><div class="row">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-4 column_first">
                <div class="feature-box">
                    <div class="feature-box-icon">
                        <i class="fa fa-users"></i>
                    </div>
                    <div class="feature-box-info">
                        <h4 class="shorter yellow">Staff</h4>
                        <p class="tall">Temos uma  gestão de profissionais ativos no ramo do eSport para trazer um evento bem estruturado e organizado para os jogadores.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="feature-box">
                    <div class="feature-box-icon">
                        <i class="fa fa-sitemap"></i>
                    </div>
                    <div class="feature-box-info">
                        <h4 class="shorter yellow">Campeonatos Semanais</h4>
                        <p class="tall">Todo fim de semana acontece uma etapa de nossas temporadas mensais com os melhores jogos. </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 column_last">
                <div class="feature-box">
                    <div class="feature-box-icon">
                        <i class="fa fa-desktop"></i>
                    </div>
                    <div class="feature-box-info">
                        <h4 class="shorter yellow">SocialgamersTV</h4>
                        <p class="tall">Stream do evento com narração, brindes, sorteios e brincadeiras.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-4 column_first">
                <div class="feature-box">
                    <div class="feature-box-icon">
                        <i class="fa fa-trophy"></i>
                    </div>
                    <div class="feature-box-info">
                        <h4 class="shorter yellow">Premiações</h4>
                        <p class="tall">Campeonatos com premiações exclusivas* (premiação liberada mediante a desenvolvedora ou parceiros).</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="feature-box">
                    <div class="feature-box-icon">
                        <i class="fa fa-comments"></i>
                    </div>
                    <div class="feature-box-info">
                        <h4 class="shorter yellow">Estrutura de comunicação</h4>
                        <p class="tall">Disponibilizamos para equipes salas de teamspeak 3 para facilitar a comunicação das equipes durante os eventos.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 column_last">
                <div class="feature-box">
                    <div class="feature-box-icon">
                        <i class="fa fa-support"></i>
                    </div>
                    <div class="feature-box-info">
                        <h4 class="shorter yellow">Suporte ao campeonato</h4>
                        <p class="tall">Estamos 24hs disponíveis para tirar qualquer duvida ou ajudar os jogadores.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="span12 col-md-12 column_first column_last">
                <hr class="tall">
                <h2>Conheça nossos <strong>Campeonatos</strong></h2>
                <div class="row">
                    <div class="col-sm-6 col-md-6 column_first">
                        <div class="feature-box">
                            <div class="feature-box-icon">
                                <i class="fa fa-sitemap"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter yellow">League of Legends</h4>
                                <p class="tall">Campeonato de League of Legends voltado aos jogadores de nível amador e semiprofissional, para saber mais  <a href="/campeonatos/league-of-legends/" class="learn-more">clique aqui. <i class="icon icon-angle-right"></i></a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 column_last">
                        <div class="feature-box">
                            <div class="feature-box-icon">
                                <i class="fa fa-sitemap"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter yellow">Heroes of the Storm</h4>
                                <p class="tall">Campeonato de Heroes of the Storm voltado aos jogadores de nível amador e semiprofissional, para saber mais <a href="http://socialgamers.com.br/campeonatos/campeonato-de-heroes-of-the-storm" class="learn-more">clique aqui. <i class="icon icon-angle-right"></i></a></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-md-6 column_first">
                        <div class="feature-box">
                            <div class="feature-box-icon">
                                <i class="fa fa-sitemap"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter yellow">Hearthstone</h4>
                                <p class="tall">Aguarde novidades.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 column_last">
                        <div class="feature-box">
                            <div class="feature-box-icon">
                                <i class="fa fa-sitemap"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter yellow">Counter-Strike: Global Offensive</h4>
                                <p class="tall">Aguarde novidades.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-md-6 column_first">
                        <div class="feature-box">
                            <div class="feature-box-icon">
                                <i class="fa fa-sitemap"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter yellow">Smite</h4>
                                <p class="tall">Aguarde novidades.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="span12 col-md-12 column_first column_last yellow-bg">
    <div class ="container">
        <div class="row center counters">
            <div class="col-md-3 col-sm-6">
                <strong data-to="5682">0</strong>
                <label>Jogadores cadastrados</label>
            </div>
            <div class="col-md-3 col-sm-6">
                <strong data-to="1031">0</strong>
                <label>Times cadastros</label>
            </div>
            <div class="col-md-3 col-sm-6">
                <strong data-to="60">0</strong>
                <label>Campeonatos realizados</label>
            </div>
            <div class="col-md-3 col-sm-6">
                <strong data-to="12">0</strong>
                <label>Vitoriosos</label>
            </div>
        </div>
    </div>
</div>
