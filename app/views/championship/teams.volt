<section class="page-top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/" class="home">Social Gamers</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/campeonatos/" class="home">Campeonatos</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a property="v:title" title="Go to Social Gamers." href="/campeonatos/{{championship.slug}}" class="home">{{championship.name}}</a>
						</span>
					</li>
					<li class="current_item">
						<span typeof="v:Breadcrumb">
							<span property="v:title">Times Participantes</span>
						</span>
					</li>
                </ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2 class="yellow">Times Participantes</h2>
			</div>
		</div>
	</div>
</section>

<div class="container">
	<div class="row">
       <div class="col-md-12">
    		<div class="center">
                	<p class="featured lead">
                	 	{% if steps is empty %}
 							Não existem etapas disponíveis para realizar check-in.
					 	{% else %}
							Selecione a Etapa para para ver os times participantes:
						{% endif %}
						<br/>
					</p>	
			</div>
		</div>
	</div>
	<div class="row">
	<div class="col-md-12">
		<?php $this->flashSession->output() ?>
	</div>		

	<div class="row">
 	{% if steps is empty %}

 	{% else %}
		<div class="pricing-table">			
			{% for step in steps %}
				<div class="col-md-3">
					<a href="/campeonatos/times/etapa/{{step.id}}" class="push-top push-bottom">
						<div class="plan">
							<h3>
								{{step.name}}
								<span>
									{% if step.championship.image is defined %}
									<img class="img-circle img-responsive img-championship" src="/files/championship/{{step.championship.id}}/{{step.championship.image}}">
									{% else %}
									<img class="img-circle img-responsive img-championship"  src="/img/team/team-1.png">
									{% endif %}
								</span>
							</h3>
							<button class="btn btn-lg btn-primary yellow-btn">Ver Lista de Times</button>
						</div>
					</a>
				</div>
			{% endfor %}
		</div>
		{% endif %}
	</div>
</div>