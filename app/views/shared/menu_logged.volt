<header id="header">
	<div class="container">
		<div class="logo">
			<a href="/">
				<img alt="SocialGamers" data-sticky-width="82" data-sticky-height="40" src="/img/logo.png">
			</a>
		</div>
		<nav>
			<ul class="nav nav-pills nav-top">

				<li class="phone">
					<span><i class="fa fa-phone"></i>Team speak 3: ts.socialgamers.com.br</span>
				</li>
			</ul>
		</nav>
		<button class="btn btn-responsive-nav btn-inverse" data-toggle="collapse" data-target=".nav-main-collapse">
			<i class="fa fa-bars"></i>
		</button>
	</div>

	<div class="navbar-collapse nav-main-collapse collapse">
		<div class="container">
			<ul class="social-icons">
				<li class="facebook"><a href="https://www.facebook.com/socialgamersbrasil" target="_blank" title="Facebook">Facebook</a></li>
				<li class="youtube"><a href="https://www.youtube.com/user/socialgamersbrtv/" target="_blank" title="Twitter">Twitter</a></li>
				<li class="twitter"><a href="https://twitter.com/socialgamersbr/" target="_blank" title="Youtube">Youtube</a></li>
				<li class="zerply"><a href="http://www.twitch.tv/socialgamerstv/" target="_blank" title="Twitch">Twitch</a></li>
			</ul>
			<nav class="nav-main mega-menu">
				<ul class="nav nav-pills nav-main" id="mainMenu">
					<li class="dropdown active">
						<a href="/">Home</a>
					</li>

					<li> 
						<a href="http://blog.socialgamers.com.br/">Notícias</a>
					</li>

					<li class="dropdown mega-menu-item mega-menu-fullwidth">
						<a class="dropdown-toggle" href="/campeonatos">
							Campeonatos
							<i class="fa fa-angle-down"></i>
						</a>
						<ul class="dropdown-menu">
							<li>
								<div class="mega-menu-content">
									<div class="row">
									{% for championship in championships %}
										<div class="col-md-3">
											<ul class="sub-menu border-yellow">
												<li>
													<span class="mega-menu-sub-title">
														{% if championship.imageMenu is defined %}
															<img src="/files/championship/{{championship.id}}/{{championship.imageMenu}}">
														{% else %}
															<!-- <img class="img-circle img-responsive img-championship"  src="/img/team/team-1.png"> -->
														{% endif %}
													</span>
													<ul class="sub-menu campeonatos">
														<li>
															<a href="/campeonatos/{{championship.slug}}">O que é</a>
														</li>

														{%if championship.slug != "circuito-social-league"%}

														<li>
															<a href="/campeonatos/inscricao/{{championship.id}}">Inscreva-se</a>
														</li>

														{%else%}

														<li>
														</li>

														{%endif%}
														
														<li>
															<a href="/campeonatos/check-in/{{championship.id}}">Check-In</a>
														</li>
														<li>
															<a href="/campeonatos/reportar-vitoria/{{championship.id}}">Reportar Vitória
															</a>

														</li>
														<li>
															<a href="/campeonatos/regras/{{championship.id}}">Regras</a>
														</li>
														<li>
															<a href="/campeonatos/chave/{{championship.id}}">Chave</a>
														</li>
														<li>
															<a href="/campeonatos/ranking/{{championship.id}}">Ranking</a>
														</li>
														<li>
															<a href="/campeonatos/times/{{championship.id}}">Times Participantes</a>
														</li>
													</ul>
												</li>
											</ul>
										</div>
									{% endfor %}
									</div>
								</div>
							</li>
						</ul>
					</li>					
					<li> 
						<a href="/times">Times</a>
					</li>
 
 					<li> 
						<a href="http://blog.socialgamers.com.br/parceiros/">Parceiros</a>
					</li>

					<li> 
						<a href="http://blog.socialgamers.com.br/contato/">Contato</a>
					</li>
					
					<li class="dropdown mega-menu-item mega-menu-signin signin logged">
						<a href="/usuario/perfil/" class="dropdown-toggle white">
							<i class="fa fa-user"></i> 
							{{summoner}}
							<i class="fa fa-angle-down"></i>
						</a>
						<ul class="dropdown-menu">
							<li>
								<div class="mega-menu-content">
									<div class="row">
										<div class="col-md-8">
											<div class="user-avatar">
												<div class="img-thumbnail">
													{% if userPicture is defined %}
														<img src="/files/userprofile/{{userId}}/thumb_profile_id_{{userPicture}}" alt="">
													{% else %}
														<img src="/img/avatar.png" alt="">
													{% endif %}
												</div>
												<p><strong>{{name}}</strong></p>
											</div>
										</div>
										<div class="col-md-4">
											<ul class="list-account-options">
												<li>
													<a href="/usuario/perfil/">Perfil</a>
												</li>
												{% if teamId != 0%}
												<li>
													<a href="/times/meu-time">Time</a>
												</li>
												{% endif %}
												<li>
													<a href="/sair">Sair</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</li>

				</ul>
			</nav>
		</div>
	</div>
</header>