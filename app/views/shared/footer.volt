<div class="container">
	<div class="row">
		<div class="footer-ribbon">
			<span>Isso é Social Gamers</span>
		</div>
	</div>
		<div class="col-md-10">
		</div>
		<div class="col-md-2">
			<h4>Siga-nos</h4>
			<div class="social-icons">
				<ul class="social-icons">
					<li class="facebook"><a href="https://www.facebook.com/socialgamersbrasil" target="_blank" title="Facebook">Facebook</a></li>
					<li class="youtube"><a href="https://www.youtube.com/user/socialgamersbrtv/" target="_blank" title="Twitter">Twitter</a></li>
					<li class="twitter"><a href="https://twitter.com/socialgamersbr" target="_blank" title="Youtube">Youtube</a></li>
					<li class="zerply"><a href="http://www.twitch.tv/socialgamerstv" target="_blank" title="Twitch">Twitch</a></li>
				</ul>
			</div>
		</div>
</div>

<div class="footer-copyright">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<a href="/" class="logo">
					<img alt="SocialGamers" class="img-responsive" src="/img/logo-footer.png">
				</a>
			</div>
			<div class="col-md-3">
				<p>© Copyright 2015. All Rights Reserved.</p>
			</div>

	</div>
</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-48826003-2', 'auto');
  ga('send', 'pageview');

</script>