<section class="page-header mb-none">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li><a href="/">Home</a></li>
					<li class="active">{{bradcrumb}}</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h1>{{pageName}}</h1>
			</div>
		</div>
	</div>
</section>