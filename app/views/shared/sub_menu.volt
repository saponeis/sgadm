<aside class="nav-secondary" id="navSecondary" data-plugin-sticky data-plugin-options='{"minWidth": 991, "padding": {"top": 68}}'>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-pills">
					<li><a data-hash data-hash-offset="149" href="#first">Perfil</a></li>
					<li><a data-hash data-hash-offset="149" href="#second">Time</a></li>
					<li><a data-hash data-hash-offset="149" href="#third">Configurações</a></li>
				</ul>
			</div>
		</div>
	</div>
</aside>