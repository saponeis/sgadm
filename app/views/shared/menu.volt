<header id="header">
	<div class="container">
		<div class="logo">
			<a href="/">
				<img alt="SocialGamers" data-sticky-width="82" data-sticky-height="40" src="/img/logo.png">
			</a>
		</div>
		<nav>
			<ul class="nav nav-pills nav-top">

				<li class="phone">
					<span><i class="fa fa-phone"></i>Team speak 3: ts.socialgamers.com.br</span>
				</li>
			</ul>
		</nav>
		<button class="btn btn-responsive-nav btn-inverse" data-toggle="collapse" data-target=".nav-main-collapse">
			<i class="fa fa-bars"></i>
		</button>
	</div>

	<div class="navbar-collapse nav-main-collapse collapse">
		<div class="container">
			<ul class="social-icons">
				<li class="facebook"><a href="https://www.facebook.com/socialgamersbrasil" target="_blank" title="Facebook">Facebook</a></li>
				<li class="youtube"><a href="https://www.youtube.com/user/socialgamersbrtv/" target="_blank" title="Twitter">Twitter</a></li>
				<li class="twitter"><a href="https://twitter.com/socialgamersbr/" target="_blank" title="Youtube">Youtube</a></li>
				<li class="zerply"><a href="http://www.twitch.tv/socialgamerstv/" target="_blank" title="Twitch">Twitch</a></li>
			</ul>
			<nav class="nav-main mega-menu">
				<ul class="nav nav-pills nav-main" id="mainMenu">
					<li class="dropdown active">
						<a href="/">Home</a>
					</li>

					<li> 
						<a href="http://blog.socialgamers.com.br">Notícias</a>
					</li>

					<li class="dropdown mega-menu-item mega-menu-fullwidth">
						<a class="dropdown-toggle" href="/campeonatos">
							Campeonatos
							<i class="fa fa-angle-down"></i>
						</a>
						<ul class="dropdown-menu">
							<li>
								<div class="mega-menu-content">
									<div class="row">
									{% for championship in championships %}
										<div class="col-md-3">
											<ul class="sub-menu border-yellow">
												<li>
													<span class="mega-menu-sub-title">
														{% if championship.imageMenu is defined %}
															<img src="/files/championship/{{championship.id}}/{{championship.imageMenu}}">
														{% else %}
															<!-- <img class="img-circle img-responsive img-championship"  src="/img/team/team-1.png"> -->
														{% endif %}
													</span>
													<ul class="sub-menu campeonatos">
														<li>
															<a href="/campeonatos/{{championship.slug}}">O que é</a>
														</li>
														<li>
															<a href="/campeonatos/regras/{{championship.id}}">Regras</a>
														</li>
														<li>
															<a href="/campeonatos/chave/{{championship.id}}">Chave</a>
														</li>	
														<li>
															<a href="/campeonatos/ranking/{{championship.id}}">Ranking</a>
														</li>
														<li>
															<a href="/campeonatos/times/{{championship.id}}">Times Participantes</a>
														</li>
													</ul>
												</li>
											</ul>
										</div>
									{% endfor %}
									</div>
								</div>
							</li>
						</ul>
					</li>

					<li> 
						<a href="/times">Times</a>
					</li>
 
 					<li> 
						<a href="http://blog.socialgamers.com.br/parceiros/">Parceiros</a>
					</li>

					<li> 
						<a href="http://blog.socialgamers.com.br/contato/">Contato</a>
					</li>
					<li class="dropdown mega-menu-item mega-menu-signin signin active" id="headerAccount">
						<a class="dropdown-toggle white" href="/entrar">
							<i class="fa fa-user"></i> Login
							<i class="fa fa-angle-down"></i>
						</a>
						<ul class="dropdown-menu">
							<li>
								<div class="mega-menu-content">
									<div class="row">
										<div class="col-md-12">
											<div class="signin-form">
												<span class="mega-menu-sub-title">Login</span>
												<form action="/usuario/login" id="login" method="post">
													<div class="row">
														<div class="form-group">
															<div class="col-md-12">
																<label>Email</label>
																<input type="text" name="email" value="" class="form-control input-lg" required>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="form-group">
															<div class="col-md-12">
																<a class="pull-right" id="headerRecover" href="/usuario/esqueci-senha">(Esqueceu a Senha?)</a>
																<label>Senha</label>
																<input type="password" name="password" value="" class="form-control input-lg" required>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<input type="submit" value="Login" class="btn btn-primary pull-right push-bottom yellow-btn" data-loading-text="Loading...">
														</div>
													</div>
												</form>
												<p class="sign-up-info">Ainda não tem Cadastro? <a href="/entrar" id="headerSignUp">Cadastre-se aqui!</a></p>
											</div>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</li>
				</ul>
			</nav>
		</div>
	</div>
</header>