<head>
	<!-- Basic -->
	<meta charset="utf-8">
	<title>SocialGamers -</title>		
	<meta name="keywords" content="League Of Legends Social Gamers Campeonatos " />
	<meta name="description" content="SocialGamers">
	<meta name="author" content="saponeis">

	<!-- Mobile Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Web Fonts  -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

	<!-- Vendor CSS -->
    {{stylesheet_link('vendor/bootstrap/bootstrap.css')}}
    {{stylesheet_link('vendor/fontawesome/css/font-awesome.css')}}
    {{stylesheet_link('vendor/owlcarousel/owl.carousel.min.css')}}
    {{stylesheet_link('vendor/owlcarousel/owl.theme.default.min.css')}}
    {{stylesheet_link('vendor/magnific-popup/magnific-popup.css')}}
	{{stylesheet_link('vendor/pnotify/pnotify.custom.css')}}
	{{stylesheet_link('vendor/bootstrap-datepicker/css/datepicker3.css')}}
	{{stylesheet_link('vendor/summernote/summernote.css')}}

	<!-- Theme CSS -->
    {{stylesheet_link('css/theme.css')}}
    {{stylesheet_link('css/theme-elements.css')}}
    {{stylesheet_link('css/theme-blog.css')}}
    {{stylesheet_link('css/theme-shop.css')}}
    {{stylesheet_link('css/theme-animate.css')}}


	<!-- Skin CSS -->
    {{stylesheet_link('css/theme-animate.css')}}

	<!-- Theme Custom CSS -->
    {{stylesheet_link('css/custom.css')}}

	<!-- Head Libs -->
	{{ javascript_include('vendor/modernizr/modernizr.js') }}

	<!--[if IE]>
        {{stylesheet_link('/css/ie.css"')}}
	<![endif]-->

	<!--[if lte IE 8]>
		{{ javascript_include('/vendor/respond/respond.js') }}
		{{ javascript_include('/vendor/excanvas/excanvas.js') }}
	<![endif]-->

	<!-- Vendor -->
	{{ javascript_include('vendor/jquery/jquery.js') }}

	{{ javascript_include('vendor/jquery.appear/jquery.appear.js') }}
	{{ javascript_include('vendor/jquery.easing/jquery.easing.js') }}
	{{ javascript_include('vendor/jquery-cookie/jquery-cookie.js') }}
	{{ javascript_include('vendor/bootstrap/bootstrap.js') }}
	
	{{ javascript_include('vendor/jquery-browser-mobile/jquery.browser.mobile.js') }}
	{{ javascript_include('vendor/nanoscroller/nanoscroller.js') }}
	{{ javascript_include('vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}
	{{ javascript_include('vendor/jquery-placeholder/jquery.placeholder.js') }}

	{{ javascript_include('vendor/jquery.validation/jquery.validation.js') }}
	{{ javascript_include('vendor/jquery.stellar/jquery.stellar.js') }}
	{{ javascript_include('vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js') }}
	{{ javascript_include('vendor/jquery.gmap/jquery.gmap.js') }}
	{{ javascript_include('vendor/isotope/jquery.isotope.js') }}
	{{ javascript_include('vendor/owlcarousel/owl.carousel.js') }}
	{{ javascript_include('vendor/jflickrfeed/jflickrfeed.js') }}
	{{ javascript_include('vendor/magnific-popup/jquery.magnific-popup.js') }}
	{{ javascript_include('vendor/vide/vide.js') }}
	{{ javascript_include('vendor/modernizr/modernizr.js') }}
	{{ javascript_include('vendor/common/common.js') }}
	<!-- Theme Base, Components and Settings -->
	{{ javascript_include('js/theme.js') }}
{{ javascript_include('vendor/bootstrap-wizard/jquery.bootstrap.wizard.js') }}
{{ javascript_include('vendor/jquery-maskedinput/jquery.maskedinput.min.js') }}
{{ javascript_include('vendor/jquery-validation/jquery.validate.js') }}
{{ javascript_include('vendor/summernote/summernote.js') }}
{{ javascript_include('vendor/summernote/lang/summernote-pt-BR.js') }}

	<!-- Theme Custom -->
	{{ javascript_include('js/custom.js') }}
	
	<!-- Theme Initialization Files -->
	{{ javascript_include('js/theme.init.js') }}

	<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
	<script type="text/javascript">
	
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-12345678-1']);
		_gaq.push(['_trackPageview']);
	
		(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
	
	</script>
	 -->

</head>