    {{stylesheet_link('vendor/bootstrap-fileinput/bootstrap-fileinput.min.css')}}
<div role="main" class="main">
    <section class="page-top">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumb">
						<li class="home">
							<span typeof="v:Breadcrumb">
								<a rel="v:url" property="v:title" title="Go to Social Gamers." href="/" class="home">Social Gamers</a>
							</span>
						</li>
						<li class="home">
							<span typeof="v:Breadcrumb">
								<a rel="v:url" property="v:title" title="Go to Social Gamers." href="/usuario/perfil/" class="home">Perfil</a>
							</span>
						</li>
						<li class="current_item">
							<span typeof="v:Breadcrumb">
								<span property="v:title">Editar Perfil!</span>
							</span>
						</li>
	                </ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h2 class="yellow">Editar Perfil</h2>
				</div>
			</div>
		</div>
	</section>

    <div class="container">
        <div class="row">
        <div class="col-md-12">
        	<?php $this->flashSession->output() ?>
	
        </div>
            <div class="span12 col-md-12 column_first column_last">
				<div class="box-content">
					<form id="editProfile" action="/usuario/editar" method="post" enctype="multipart/form-data">
						<div class="row">
 							 <div class="form-group col-md-6">
								<label for="FirstName">Nome</label>
								<input type="text" value="{{user.firstName}}" name="firstName"  class="form-control input-lg" id="firstName" />						

								<label for="lastName">Sobrenome</label>
								<input type="text" value="{{user.lastName}}" name="lastName"  class="form-control input-lg" id="lastName" />						

								<label for="summoner">Invocador</label>
								<input type="text" value="{{user.summoner}}" name="summoner"  class="form-control input-lg" id="summoner" />						

								<label for="email">Email</label>
								<input type="email" value="{{user.email}}" name="email"  class="form-control input-lg" id="email" />						

								<label for="birthDate">Data de Nascimento</label>
								<input type="text" value="{{user.getBirthDate()}}" name="birthDate"  class="form-control input-lg" id="birthDate" />									

								<label for="city">Cidade</label>
								<input type="text" value="{{user.city}}" name="city"  class="form-control input-lg" id="city" />						

								<label for="state">Estado</label>
								<input type="text" value="{{user.state}}" name="state"  class="form-control input-lg" id="state" cl/>						
							</div>
 							 <div class="form-group col-md-6">
								<label for="college">Universidade</label>
								<input type="text" value="{{user.college}}" name="college"  class="form-control input-lg" id="college" />	

								<label for="facebook">Facebook</label>
								<input type="text" value="{{user.facebook}}" name="facebook"  class="form-control input-lg" id="facebook" />						

								<label for="twitter">Twitter</label>
								<input type="text" value="{{user.twitter}}" name="twitter"  class="form-control input-lg" id="twitter"  />						

								<label for="skype">Skype</label>
								<input type="text" value="{{user.skype}}" name="skype"  class="form-control input-lg" id="skype" />						

								<label for="youtube">Youtube</label>
								<input type="text" value="{{user.youtube}}" name="youtube"  class="form-control input-lg" id="youtube" />						

								<label for="twitch">Twitch</label>
								<input type="text" value="{{user.twitch}}" name="twitch"  class="form-control input-lg" id="twitch" />		
							</div>
 							 <div class="form-group col-md-6">
								<label for="picture">Foto - Arquivo com no máximo 500K
								</label> 

								<input id="fileupload" type="file" name="picture" class="form-control input-lg">

							</div>
 							 <div class="form-group col-md-12">
								<label for="about">Sobre</label>
								<textarea name="about" id="about" class="editor form-control" rows="3" id="textareaAutosize" data-plugin-textarea-autosize="true" style="overflow: hidden; word-wrap: break-word; resize: none; height: 94px;">{{user.about}}</textarea>
							</div>
						</div>
					</div>
					<hr class="tall"/>
					<div class="row">
						<div class="col-md-3 col-md-offset-9">
							<input type="submit" value="Salvar" id="salvar" class="btn-lg btn yellow-btn" />
							<a href="/usuario/perfil/">
								<input type="button" value="Voltar" id="voltar" class="btn-lg btn yellow-btn" />
							</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

	{{ javascript_include('vendor/jquery-autosize/jquery.autosize.js') }}
	{{ javascript_include('vendor/bootstrap-fileinput/bootstrap-fileinput.min.js') }}
	{{ javascript_include('vendor/bootstrap-fileinput/fileinput_locale_pt-BR.js') }}
	{{ javascript_include('js/views/view.editProfile.js') }}

