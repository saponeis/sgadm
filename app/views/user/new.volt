<a href="/" class="logo pull-left">
	<img src="/assets/images/logo.png" height="54" alt="Porto Admin" />
</a>
<div class="panel panel-sign">
	<div class="panel-title-sign mt-xl text-right">
		<h2 class="title text-uppercase text-bold m-none"><i class="fa fa-user mr-xs"></i>Cadastrar</h2>
	</div>
	<div class="panel-body">
		{{ this.flashSession.output() }}

		<form id="new-user-form" action="/usuario/novo" method="post" >
			<div class="validation-message">
				<ul></ul>
			</div>

			<div class="form-group mb-lg">
				<label class="control-label">Nome: <span class="required">*</span> </label>
				<input type="text" name="firstName" class="form-control input-lg" title="Insira seu nome" required/>
			</div>

			<div class="form-group mb-lg">
				<label class="control-label">Sobrenome: <span class="required">*</span> </label>
				<input type="text" name="lastName" class="form-control input-lg" title="Insira seu sobrenome" required/>
			</div>
			<div class="form-group mb-lg">
				<label class="control-label">Email: <span class="required">*</span></label>
				<input type="email" name="email" class="form-control input-lg" title="Insira seu e-mail" required/>
			</div>
			
			<div class="form-group mb-lg">
				<div class="row">
					<div class="col-sm-6 mb-lg">
						<label>Senha: <span class="required">*</span> </label>
						<input name="password" type="password" class="form-control input-lg" title="Insira uma senha" required/>
					</div>
					<div class="col-sm-6 mb-lg">
						<label>Confirmação da Senha: <span class="required">*</span> </label>
						<input name="passwordConfirm" type="password" class="form-control input-lg" title="Insira novamente sua senha" required/>
					</div>
				</div>
			</div>

			<footer class="panel-footer">	
				<div class="row">
					<div class="col-sm-8">
						<div class="checkbox-custom checkbox-default">
							<input id="AgreeTerms" name="terms" type="checkbox" title="Aceite os Termos de Uso do site" required/>
							<label for="AgreeTerms">Eu concordo com os <a href="#">termos de uso</a> da SocialGamers</label>
						</div>
					</div>
					<div class="col-sm-4 text-right">
						<button type="submit" class="btn btn-primary hidden-xs">Cadastrar</button>
						<button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">Cadastrar</button>
					</div>
				</div>
			</footer>
			<br/>
			<p class="text-center">Já possui cadastro? <a href="usuario/login">Faça o Login!</a>
		</form>
	</div>
</div>
<script src="assets/javascripts/forms/new.user.validation.form.js"></script>
