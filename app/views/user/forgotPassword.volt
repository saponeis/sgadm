<div role="main" class="main">
    <section class="page-top">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumb">
						<li class="home">
							<span typeof="v:Breadcrumb">
								<a rel="v:url" property="v:title" title="Go to Social Gamers." href="/" class="home">Social Gamers</a>
							</span>
						</li>
						<li class="current_item">
							<span typeof="v:Breadcrumb">
								<span property="v:title">Nova Senha!</span>
							</span>
						</li>
	                </ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h2 class="yellow">Nova Senha</h2>
				</div>
			</div>
		</div>
	</section>

    <div class="container">
        <div class="row">
            <div class="span12 col-md-12 column_first column_last">
        		<div class="center">
            		<h2 class="short word-rotator-title">
                		Saudações
                		<strong class="inverted">
                    		<span class="word-rotate" data-plugin-options='{"delay": 2000}'>
                        		<span class="word-rotate-items">
                            		<span>Invocador</span>
                            		<span>Summoner</span>
                        		</span>
                    		</span>
                		</strong>
            		</h2>
					<div class="col-md-12">
                    	<p class="featured lead">
							Insira seu email abaixo que enviaremos uma nova senha para o seu email. <br/>
							<br/>
						</p>	
					</div>
					<div class="col-md-6 col-md-offset-3">
						<div class="featured-box featured-box-yellow default">
							<div class="box-content">
							<form action="/usuario/nova-senha" method="post">
									<div class="row">
										<div class="form-group">
											<div class="col-md-12">
											<label for="email">Email</label>
											<input type="text" name="email"  class="form-control input-lg"/>						
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<input type="submit" value="Solicitar nova senha" class="btn btn-primary pull-right push-bottom yellow-btn" data-loading-text="Loading...">
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
    				<br/><br/>
					<div class="col-md-12 ">
						<p>
                    		Muito Obrigado por Jogar com a gente!<br/>
                    		Equipe SocialGamers.
                    	</p>
                	</div>
            	</div>
        	</div>
    	</div>
	</div>
</div>