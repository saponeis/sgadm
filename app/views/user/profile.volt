<section class="page-top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a rel="v:url" property="v:title" title="Go to Social Gamers." href="/" class="home">Social Gamers</a>
						</span>
					</li>
					<li class="current_item">
						<span typeof="v:Breadcrumb">
							<span property="v:title">{{userName | uppercase}}</span>
						</span>
					</li>
                </ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2 class="yellow">{{user.summoner}}</h2>
			</div>
		</div>
	</div>
</section>

<div class="container">
	<div class="row">
	<?php $this->flashSession->output() ?>

		<div class="col-md-4">
			<div class="thumbnail">
				{% if user.picture is defined %}
					<img alt="" height="300" class="img-responsive" src="/files/userprofile/{{user.id}}/profile_id_{{user.picture}}">
				{% else %}
					<img alt="" height="300" class="img-responsive" src="/img/team/team-1.png">
				{% endif %}
			</div>
		</div>
		<div class="col-md-8">
			<h1 class="shorter">{{user.firstName}} <strong>"{{user.summoner}}"</strong> {{user.lastName}}</h1>
			{% if user.college is defined %}
				<h4><strong>Universidade: {{user.college}} </h4> 
			{% endif %}

			<h2><strong>SG.Tag:</strong> #{{user.id}} </h2> 
			<h6><strong>Data de Nascimento:</strong> {{ user.getBirthDate() }}</h6> 
			{% if user.about is defined %}
				<h6><strong>Sobre: {{user.about}} </h6> 
			{% endif %}
			<h6><strong>Cidade:</strong> {{user.city}} </h6> 
			<h6><strong>Estado:</strong> {{user.state}} </h6> 
			<ul class="social-icons">

				{% if user.facebook is defined %}
					<li class="facebook"><a href="{{user.facebook}}" target="_blank" title="Facebook">Facebook</a></li>
				{% endif %}

				{% if user.youtube is defined %}
					<li class="youtube"><a href="{{user.youtube}}" target="_blank" title="youtube">Twitter</a></li>
				{% endif %}

				{% if user.twitter is defined %}
					<li class="twitter"><a href="{{user.twitter}}" target="_blank" title="twitter">Youtube</a></li>
				{% endif %}

				{% if user.twitch is defined %}
					<li class="zerply"><a href="{{user.twitch}}" target="_blank" title="Twitch">Twitch</a></li>
				{% endif %}

			</ul>
		</div>
	</div>
	<hr class="tall"/>
	

	{% if menu == true %}
	<div class="row">
		<div class="col-md-6 col-md-offset-6">
			<a href="/usuario/editar">
				<button type="button" class="btn yellow-btn btn-lg ">Editar Perfil</button>
			</a>
			{% if user.teamId == 0 %}
				<a href="/times/novo">
					<button type="button" class="btn yellow-btn btn-lg ">Novo Time</button>
				</a>			
			{% else %}
				<a href="/times/meu-time">
					<button type="button" class="btn yellow-btn btn-lg ">Meu Time</button>
				</a>
			{% endif %}
			<a href="/usuario/alterar-senha">
				<button type="button" class="btn yellow-btn btn-lg ">Alterar Senha</button>
			</a>
			<a href="/sair">
				<button type="button" class="btn yellow-btn btn-lg ">Sair</button>
			</a>
		</div>
	</div>
	{% endif %}

