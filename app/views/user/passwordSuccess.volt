<div role="main" class="main">
    <section class="page-top">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumb">
						<li class="home">
							<span typeof="v:Breadcrumb">
								<a rel="v:url" property="v:title" title="Go to Social Gamers." href="/" class="home">Social Gamers</a>
							</span>
						</li>
						<li class="current_item">
							<span typeof="v:Breadcrumb">
								<span property="v:title">Sucesso!</span>
							</span>
						</li>
	                </ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h2 class="yellow">GG EASY</h2>
				</div>
			</div>
		</div>
	</section>

    <div class="container">
        <div class="row">
            <div class="span12 col-md-12 column_first column_last">
        		<div class="center">
            		<h2 class="short word-rotator-title">
                		Saudações
                		<strong class="inverted">
                    		<span class="word-rotate" data-plugin-options='{"delay": 2000}'>
                        		<span class="word-rotate-items">
                            		<span>Invocador</span>
                            		<span>Summoner</span>
                        		</span>
                    		</span>
                		</strong>
            		</h2>
                	<div>
                    	<p class="featured lead">
                    		Enviamos sua nova senha por email!<br/>
							<br/>
                    		Muito Obrigado por Jogar com a gente!<br/>
                    		Equipe SocialGamers.                    		
                    	</p>
            		</div>
            	</div>
        	</div>
    	</div>
	</div>
</div>