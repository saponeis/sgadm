<div role="main" class="main">
    <section class="page-top">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumb">
						<li class="home">
							<span typeof="v:Breadcrumb">
								<a rel="v:url" property="v:title" title="Go to Social Gamers." href="/" class="home">Social Gamers</a>
							</span>
						</li>
						<li class="current_item">
							<span typeof="v:Breadcrumb">
								<span property="v:title">Erro!</span>
							</span>
						</li>
	                </ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h2 class="yellow">Erro</h2>
				</div>
			</div>
		</div>
	</section>

    <div class="container">
        <div class="row">
            <div class="span12 col-md-12 column_first column_last">
        		<div class="center">
            		<h2 class="short word-rotator-title">
                		Saudações
                		<strong class="inverted">
                    		<span class="word-rotate" data-plugin-options='{"delay": 2000}'>
                        		<span class="word-rotate-items">
                            		<span>Invocador</span>
                            		<span>Summoner</span>
                        		</span>
                    		</span>
                		</strong>
            		</h2>
                	<div>
                    	<p class="featured lead">
							Algo deu errado durante o processamento de sua ultima ação; <br/>
							Veja abaixo os erros e tente novamente.
							<br/><br/>
							Erros:
						</p>

						<?php $this->flashSession->output() ?>


						<br/>
                    	<p class="featured lead">
							Em caso de dúvidas ou dificuldades, entre em contato com a nossa Staff.<br/>
							Team Speak: ts.socialgamers.com.br<br/>
							Email: contato@socialgamers.com.br<br/><br/>
                    		Muito Obrigado por Jogar com a gente!<br/>
                    		Equipe SocialGamers.                    		
                    	</p>
						<a href="{{backUrl}}">
							<button type="button" class="btn yellow-btn btn-lg ">
								Voltar
							</button>
						</a> 
		      		</div>
            	</div>
        	</div>
    	</div>
	</div>
</div>