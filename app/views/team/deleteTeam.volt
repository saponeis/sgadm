<div role="main" class="main">
    <section class="page-top">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumb">
						<li class="home">
							<span typeof="v:Breadcrumb">
								<a rel="v:url" property="v:title" title="Go to Social Gamers." href="/" class="home">Social Gamers</a>
							</span>
						</li>
						<li class="home">
							<span typeof="v:Breadcrumb">
								<a rel="v:url" property="v:title" title="Go to Social Gamers." href="/times/meu-time" class="home">Times</a>
							</span>
						</li>						
						<li class="current_item">
							<span typeof="v:Breadcrumb">
								<span property="v:title">Deletar Time</span>
							</span>
						</li>
	                </ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h2 class="yellow">Deletar Time</h2>
				</div>
			</div>
		</div>
	</section>

    <div class="container">
        <div class="row">
            <div class="span12 col-md-12 column_first column_last">
        		<div class="center">
            		<h2 class="short word-rotator-title">
                		Saudações
                		<strong class="inverted">
                    		<span class="word-rotate" data-plugin-options='{"delay": 2000}'>
                        		<span class="word-rotate-items">
                            		<span>Invocador</span>
                            		<span>Summoner</span>
                        		</span>
                    		</span>
                		</strong>
            		</h2>
					<div class="col-md-12">
                    	<p class="featured lead ">
                    	Você tem certeza que deseja excuir o time?
						</p>
					</div>
					<div class="alert alert-danger">
							ATENÇÃO - Ao deletar o time, <strong>TODOS</strong> os jogadores irão ficar sem time.<br/>
							Caso deseje apenas sair do time, passe o poder de capitão para outro jogador e em seguida será liberada a opção de sair do time ;)
					</div>
					<div class="col-md-6 col-md-offset-3">
						<div class="featured-box featured-box-yellow default">
							<div class="box-content">
							<form action="/times/deletar" method="post">
								<div class="row">
									<div class="col-md-12">
										<input type="submit" value="Deletar Time" class="btn btn-primary push-bottom yellow-btn" data-loading-text="Loading..."/>
										<a href="/times/meu-time">
											<input type="button" value="Voltar" id="voltar" class="btn btn-primary push-bottom yellow-btn" />
										</a>
									</div>
								</div>
							</form>
							</div>
						</div>
					</div>
    				<br/><br/>
					<div class="col-md-12 ">
						<p>
                    		Muito Obrigado por Jogar com a gente!<br/>
                    		Equipe SocialGamers.
                    	</p>
                	</div>
            	</div>
        	</div>
    	</div>
	</div>
</div>