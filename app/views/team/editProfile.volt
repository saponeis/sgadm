    {{stylesheet_link('vendor/bootstrap-fileinput/bootstrap-fileinput.min.css')}}



<div role="main" class="main">
    <section class="page-top">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumb">
						<li class="home">
							<span typeof="v:Breadcrumb">
								<a rel="v:url" property="v:title" title="Go to Social Gamers." href="/" class="home">Social Gamers</a>
							</span>
						</li>
						<li class="home">
							<span typeof="v:Breadcrumb">
								<a rel="v:url" property="v:title" title="Go to Social Gamers." href="/times/perfil/{{team.id}}" class="home">{{team.name}}</a>
							</span>
						</li>
						<li class="current_item">
							<span typeof="v:Breadcrumb">
								<span property="v:title">Editar Time!</span>
							</span>
						</li>
	                </ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h2 class="yellow">Editar Time!</h2>
				</div>
			</div>
		</div>
	</section>

    <div class="container">
        <div class="row">
	        <div class="col-md-12">
	        	<?php $this->flashSession->output() ?>
	        </div>
            <div class="span12 col-md-12 column_first column_last">
				<div class="box-content">
					<form id="editProfile" action="/times/editar" method="post" enctype="multipart/form-data">
						<div class="row">
 							 <div class="form-group col-md-6">
								<input type="hidden" name="teamId" value={{team.id}} />
								<label for="facebook">Facebook</label>
								<input type="text" value="{{team.facebook}}" name="facebook"  class="form-control input-lg" id="facebook" />						

								<label for="twitter">Twitter</label>
								<input type="text" value="{{team.twitter}}" name="twitter"  class="form-control input-lg" id="twitter"  />							

								<label for="youtube">Youtube</label>
								<input type="text" value="{{team.youtube}}" name="youtube"  class="form-control input-lg" id="youtube" />						

								<label for="twitch">Twitch</label>
								<input type="text" value="{{team.twitch}}" name="twitch"  class="form-control input-lg" id="twitch" />		
						
								<label for="picture">Foto - Arquivo com no máximo 500K
								</label> 
								<input id="fileupload" type="file" name="picture" class="form-control input-lg">
							</div>
						</div>
					</div>
					<hr class="tall"/>
					<div class="row">
						<div class="col-md-3 col-md-offset-9">
							<input type="submit" value="Salvar" id="salvar" class="btn-lg btn yellow-btn" />
							<a href="/times/meu-time">
								<input type="button" value="Voltar" id="voltar" class="btn-lg btn yellow-btn" />
							</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

	{{ javascript_include('vendor/jquery-autosize/jquery.autosize.js') }}
	{{ javascript_include('vendor/bootstrap-fileinput/bootstrap-fileinput.min.js') }}
	{{ javascript_include('vendor/bootstrap-fileinput/fileinput_locale_pt-BR.js') }}
	{{ javascript_include('js/views/view.editProfile.js') }}

