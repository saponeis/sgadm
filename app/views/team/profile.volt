<section class="page-top">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a rel="v:url" property="v:title" title="Go to Social Gamers." href="/" class="home">Social Gamers</a>
						</span>
					</li>
					<li class="home">
						<span typeof="v:Breadcrumb">
							<a rel="v:url" property="v:title" title="Go to Social Gamers." href="/" class="home">Times</a>
						</span>
					</li>					
					<li class="current_item">
						<span typeof="v:Breadcrumb">
							<span property="v:title">{{team.name | uppercase}}</span>
						</span>
					</li>
                </ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2 class="yellow">{{team.name}}</h2>
			</div>
		</div>
	</div>
</section>

<div class="container">
	<div class="row">
	<?php $this->flashSession->output() ?>

		<div class="col-md-4">
			<div class="thumbnail">
				{% if team.picture is defined %}
					<img alt="" height="300" class="img-responsive" src="/files/teamprofile/{{team.id}}/profile_id_{{team.picture}}">
				{% else %}
					<img alt="" height="300" class="img-responsive" src="/img/team/team-1.png">
				{% endif %}
			</div>
		</div>
		<div class="col-md-8">
			<h1 class="shorter">{{team.name}}</h1>
			<h2><strong>ID.TeamTag:</strong> #{{team.id}} </h2> 
			<ul class="social-icons">
				{% if team.facebook is defined %}
					<li class="facebook"><a href="{{team.facebook}}" target="_blank" title="Facebook">Facebook</a></li>
				{% endif %}

				{% if team.youtube is defined %}
					<li class="youtube"><a href="{{team.youtube}}" target="_blank" title="youtube">Twitter</a></li>
				{% endif %}

				{% if team.twitter is defined %}
					<li class="twitter"><a href="{{team.twitter}}" target="_blank" title="twitter">Youtube</a></li>
				{% endif %}

				{% if team.twitch is defined %}
					<li class="zerply"><a href="{{team.twitch}}" target="_blank" title="Twitch">Twitch</a></li>
				{% endif %}
			</ul>
		</div>
	</div>
	<hr class="tall"/>
	

<div class="row">
	<div class="pricing-table">			
		{% for player in players %}
			<div class="col-md-3">

			{% if player.id == team.capitanId %}
			<div class="plan most-popular">
				<div class="plan-ribbon-wrapper"><div class="plan-ribbon">Capitão</div></div>
			{% else %}
				<div class="plan">
			{% endif %}
					<h3>
						{{player.getFullName()}}
						<span>
							{% if player.picture is defined %}
								<img class="img-circle img-responsive img-profile-team"  src="/files/userprofile/{{player.id}}/thumb_profile_id_{{player.picture}}" alt="">
							{% else %}
								<img class="img-circle img-responsive" width="100"  src="/img/avatar.png" alt="">
							{% endif %}
						</span>
					</h3>
					<a class="btn btn-lg btn-primary yellow-btn" href="/usuario/perfil/{{player.id}}">{{player.summoner}}</a>
					<br/>
					<br/>
					<ul class="social-icons">
						{% if team.facebook is defined %}
							<li class="facebook"><a href="{{team.facebook}}" target="_blank" title="Facebook">Facebook</a></li>
						{% endif %}

						{% if team.youtube is defined %}
							<li class="youtube"><a href="{{team.youtube}}" target="_blank" title="youtube">Twitter</a></li>
						{% endif %}

						{% if team.twitter is defined %}
							<li class="twitter"><a href="{{team.twitter}}" target="_blank" title="twitter">Youtube</a></li>
						{% endif %}

						{% if team.twitch is defined %}
							<li class="zerply"><a href="{{team.twitch}}" target="_blank" title="Twitch">Twitch</a></li>
						{% endif %}
					</ul>		
				</div>
			</div>
		{% endfor %}
	</div>
</div>


	<div class="row">
		{% if menu == true %}
		<div class="col-md-9 col-md-offset-3">
			<a href="/times/editar-time">
				<button type="button" class="btn yellow-btn btn-lg ">Editar Time</button>
			</a>
			<a href="/times/adicionar-jogador">
				<button type="button" class="btn yellow-btn btn-lg ">Adicionar Jogador</button>
			</a>
			<a href="/times/mudar-capitao">
				<button type="button" class="btn yellow-btn btn-lg ">Alterar Capitão</button>
			</a>
			<a href="/times/remover-jogador">
				<button type="button" class="btn yellow-btn btn-lg ">Remover Jogador</button>
			</a>
			<a href="/times/deletar-time">
				<button type="button" class="btn yellow-btn btn-lg ">Deletar Time</button>
			</a>
		</div>
		{% else %}
		{% if isLog %}
		<div class="col-md-2 col-md-offset-10">
			<a href="#" class="push-top push-bottom" data-toggle="modal" data-target="#sair">
				<button type="button" class="btn yellow-btn btn-lg ">Sair do Time</button>
			</a>
		</div>
		{% endif %}
		{% endif %}
	</div>
	<div class="modal fade" id="sair" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<div class="featured-box featured-box-yellow default">
						<div class="box-content border-yellow">
							<form id="editProfile" action="/times/sair" method="get" enctype="multipart/form-data">
								<div class="row">
									<div class="col-md-12">
										<div class="alert alert-warning">
										ATENÇÃO - VocÊ deseja sair do time?
										</div>
										<input type="submit" value="Sair do Time" id="salvar" class="btn-lg btn yellow-btn" />
										<button type="button" class="btn-lg btn btn-red" data-dismiss="modal">Cancelar</button>
									</div>
								</div>
							</form>
						</div>
			    	</div>
				</div>
			</div>
		</div>
	</div>
