<html>
    <head>
        <title>Boas Vindas!</title>
    </head>
    <body>
        <table>
            <tr> <td>Saudações Jogador! Obrigado por se cadastrar no Sistema SocialGamers.</td> </tr>
            <tr> <td>Você está a um passo de confirmar seu cadastro. Por gentileza confira os dados abaixo:</td> </tr>
			<tr> <td>Email: {{userData.email}}</td> </tr>
			<tr> <td>Nome: {{userData.firstName}}</td> </tr>
			<tr> <td>Sobrenome: {{userData.lastName}}</td> </tr>
			<tr><br /></tr>
			<tr> <td>Agora clique neste  <a href="{{publicUrl}}"> link </a> e confirme seu cadastro</td> </tr>
			<tr> <td>Caso não consiga, copie e cole a url abaixo no seu navegador!</td> </tr>
			<tr> <td>URL: {{publicUrl}} </td> </tr>
			<tr><br /></tr>
			<tr><br /></tr>

			<tr> <td> Muito Obrigado! </td></tr>
			<tr> <td> Atenciosamente, Equipe SocialGamers!</td></tr>
        </table>
    </body>
</html>