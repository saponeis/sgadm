<html>
    {{ partial("shared/header") }}
	<body>
	<div class="body">
		{{ partial("shared/menu") }}
			<div role="main" class="main">
				<?php echo $this->getContent();?>
			</div>

			<footer id="footer">
				{{ partial("shared/footer") }}

			</footer>
		</div>
	</body>
</html>