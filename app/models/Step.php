<?php

namespace Admin\Models;

use Phalcon\Mvc\Model\Validator\Uniqueness,
    Phalcon\Mvc\Model\Validator\Email,
    Phalcon\Mvc\Model\Validator\StringLength;

class Step extends Dao\Step
{

    public function createData($data)
    {
    	$this->name = $data['name'];
        $this->championshipId = $data['championshipId'];
        $this->brackets = $data['brackets'];
        $this->startDate = \DateTime::createFromFormat('d/m/Y', trim($data['startDate']))->format('Y-m-d');
        $this->startTime = \DateTime::createFromFormat('G:i', trim($data['startTime']))->format('G:i:s');
        $this->checkinDate = \DateTime::createFromFormat('d/m/Y', trim($data['checkinDate']))->format('Y-m-d');
        $this->checkinTime = \DateTime::createFromFormat('G:i', trim($data['checkinTime']))->format('G:i:s');
        $this->createdAt = Date("Y-m-d H:m:s");
        $this->updatedAt = Date("Y-m-d H:m:s");
   }
 
    public function editData($data)
    {

        $this->name = $data['name'] ? $data['name'] : $this->name;
        $this->championshipId = $data['championshipId'] ? $data['championshipId'] : $this->championshipId ;
        $this->brackets = $data['brackets']  ? $data['brackets'] : $this->brackets ;
        $this->startDate = $data['startDate'] ? \DateTime::createFromFormat('d/m/Y', trim($data['startDate']))->format('Y-m-d') : $this->startDate;
        $this->startTime = $data['startTime'] ? \DateTime::createFromFormat('G:i', trim($data['startTime']))->format('G:i:s') : $this->startTime ;
        $this->checkinDate = $data['checkinDate'] ? \DateTime::createFromFormat('d/m/Y', trim($data['checkinDate']))->format('Y-m-d') : $this->checkinDate;
        $this->checkinTime = $data['checkinTime'] ? \DateTime::createFromFormat('G:i', trim($data['checkinTime']))->format('G:i:s') : $this->checkinTime;
        $this->updatedAt = Date("Y-m-d H:m:s");
        return $this;
    }

    public function createSlug($str)
    {
        $slug = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $slug = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $slug);
        $slug = strtolower(trim($slug, '-'));
        $slug = preg_replace("/[\/_|+ -]+/", '-', $slug);

        return $slug;
    }


    public function getStartDate()
    {
        if ($this->startDate) {
            return \DateTime::createFromFormat('Y-m-d', $this->startDate)->format('d/m/Y');
        }
        return null;
    }

   public function getStartTime()
    {
        if ($this->startTime){
            return \DateTime::createFromFormat('G:i:s', $this->startTime)->format('G:i:s');
        }
    }

   public function getCheckinDate()
    {
        if ($this->StartDate){
             return \DateTime::createFromFormat('Y-m-d', $this->checkinDate)->format('d/m/Y');
        }
        
    }

   public function getCheckinTime()
    {
        if ($this->StartDate){
            return \DateTime::createFromFormat('G:i:s', $this->checkinTime)->format('G:i:s');
        }
    }


    public function canCheckin(){
     
        $dateNow = \DateTime::createFromFormat('Y-m-d', Date("Y-m-d"));
        $hourNow = \DateTime::createFromFormat('G:i', Date("G:i"));
        $startDate = \DateTime::createFromFormat('Y-m-d', $this->startDate);
        $startTime = \DateTime::createFromFormat('G:i:s', $this->startTime);
        $checkinDate = \DateTime::createFromFormat('Y-m-d', $this->checkinDate);
        $checkinTime = \DateTime::createFromFormat('G:i:s', $this->checkinTime);

        $startDate->setTime($startTime->format('H'),$startTime->format('i'));
        $checkinDate->setTime($checkinTime->format('H'),$checkinTime->format('i'));

        $startDate->sub(new \DateInterval('PT1H'));

        if ($dateNow >= $checkinDate && $dateNow < $startDate) {
                return true;

        }
        return false;
    }

 }