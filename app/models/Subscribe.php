<?php

namespace Admin\Models;

use Phalcon\Mvc\Model\Validator\Uniqueness,
    Phalcon\Mvc\Model\Validator\Email,
    Phalcon\Mvc\Model\Validator\StringLength;

class Subscribe extends Dao\Subscribe
{

    public function createData($data)
    {
    	$this->stepId = $data['stepId'];
        $this->teamId = $data['teamId'];
        $this->status = 1;
        $this->createdAt = Date("Y-m-d H:m:s");
        $this->updatedAt = Date("Y-m-d H:m:s");
   }


     
    public function editData($data)
    {
        $this->stepId = $data['stepId'] ? $data['stepId'] : $this->name ;
        $this->teamId = $data['teamId']  ? $data['teamId'] : $this->about ;
        $this->status = $data['status']  ? $data['status'] : $this->rules ;
        $this->updatedAt = Date("Y-m-d H:m:s");
        return $this;
    }

     public function getCreatedDate()
    {
        if ($this->cratedAt) {
            return \DateTime::createFromFormat('Y-m-d', $this->cratedAt)->format('d/m/Y');
        }
        return null;
    }

 }