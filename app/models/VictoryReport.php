<?php

namespace Admin\Models;

use Phalcon\Mvc\Model\Validator\Uniqueness,
    Phalcon\Mvc\Model\Validator\Email,
    Phalcon\Mvc\Model\Validator\StringLength;

class VictoryReport extends Dao\VictoryReport
{

    public function createData($data)
    {
        $this->victoryTeam = $data['victoryTeam'];
        $this->defeatedTeam = $data['defeatedTeam'];
        $this->stepId = $data['stepId'];
        $this->createdAt = Date("Y-m-d H:m:s");
        $this->updatedAt = Date("Y-m-d H:m:s");
        $this->adminId = 0;
        $this->printUrl = $data['printUrl'];
        $this->status = 1;
   }
 
    public function editData($data)
    {
        $this->victoryTeam = $data['victoryTeam'] ? $data['victoryTeam'] : $this->victoryTeam;
        $this->defeatedTeam =$data['defeatedTeam'] ? $data['defeatedTeam'] : $this->defeatedTeam;
        $this->stepId = $data['stepId'] ? $data['stepId'] : $this->stepId;
        $this->updatedAt = Date("Y-m-d H:m:s");
        $this->adminId = $data['adminId'] ? $data['adminId'] : $this->adminId;
        $this->status = $data['status'] ? $data['status'] : $this->status;
        return $this;
    }


 }