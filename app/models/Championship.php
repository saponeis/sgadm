<?php

namespace Admin\Models;

use Phalcon\Mvc\Model\Validator\Uniqueness,
    Phalcon\Mvc\Model\Validator\Email,
    Phalcon\Mvc\Model\Validator\StringLength;

class Championship extends Dao\Championship
{

    public function createData($data)
    {
        $this->name = $data['name'];
        $this->about = $data['about'];
        $this->rules = $data['rules'];
        $this->slug = $this->createSlug($this->name);
        $this->createdAt = Date("Y-m-d H:m:s");
        $this->updatedAt = Date("Y-m-d H:m:s");

   }
     
    public function editData($data)
    {
        $this->name = $data['name'] ? $data['name'] : $this->name ;
        $this->about = $data['about']  ? $data['about'] : $this->about ;
        $this->rules = $data['rules']  ? $data['rules'] : $this->rules ;
        $this->slug = $this->createSlug($this->name);
        $this->updatedAt = Date("Y-m-d H:m:s");
        return $this;
    }

    public function createSlug($str)
    {
        $slug = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $slug = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $slug);
        $slug = strtolower(trim($slug, '-'));
        $slug = preg_replace("/[\/_|+ -]+/", '-', $slug);

        return $slug;
    }

 }