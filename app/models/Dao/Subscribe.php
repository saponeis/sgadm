<?php

namespace Admin\Models\Dao;

use Phalcon\Mvc\Model\Validator\Uniqueness,
    Phalcon\Mvc\Model\Validator\Email,
    Phalcon\Mvc\Model\Validator\StringLength;

class Subscribe extends \Phalcon\Mvc\Model
{

    /**
     * @Primary
     * @Identity
     * @Column(type="integer", nullable=false)
     */
	public $id;

    /**
     * @Column(type="string", length=30, nullable=false)
     */
	public $stepId;

    /**
     * @Column(type="string", length=30, nullable=false)
     */
	public $teamId;

    /**
     * @Column(type="string", length=30, nullable=false)
     */
	public $status;

    /**
     * @Column(type="integer", nullable=false)
     */
    public $createdAt;

    /**
     * @Column(type="date", nullable=false)
     */
	public $updatedAt;


    public function getSource()
    {
        return "subscribe";
    }

	public function columnMap()
	{
	    //Keys are the real names in the table and
	    //the values their names in the application
	    return array(
	        'id' => 'id',
	        'step_id' => 'stepId',
	        'team_id' => 'teamId',
	        'created_at' => 'createdAt',
	        'updated_at' => 'updatedAt',
	        'status' => 'status',
          );
	}

    public function initialize()
    {

        $this->belongsTo("stepId", "\Admin\Models\Step", "id", ["alias" => "step"]);
        $this->belongsTo("teamId", "\Admin\Models\Team", "id", ["alias" => "team"]);
    }

 }