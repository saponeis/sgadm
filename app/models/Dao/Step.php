<?php

namespace Admin\Models\Dao;

use Phalcon\Mvc\Model\Validator\Uniqueness,
    Phalcon\Mvc\Model\Validator\Email,
    Phalcon\Mvc\Model\Validator\StringLength;

class Step extends \Phalcon\Mvc\Model
{

    /**
     * @Primary
     * @Identity
     * @Column(type="integer", nullable=false)
     */
	public $id;

    /**
     * @Column(type="string", length=30, nullable=false)
     */
	public $championshipId;

    /**
     * @Column(type="string", length=30, nullable=false)
     */
	public $name;

    /**
     * @Column(type="string", length=255, nullable=false)
     */
    public $brackets;

    /**
     * @Column(type="string", length=30, nullable=false)
     */
	public $startDate;

    /**
     * @Column(type="string", length=255, nullable=false)
     */
	public $startTime;

    /**
     * @Column(type="string", length=45, nullable=false)
     */
	public $checkinDate;

    /**
     * @Column(type="string", length=45, nullable=false)
     */
	public $checkinTime;

    /**
     * @Column(type="integer", nullable=false)
     */
    public $createdAt;

    /**
     * @Column(type="date", nullable=false)
     */
	public $updatedAt;

	public $status;


    public function getSource()
    {
        return "step";
    }

	public function columnMap()
	{
	    //Keys are the real names in the table and
	    //the values their names in the application
	    return array(
	        'id' => 'id',
	        'championship_id' => 'championshipId',
	        'name' => 'name',
            'brackets' => 'brackets',
	        'start_date' => 'startDate',
            'start_time' => 'startTime',
            'chekin_date' => 'checkinDate',
            'chekin_time' => 'checkinTime',
	        'created_at' => 'createdAt',
	        'updated_at' => 'updatedAt',
	        'status' => 'status',
          );
	}

    public function initialize()
    {

        $this->belongsTo("championshipId", "\Admin\Models\Championship", "id", ["alias" => "championship"]);
        $this->hasMany("id", "\Admin\Models\Subscribe", "stepId", ["alias" => "subscribes"]);
    }

 }