<?php

namespace Admin\Models\Dao;

use Phalcon\Mvc\Model\Validator\Uniqueness,
    Phalcon\Mvc\Model\Validator\Email,
    Phalcon\Mvc\Model\Validator\StringLength;

class Championship extends \Phalcon\Mvc\Model
{

    /**
     * @Primary
     * @Identity
     * @Column(type="integer", nullable=false)
     */
	public $id;

    /**
     * @Column(type="string", length=30, nullable=false)
     */
	public $name;

    /**
     * @Column(type="string", length=30, nullable=false)
     */
	public $about;

    /**
     * @Column(type="string", length=45, nullable=false)
     */
	public $rules;

    /**
     * @Column(type="string", length=45, nullable=false)
     */
	public $image;

    /**
     * @Column(type="string", length=45, nullable=false)
     */
    public $imageMenu;


    /**
     * @Column(type="string", length=45, nullable=false)
     */
    public $slug;

    /**
     * @Column(type="integer", nullable=false)
     */
    public $createdAt;

    /**
     * @Column(type="date", nullable=false)
     */
	public $updatedAt;


    public function getSource()
    {
        return "championship";
    }

    public function getHash()
    {
        return $this->hash;
    }

	public function columnMap()
	{
	    //Keys are the real names in the table and
	    //the values their names in the application
	    return array(
	        'id' => 'id',
	        'name' => 'name',
	        'about' => 'about',
            'rules' => 'rules',
            'image' => 'image',
            'image_menu' => 'imageMenu',
            'slug' => 'slug',
	        'created_at' => 'createdAt',
	        'updated_at' => 'updatedAt',
          );
	}

    public function initialize(){
        
        $this->hasMany("id", "\Admin\Models\Step", "championshipId", ["alias" => "steps"]);
    }


 }