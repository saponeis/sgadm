<?php

namespace Admin\Models\Dao;

use Phalcon\Mvc\Model\Validator\Uniqueness,
    Phalcon\Mvc\Model\Validator\Email,
    Phalcon\Mvc\Model\Validator\StringLength;

class VictoryReport extends \Phalcon\Mvc\Model
{

    /**
     * @Primary
     * @Identity
     * @Column(type="integer", nullable=false)
     */
	public $id;

    /**
     * @Column(type="string", length=30, nullable=false)
     */
	public $victoryTeam;

    /**
     * @Column(type="string", length=30, nullable=false)
     */
	public $defeatedTeam;

    public $stepId;

    /**
     * @Column(type="string", length=30, nullable=false)
     */
	public $status;

    /**
     * @Column(type="integer", nullable=false)
     */
    public $createdAt;

    /**
     * @Column(type="date", nullable=false)
     */
	public $updatedAt;

    /**
     * @Column(type="date", nullable=false)
     */
    public $adminId;

    public $printUrl;

    public function getSource()
    {
        return "report";
    }

	public function columnMap()
	{
	    //Keys are the real names in the table and
	    //the values their names in the application
	    return array(
	        'id' => 'id',
            'step_id' => 'stepId',
            'id_team_victory' => 'victoryTeam',
            'id_team_defeat' => 'defeatedTeam',
	        'created_at' => 'createdAt',
	        'updated_at' => 'updatedAt',
            'status' => 'status',
            'admin_id' => 'adminId',
            'link_report' => 'printUrl',
          );
	}

    public function initialize()
    {
        $this->belongsTo('victoryTeam', '\Admin\Models\Team', 'id', ["alias" => "teamvictory"]);
        $this->belongsTo('defeatedTeam', '\Admin\Models\Team', 'id', ["alias" => "teamdefeated"]);
        $this->belongsTo('stepId', '\Admin\Models\Steps', 'id', ["alias" => "step"]);
    }
 }