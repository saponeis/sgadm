<?php

namespace Admin\Models\Dao;

use Phalcon\Mvc\Model\Validator\Uniqueness,
    Phalcon\Mvc\Model\Validator\Email,
    Phalcon\Mvc\Model\Validator\StringLength;

class Team extends \Phalcon\Mvc\Model
{
    const ROLE_GOD = 0;
    const ROLE_ADMIN = 1;
    const ROLE_STAFF = 2;
    const ROLE_CAPITAIN = 3;
    const ROLE_PLAYER = 4;
    const ROLE_USER = 5;

    /**
     * @Primary
     * @Identity
     * @Column(type="integer", nullable=false)
     */
	public $id;

    /**
     * @Column(type="string", length=30, nullable=false)
     */
	public $name;

    /**
     * @Column(type="string", length=30, nullable=false)
     */
	public $capitanId;

    /**
     * @Column(type="string", length=255, nullable=false)
     */
	public $picture;

    /**
     * @Column(type="string", length=45, nullable=false)
     */
	public $facebook;

    /**
     * @Column(type="string", length=45, nullable=false)
     */
	public $twitter;

    /**
     * @Column(type="string", length=45, nullable=false)
     */
    public $skype;

    /**
     * @Column(type="string", length=45, nullable=false)
     */
	public $youtube;

    /**
     * @Column(type="string", length=45, nullable=false)
     */
	public $twitch;

	/**
     * @Column(type="string", length=45, nullable=false)
     */
	public $hash;

    /**
     * @Column(type="integer", nullable=false)
     */
    public $createdAt;

    /**
     * @Column(type="date", nullable=false)
     */
	public $updatedAt;

    /**
     * @Column(type="integer", nullable=false)
     */
	public $status;
   

   public $updateUserId;

    public function getSource()
    {
        return "teams";
    }

    public function getHash()
    {
        return $this->hash;
    }

	public function columnMap()
	{
	    //Keys are the real names in the table and
	    //the values their names in the application
	    return array(
	        'id' => 'id',
	        'name' => 'name',
	        'last_name' => 'lastName',
            'capitan_id' => 'capitanId',
            'picture' => 'picture',
	        'status' => 'status',
            'twitter' => 'twitter',
            'facebook' => 'facebook',
            'twitch' => 'twitch',
            'youtube' => 'youtube',
            'created_at' => 'createdAt',
            'updated_at' => 'updatedAt',
            'hash' => 'hash',
            'update_user_id' => 'updateUserId',

          );
	}

    public function setStatus($status){
        $this->status = $status;
        return $this;
    }
    
     public function initialize()
    {
        $this->hasMany('id', '\Admin\Models\User', 'teamId', ["alias" =>"users"]);
        $this->hasMany('id', '\Admin\Models\Subscribe', 'teamId', ["alias" => "subscribes"]);
        $this->hasMany('id', '\Admin\Models\VicoryReport', 'victoryTeam', ["alias" => "victoryTeam"]);
        $this->hasMany('id', '\Admin\Models\VicoryReport', 'defeatedTeam', ["alias" => "defeatedTeam"]);
    }

 }