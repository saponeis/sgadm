<?php

namespace Admin\Models\Dao;

use Phalcon\Mvc\Model\Validator\Uniqueness,
    Phalcon\Mvc\Model\Validator\Email,
    Phalcon\Mvc\Model\Validator\StringLength;

class User extends \Phalcon\Mvc\Model
{
    const ROLE_GOD = 0;
    const ROLE_ADMIN = 1;
    const ROLE_STAFF = 2;
    const ROLE_CAPITAIN = 3;
    const ROLE_PLAYER = 4;
    const ROLE_USER = 5;

    /**
     * @Primary
     * @Identity
     * @Column(type="integer", nullable=false)
     */
	public $id;

    /**
     * @Column(type="string", length=30, nullable=false)
     */
	public $firstName;

    /**
     * @Column(type="string", length=30, nullable=false)
     */
	public $lastName;

    /**
     * @Column(type="string", length=30, nullable=false)
     */
    public $summoner;

    /**
     * @Column(type="string", length=30, nullable=false)
     */
    public $email;

    /**
     * @Column(type="date", nullable=false)
     */
	public $birthDate;

    /**
     * @Column(type="string", length=30, nullable=false)
     */
    public $college;

    /**
     * @Column(type="string", length=30, nullable=false)
     */
    public $city;

    /**
     * @Column(type="string", length=30, nullable=false)
     */
    public $state;

    /**
     * @Column(type="string", length=30, nullable=false)
     */
    public $password;

    /**
     * @Column(type="string", length=45, nullable=false)
     */
	public $hash;

    /**
     * @Column(type="string", length=255, nullable=false)
     */
	public $picture;


    /**
     * @Column(type="string", length=45, nullable=false)
     */
	public $about;

    /**
     * @Column(type="string", length=45, nullable=false)
     */
	public $facebook;

    /**
     * @Column(type="string", length=45, nullable=false)
     */
	public $twitter;

    /**
     * @Column(type="string", length=45, nullable=false)
     */
    public $skype;

    /**
     * @Column(type="string", length=45, nullable=false)
     */
	public $youtube;

    /**
     * @Column(type="string", length=45, nullable=false)
     */
	public $twitch;

    /**
     * @Column(type="integer", nullable=false)
     */
    public $createdAt;

    /**
     * @Column(type="date", nullable=false)
     */
	public $updatedAt;

    /**
     * @Column(type="integer", nullable=false)
     */
    public $updateUser;

    /**
     * @Column(type="string", length=30, nullable=false)
     */
	public $role;

    /**
     * @Column(type="integer", nullable=false)
     */
    public $status;

    /**
    * @Column(type="boolean", nuable=false)
    */
    public $userTerm;
    /**
    * @Column(type="integer", nuable=false)
    */
    public $teamId;

    public function getSource()
    {
        return "users";
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getFullName()
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    public function getHash()
    {
        return $this->hash;
    }

	public function columnMap()
	{
	    //Keys are the real names in the table and
	    //the values their names in the application
	    return array(
	        'id' => 'id',
	        'first_name' => 'firstName',
	        'last_name' => 'lastName',
            'summoner' => 'summoner',
            'email' => 'email',
	        'password' => 'password',
            'birthdate' => 'birthDate',
            'college' => 'college',
            'city' => 'city',
            'state' => 'state',
            'hash' => 'hash',
            'facebook' => 'facebook',
            'twitter' => 'twitter',
            'skype' => 'skype',
            'twitch' => 'twitch',
            'youtube' => 'youtube',
            'about' => 'about',
            'picture' => 'picture',
	        'created_at' => 'createdAt',
	        'updated_at' => 'updatedAt',
	        'user_update_id' => 'updateUser',
            'role' => 'role',
	        'status' => 'status',
            'user_term' => 'userTerm',
            'team_id' => 'teamId',
	    );
	}

    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function isAdm()
    {
        return ($this->role == 1);
    }

    public function initialize()
    {
        $this->belongsTo('teamId', 'Team', 'id');
    }

 }