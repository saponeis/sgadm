<?php

namespace Admin\Models;

use Phalcon\Mvc\Model\Validator\Uniqueness,
    Phalcon\Mvc\Model\Validator\Email,
    Phalcon\Mvc\Model\Validator\StringLength;

class Team extends Dao\Team
{

    public function createData($data)
    {
        $this->name = $data['name'];
        $this->capitanId = $data['capitanId'];
        $this->hash = md5(uniqid(rand(), TRUE));
        $this->createdAt = Date("Y-m-d H:m:s");
        $this->updatedAt = Date("Y-m-d H:m:s");
        $this->status = 1;

   }

    public function editProfile($data)
    {
        $this->facebook = $data['facebook'] ? $data['facebook'] : $this->facebook;
        $this->twitter = $data['twitter'] ? $data['twitter'] : $this->twitter;
        $this->twitch = $data['twitch'] ? $data['twitch'] : $this->twitch;
        $this->youtube = $data['youtube'] ? $data['youtube'] : $this->youtube;
        $this->updatedAt = Date("Y-m-d H:m:s");
        $this->updateUser  = $this->id;

        return $this;
    }

    public function getAllPlayers()
    {
        return $user = \Admin\Models\User::find(
            [
                "teamId = :teamId:",
                "bind" => [
                    "teamId" => $this->id,
                ],
            ]
        );   
    }

    public function isTeamFull()
    {
        $user = \Admin\Models\User::find(
            [
                "teamId = :teamId:",
                "bind" => [
                    "teamId" => $this->id,
                ],
            ]
        );
        return $user->count() >= 9;
    }

    public function isInTournament()
    {
        return false;
    }

    public function initialize()
    {

        $this->hasOne("capitanId", "\Admin\Models\User", "id", ["alias" => "capitan"]);
        $this->hasMany("id", "\Admin\Models\User", "teamId", ["alias" => "player"]);
        $this->hasMany("id", "\Admin\Models\Subscribe", "teamId", ["alias" => "subscribe"]);
    }
}