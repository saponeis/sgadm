<?php
namespace Admin\Models;


use Phalcon\Mvc\Controller,
    Phalcon\Mvc\View;

require_once __DIR__ . '/../../vendor/swiftmailer/swiftmailer/lib/swift_required.php';
/**
 *
 * Sends e-mails based on pre-defined templates
 */
class Mail extends \Phalcon\Mvc\User\Component
{

    const TEMPLATE_INVITE_PLAYER = 'convidaplayer';
    const TEMPLATE_CONFIRM_SUBSCRIPTION = 'confirmacadastro';
    const TEMPLATE_RESET_PASSWORD = 'passwordreset';
	protected $transport;
	protected $config;

	public function __construct(){
		$this->config = $this->getDI()->get('config');

	}

	/**
	 * Applies a template to be used in the e-mail
	 *
	 * @param string $name
	 * @param array $params
	 */
	public function getTemplate($name, $params)
	{
		$parameters = array_merge(
			['publicUrl' => $this->config->application->baseUri . $params['confirmUrl']], 
			$params
		);
		return $this->view->getRender('layouts/email', $name, $parameters, function($view){
			$view->setRenderLevel(View::LEVEL_ACTION_VIEW);
		});
		return $view->getContent();
	}
	/**
	 * Sends e-mails via gmail based on predefined templates
	 *
	 * @param array $to
	 * @param string $subject
	 * @param string $name
	 * @param array $params
	 */
	public function send($to, $subject, $name, $params)
	{
		//Settings

		$mailSettings = $this->config->mail;
		$template = $this->getTemplate($name, $params);
		// Create the message
		$message = \Swift_Message::newInstance()
  			->setSubject($subject)
  			->setTo($to)
  			->setFrom(array(
  				$mailSettings->fromEmail => $mailSettings->fromName
  			))
  			->setBody($template, 'text/html');
  			if (!$this->transport) {
				$this->transport = \Swift_SmtpTransport::newInstance(
					$mailSettings->smtp->server,
					$mailSettings->smtp->port
					//$mailSettings->smtp->security
				)
		  			->setUsername($mailSettings->smtp->username)
		  			->setPassword($mailSettings->smtp->password);
		  	}
		  	// Create the Mailer using your created Transport
			$mailer = \Swift_Mailer::newInstance($this->transport);
			return $mailer->send($message);
	}

    public function sendConfirmEmail(\Admin\models\User $user)
    {
        $to = array($user->getEmail() => $user->getFullName());
        $subject = 'Confirmação de Cadastro da SocialGamers';
        $template = Mail::TEMPLATE_CONFIRM_SUBSCRIPTION;
        $urlconfirma = array(
                'confirmUrl' => 'usuario/confirmar/' . $user->getHash(),
                'userData' => $user
            );
        return $this->send($to, $subject, $template, $urlconfirma);
    }


    public function sendResetPasswordEmail(\Admin\models\User $user, $newPassword)
    {
        $to = [$user->getEmail() => $user->getFullName()];
        $subject = 'Sua nova senha da MSL';
        $template = Mail::TEMPLATE_RESET_PASSWORD;
        $params = [
                'confirmUrl' => 'a',
                'userData' => ['password' => $newPassword]
          	];
        return $this->send($to, $subject, $template, $params);
    }
 }