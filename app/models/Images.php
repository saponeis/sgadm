<?php

namespace Admin\Models;

use Phalcon\Mvc\Model\Validator\Uniqueness,
    Phalcon\Mvc\Model\Validator\Email,
    Phalcon\Mvc\Model\Validator\StringLength;

class Images extends \Phalcon\Mvc\Model
{

	public function saveImageUserProfile ($file, $user)
	{

  		$filename = 'id_' . $user->id . '.' . $file->getExtension();
        $path = 'files/userprofile/' . $user->id . '/';
        $extension = $file->getExtension();
        if (!$this->createDir($path)) {
        	return false;
        }

        if (!$file->moveTo($path . $filename)) {
        	return false;
        }
        $filenameProfile = 'profile_id_' . $user->id . '.' . $file->getExtension();
        $origem = $path . $filename;
        $destino = $path . $filenameProfile;
        $result = $this->redimensiona($origem, $destino, $extension, 585, 585);

        if (!$result) {
        	return $result;
        }
        
		$filenameProfile = 'thumb_profile_id_' . $user->id . '.' . $file->getExtension();
        $destino = $path . $filenameProfile;
        $result = $this->redimensiona($origem, $destino, $extension, 150, 150);

        return true;
	}

	public function saveImageTeamProfile ($file, $team)
	{

  		$filename = 'id_' . $team->id . '.' . $file->getExtension();
        $path = 'files/teamprofile/' . $team->id . '/';
        $extension = $file->getExtension();
        if (!$this->createDir($path)) {
        	return false;
        }

        if (!$file->moveTo($path . $filename)) {
        	return false;
        }
        $filenameProfile = 'profile_id_' . $team->id . '.' . $file->getExtension();
        $origem = $path . $filename;
        $destino = $path . $filenameProfile;
        $this->redimensiona($origem, $destino, $extension, 585, 585);
	
        return $result;
	}

	public function saveImageChampionshipProfile ($file, $filename, $championshipId)
	{

        $path = 'files/championship/' . $championshipId . '/';
        if (!$this->createDir($path)) {
        	return false;
        }

        if (!$file->moveTo($path . $filename)) {
        	return false;
        }
	
        return true;
	}


	public function createDir($path)
	{
        
        if( is_dir($path) === false )
        {
            mkdir($path, 0777, true);
        }
		
		return true;    
	}

	public static function redimensiona($origem, $destino, $ext, $maxlargura=100, $maxaltura=100, $qualidade=80)
	{
		if (!strstr($origem,"http") && !file_exists($origem) ) {
			echo("Arquivo de origem da imagem inexistente");
			return false;
		}
		$img_origem = false;
		if ($ext == "jpg" || $ext == "jpeg") {
			$img_origem = imagecreatefromjpeg($origem);
		} elseif ($ext == "gif") {
			$img_origem = imagecreatefromgif($origem);
		} elseif ($ext == "png") {
			$img_origem = imagecreatefrompng($origem);
		}
		
		if (!$img_origem) {
			echo("Erro ao carregar a imagem, talvez formato nao suportado");
			return false;
		}
		
		$alt_origem = imagesy($img_origem);
		$lar_origem = imagesx($img_origem);
		$escala = min($maxaltura/$alt_origem, $maxlargura/$lar_origem);
		

		if ($escala < 1) {
			$alt_destino = floor($escala*$alt_origem);

			$lar_destino = floor($escala*$lar_origem);
			// Cria imagem de destino
			$img_destino = imagecreatetruecolor($lar_destino,$alt_destino);
			// Redimensiona
			imagecopyresampled($img_destino, $img_origem, 0, 0, 0, 0, $lar_destino, $alt_destino, $lar_origem, $alt_origem);
			// imagedestroy($img_origem);
		} else {
			$img_destino = $img_origem;
		}

		if ($ext == "jpg" || $ext == "jpeg") {
			imagejpeg($img_destino,$destino,$qualidade);
			return true;
		} elseif ($ext == "gif") {
			imagepng($img_destino,$destino);
			return true;
		} elseif ($ext == "png") {
			imagepng($img_destino,$destino);
			return true;
		} else {
			echo("Formato de destino nao suportado");
			return false;
		}
	}

	public static function redimensionaFixo($origem,$destino,$maxlargura,$maxaltura, $ext ,$qualidade=80)
	{
				
		$img_origem = false;
		if($ext == "jpg" || $ext == "jpeg") {
			$img_origem = @imagecreatefromjpeg($origem);
		} elseif ($ext == "gif") {
			$img_origem = @imagecreatefromgif($origem);
		} elseif ($ext == "png") {
			$img_origem = @imagecreatefrompng($origem);
		}
		
		if(!$img_origem) {
			echo("Erro ao carregar a imagem, talvez formato nao suportado");
			return false;
		}

		$maxAltura = 20;
		
		do {
			$alt_origem = imagesy($img_origem);
			$lar_origem = imagesx($img_origem);
			$escala = min($maxAltura/$alt_origem, $maxAltura/$lar_origem);
			$alt_destino = floor($escala*$alt_origem);
			$lar_destino = floor($escala*$lar_origem);
			$maxAltura = $maxAltura+2;
			} while ($alt_destino <= $maxaltura or $lar_destino <= $maxlargura);
		
		$alt = ceil((((50*$alt_destino)/100))-ceil($maxaltura/2));
		$lar = ceil((((50*$lar_destino)/100))-ceil($maxlargura/2));
		// Cria imagem de destino
		
		$img_destino = imagecreatetruecolor($maxlargura,$maxaltura);
		// Redimensiona
		
		imagecopyresampled($img_destino, $img_origem, -$lar, -$alt, 0, 0, $lar_destino, $alt_destino, $lar_origem,
		$alt_origem);
		
		// imagedestroy($img_origem);
		
		$ext = strtolower(end(explode(‘.’, $destino)));
		
		if($ext == "jpg" || $ext == "jpeg") {
			imagejpeg($img_destino,$destino,$qualidade);
			return true;
		} elseif ($ext == "gif") {
			imagepng($img_destino,$destino);
			return true;
		} elseif ($ext == "png") {
			imagepng($img_destino,$destino);
			return true;
		} else {
			echo("Formato de destino nao suportado");
			return false;
		}
	}
}