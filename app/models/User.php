<?php

namespace Admin\Models;

use Phalcon\Mvc\Model\Validator\Uniqueness,
    Phalcon\Mvc\Model\Validator\Email,
    Phalcon\Mvc\Model\Validator\StringLength;

class User extends Dao\User
{

    public function createData($data)
    {
        $this->firstName = $data['firstName'];
        $this->lastName = $data['lastName'];
        $this->summoner = $data['summoner'];
        $this->email = $data['email'];
        $this->password = md5($data['password']);
        $this->birthDate = \DateTime::createFromFormat('d/m/Y', $data["birthdate"])->format('Y-m-d'); 
        $this->city = $data['city'];
        $this->state = $data['state'];
        $this->hash = md5(uniqid(rand(), TRUE));
        $this->createdAt = Date("Y-m-d H:m:s");
        $this->updatedAt = Date("Y-m-d H:m:s");
        $this->updateUser  = 0;
        $this->role = self::ROLE_USER;
        $this->userTem = $data['terms'] ? 1 : 0;     
        $this->status = 0;
        $this->teamId = 0;
        // var_dump($this->birthdate); die;   

   }

    public function sendResetPasswordEmail( \Admin\models\User $user)
    {
        $mail = New Mail();
        $to = array($$user->email => $this->getFullName());
        $subject = 'Sua nova senha da MSL';
        $template = Mail::TEMPLATE_RESET_PASSWORD;
        $urlconfirma = array(
                'confirmUrl' => 'a',
                'userData' => $data
            );
        return $mail->send($to, $subject, $template, $urlconfirma);
    }

    public function getFullName()
    {
        return $this->firstName . " " . $this->lastName;
    }

    public function getBirthDate()
    {
        return \DateTime::createFromFormat('Y-m-d', $this->birthDate)->format('d/m/Y');
    }
     
    public function editProfile($data)
    {
        $this->firstName = $data['firstName'] ? $data['firstName'] : $this->firstName ;
        $this->lastName = $data['lastName']  ? $data['lastName'] : $this->lastName ;
        $this->summoner = $data['summoner']  ? $data['summoner'] : $this->summoner ;
        $this->email = $data['email']  ? $data['email'] : $this->email ;
        $this->birthDate = $data['birthDate'] ? \DateTime::createFromFormat('d/m/Y', $data["birthDate"])->format('Y-m-d') : $this->birthDate; 
        $this->city = $data['city'] ? $data['city'] : $this->city;
        $this->state = $data['state'] ? $data['state'] : $this->state;
       
        $this->college =  $data['college'] ? $data['college'] : '';
        $this->facebook = $data['facebook'] ? $data['facebook'] : '';
        $this->twitter = $data['twitter'] ? $data['twitter'] : '';
        $this->skype = $data['skype'] ? $data['skype'] : '';
        $this->twitch = $data['twitch'] ? $data['twitch'] : '';
        $this->youtube = $data['youtube'] ? $data['youtube'] : '';
        $this->about = $data['about'] ? $data['about'] : '';
        $this->updatedAt = Date("Y-m-d H:m:s");
        $this->updateUser  = $this->id;

        return $this;
    }
 }