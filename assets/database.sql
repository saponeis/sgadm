-- MySQL dump 10.13  Distrib 5.5.46, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: sgadm
-- ------------------------------------------------------
-- Server version	5.5.46-0ubuntu0.14.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `championship`
--

DROP TABLE IF EXISTS `championship`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `championship` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `about` text,
  `brackets` text,
  `rules` text,
  `image` varchar(45) DEFAULT NULL,
  `image_menu` varchar(45) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `championship`
--

LOCK TABLES `championship` WRITE;
/*!40000 ALTER TABLE `championship` DISABLE KEYS */;
INSERT INTO `championship` VALUES (1,'Master Social League','','','teste','header_1.png','menu_1.png','master-social-league','2016-01-07','2016-01-13'),(9,'teaste0032','','','','header_9.png','menu_9.png','teaste0032','2016-01-07','2016-01-07');
/*!40000 ALTER TABLE `championship` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player_rank`
--

DROP TABLE IF EXISTS `player_rank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `player_rank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `step_id` int(11) DEFAULT NULL,
  `player_id` int(11) DEFAULT NULL,
  `matches` int(11) DEFAULT NULL,
  `wins` int(11) DEFAULT NULL,
  `farm` int(11) DEFAULT NULL,
  `kill` int(11) DEFAULT NULL,
  `death` int(11) DEFAULT NULL,
  `assist` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player_rank`
--

LOCK TABLES `player_rank` WRITE;
/*!40000 ALTER TABLE `player_rank` DISABLE KEYS */;
/*!40000 ALTER TABLE `player_rank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report`
--

DROP TABLE IF EXISTS `report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `step_id` int(11) DEFAULT NULL,
  `id_team_victory` int(11) DEFAULT NULL,
  `id_team_defeat` int(11) DEFAULT NULL,
  `link_report` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report`
--

LOCK TABLES `report` WRITE;
/*!40000 ALTER TABLE `report` DISABLE KEYS */;
/*!40000 ALTER TABLE `report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `step`
--

DROP TABLE IF EXISTS `step`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `step` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `championship_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `start_date` date NOT NULL,
  `start_time` time NOT NULL,
  `chekin_date` date NOT NULL,
  `chekin_time` time NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `step`
--

LOCK TABLES `step` WRITE;
/*!40000 ALTER TABLE `step` DISABLE KEYS */;
INSERT INTO `step` VALUES (1,1,'Thais Zanon','2016-08-01','13:00:00','2016-08-01','12:00:00',1),(2,1,'testeeee','2016-01-16','13:00:00','2016-01-15','09:00:00',1),(3,1,'vai q cola','2017-01-01','13:00:00','2017-01-01','12:00:00',1);
/*!40000 ALTER TABLE `step` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscribe`
--

DROP TABLE IF EXISTS `subscribe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscribe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `step_id` int(11) DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscribe`
--

LOCK TABLES `subscribe` WRITE;
/*!40000 ALTER TABLE `subscribe` DISABLE KEYS */;
INSERT INTO `subscribe` VALUES (6,1,16,2,'2016-01-16 17:01:12','2016-01-16 17:01:12'),(7,1,17,1,'2016-01-22 16:01:52','2016-01-22 16:01:52');
/*!40000 ALTER TABLE `subscribe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_rank`
--

DROP TABLE IF EXISTS `team_rank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_rank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `step_id` int(11) DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  `matches` int(11) DEFAULT NULL,
  `wins` int(11) DEFAULT NULL,
  `kill` int(11) DEFAULT NULL,
  `death` int(11) DEFAULT NULL,
  `assist` int(11) DEFAULT NULL,
  `points` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team_rank`
--

LOCK TABLES `team_rank` WRITE;
/*!40000 ALTER TABLE `team_rank` DISABLE KEYS */;
/*!40000 ALTER TABLE `team_rank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `capitan_id` int(11) NOT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitch` varchar(255) DEFAULT NULL,
  `youtube` varchar(255) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `hash` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams`
--

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
INSERT INTO `teams` VALUES (1,'aaa',5,NULL,1,NULL,NULL,NULL,NULL,'2016-01-05','2016-01-05','b7d5a84b96994f5249f22b0c9fde5d62'),(2,'bbb',5,NULL,1,NULL,NULL,NULL,NULL,'2016-01-05','2016-01-05','ae206be6d28f33c603b33793895ae6ac'),(3,'bbb',5,NULL,1,NULL,NULL,NULL,NULL,'2016-01-05','2016-01-05','44c5b0ddaca18ada7b70309b644aea42'),(4,'ccc',5,NULL,1,NULL,NULL,NULL,NULL,'2016-01-05','2016-01-05','54186260fd874b0fa2614ff42f16c879'),(5,'ddd',5,NULL,1,NULL,NULL,NULL,NULL,'2016-01-05','2016-01-05','6e744fb91f4541cdcdac5040bb0d8780'),(6,'eee',5,NULL,1,NULL,NULL,NULL,NULL,'2016-01-05','2016-01-05','e6154b1842aa53dc2ffa9e1ce407e2e4'),(7,'eee',5,NULL,1,NULL,NULL,NULL,NULL,'2016-01-05','2016-01-05','c66eebedce01ed5d4c98712b8ed052b0'),(8,'fff',5,NULL,1,NULL,NULL,NULL,NULL,'2016-01-05','2016-01-05','1f256a1f3f8ae3e5c3810d976e4c7a20'),(9,'kkk',5,NULL,1,NULL,NULL,NULL,NULL,'2016-01-05','2016-01-05','7b6c39302ef8d99d4964c9568c7e6621'),(10,'mmm',5,NULL,1,NULL,NULL,NULL,NULL,'2016-01-05','2016-01-05','54499a60b2420e6512898e8a2f62634d'),(11,'mmm',2,NULL,1,NULL,NULL,NULL,NULL,'2016-01-05','2016-01-05','666ea482eacc7f2e0d9a1158f324e85d'),(12,'1010',5,NULL,1,NULL,NULL,NULL,NULL,'2016-01-05','2016-01-05','880b727a736939d3793b9846ab3a782c'),(13,'1010',5,NULL,1,NULL,NULL,NULL,NULL,'2016-01-05','2016-01-05','4479c77654b81d13b8852aeffa4039b5'),(14,'1010',5,NULL,1,NULL,NULL,NULL,NULL,'2016-01-05','2016-01-05','52d037cf4aef133b8ee3eba7f4428206'),(15,'zzz',5,NULL,1,NULL,NULL,NULL,NULL,'2016-01-05','2016-01-05','9e893a61e0404b3f5abe2f19a825bd1a'),(17,'saponeusa',6,NULL,1,NULL,'teste',NULL,NULL,'2016-01-20','2016-01-20','d0a7070af79e0b139d038af682e79822');
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(55) NOT NULL,
  `summoner` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `birthdate` date NOT NULL,
  `college` varchar(45) DEFAULT NULL,
  `city` varchar(45) NOT NULL,
  `state` varchar(45) NOT NULL,
  `hash` varchar(255) DEFAULT NULL,
  `facebook` varchar(100) DEFAULT NULL,
  `twitter` varchar(100) DEFAULT NULL,
  `skype` varchar(45) DEFAULT NULL,
  `twitch` varchar(100) DEFAULT NULL,
  `youtube` varchar(100) DEFAULT NULL,
  `about` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `user_update_id` int(11) DEFAULT NULL,
  `role` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `user_term` tinyint(4) DEFAULT '0',
  `team_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,'Erick','Amaral','Erik','erickdsamaral@gmail.com','2b8f70b80054a5f04f5ae68a6dcbea14','1989-10-22',NULL,'SÃ£o Paulo','SP','0b983fd6a571422ff21d4f9ab67cbadc',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-10-27','2015-10-27',0,5,1,1,0),(4,'Renan','Aspeti','shinosan','renan.asperti@gmail.com','0ad5bf51221d76c78944b41618cbbef6','1988-10-22',NULL,'Osasco','SP','4a5f9df0714ea7f115ac34e0cdc2fc64',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-10-31','2015-10-31',0,3,0,0,11),(5,'Raphael','Ramos','shinosan','raphael.ramos@beair.ag','0ad5bf51221d76c78944b41618cbbef6','1986-10-22','Unitau','SÃ£o Paulo','SP','1b20faff8dd0ecd88614a471ad1645b9','www.facebook.com/saponeis','www.twiiter.com/saponeis','saponeis','twitch.com/saponeis','www.youtube.com/saponeis','teste teste teste','5.jpg','2015-11-12','2016-01-06',5,1,1,1,0),(6,'Raphael','Ramos','shinosan','saponeis@gmail.com','0ad5bf51221d76c78944b41618cbbef6','1986-10-22','','SÃ£o Paulo','SP','21531247af1315b86df151f0f86fa4dd','','','','','','3teste 233','6.jpg','2016-01-20','2016-01-20',6,3,1,0,17),(7,'Raphael','Ramos','saponeis2222','raphael.sar@gmail.com','0ad5bf51221d76c78944b41618cbbef6','1986-10-22',NULL,'SÃ£o Paulo','SP','486c995ddeb13cf4232577167db8fb35',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-01-20','2016-01-20',0,5,1,0,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `victory_report`
--

DROP TABLE IF EXISTS `victory_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `victory_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `victory_team` int(11) DEFAULT NULL,
  `defeated_team` int(11) DEFAULT NULL,
  `step_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `print_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `victory_report`
--

LOCK TABLES `victory_report` WRITE;
/*!40000 ALTER TABLE `victory_report` DISABLE KEYS */;
INSERT INTO `victory_report` VALUES (1,17,14,1,'2016-01-16 18:01:58','2016-01-16 18:01:58',2,NULL,'ssss'),(2,14,17,1,'2016-01-16 18:01:52','2016-01-16 18:01:52',2,0,'ssd');
/*!40000 ALTER TABLE `victory_report` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-23 13:24:56
