jQuery( document ).ready(function() {


	var $w1finish = $('#save');
	var	$w1validator = $("#editProfile").validate({
		highlight: function(element) {
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function(element) {
			$(element).closest('.form-group').removeClass('has-error');
			$(element).remove();
		},
		errorPlacement: function( error, element ) {
			element.parent().append( error );
		}
	});

	$w1finish.on('click', function( ev ) {
		ev.preventDefault();
		var validated = $('#editProfile').valid();
		if ( validated ) {
			$("#editProfile").submit();
		}
	});
	$("#birthdate").mask("99/99/9999");

	$("#fileupload").fileinput({
		showPreview:false,
		showUpload:false,
		language: 'pt-BR',
	});

});

