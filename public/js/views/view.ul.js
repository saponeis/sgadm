$(document).ready(function() {


	$("#ul-carousel").owlCarousel({
	    items:5,
	    loop:true,
	    margin:10,
	    autoplay:true,
	    autoplayTimeout:1500,
	    autoplayHoverPause:true,
	    pagination:false,
	});
});
