jQuery( document ).ready(function() {

	'use strict';

	$('.startDate').datepicker({
	    language: "pt-BR",
	    autoclose: true,
	    todayHighlight: true,
		format: 'dd/mm/yyyy',

	});

	$('.checkinDate').datepicker({
	    language: "pt-BR",
	    autoclose: true,
	    todayHighlight: true,
		format: 'dd/mm/yyyy',

	});

	$('.startTime').timepicker({
        showMeridian: false,
        defaultTime: '13:00'

	});

	$('.checkinTime').timepicker({
        showMeridian: false,
        defaultTime: '12:00'

	});



});
